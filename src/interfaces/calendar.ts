export type DatesProp = {
  startDate?: Date;
  endDate?: Date;
  focusedInput?: string;
  currentDate?: Date;
};

export type DateBlockProp = {
  after: Date;
  time: number;
  unit: string;
};

export type DatesRange = {
  start: Date;
  end: Date;
};
