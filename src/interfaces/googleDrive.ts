import { AppError } from "./services";
import { BalanceState, CatalogState } from "./state";

export interface GDriveFilesSearch {
  files: GDriveFile[];
  incompleteSearch: boolean;
  kind: string;
}

export interface GDriveFile {
  id: string;
  kind: string;
  mimeType: string;
  name: string;
  capabilities?: GDriveCapabilities;
  copyRequiresWriterPermission?: boolean;
  createdTime?: string;
  description?: string;
  explicitlyTrashed?: boolean;
  fileExtension?: string;
  fullFileExtension?: string;
  hasThumbnail?: boolean;
  headRevisionId?: string;
  iconLink?: string;
  isAppAuthorized?: boolean;
  lastModifyingUser?: GDriveModify;
  linkShareMetadata?: GdriveShareMetadata;
  md5Checksum?: string;
  modifiedByMe?: boolean;
  modifiedByMeTime?: string;
  modifiedTime?: string;
  originalFilename?: string;
  ownedByMe?: boolean;
  owners?: GDriveModify[];
  parents?: string[];
  permissionIds?: string[];
  permissions?: GDrivePermission[];
  quotaBytesUsed?: string;
  sha1Checksum?: string;
  sha256Checksum?: string;
  shared?: boolean;
  size?: string;
  spaces?: string[];
  starred?: boolean;
  thumbnailVersion?: string;
  trashed?: boolean;
  version?: string;
  viewedByMe?: boolean;
  viewersCanCopyContent?: boolean;
  webContentLink?: string;
  webViewLink?: string;
  writersCanShare?: boolean;
}

export interface GDriveCapabilities {
  canAcceptOwnership: boolean;
  canAddChildren: boolean;
  canAddMyDriveParent: boolean;
  canChangeCopyRequiresWriterPermission: boolean;
  canChangeSecurityUpdateEnabled: boolean;
  canChangeViewersCanCopyContent: boolean;
  canComment: boolean;
  canCopy: boolean;
  canDelete: boolean;
  canDownload: boolean;
  canEdit: boolean;
  canListChildren: boolean;
  canModifyContent: boolean;
  canModifyContentRestriction: boolean;
  canModifyLabels: boolean;
  canMoveChildrenWithinDrive: boolean;
  canMoveItemIntoTeamDrive: boolean;
  canMoveItemOutOfDrive: boolean;
  canMoveItemWithinDrive: boolean;
  canReadLabels: boolean;
  canReadRevisions: boolean;
  canRemoveChildren: boolean;
  canRemoveMyDriveParent: boolean;
  canRename: boolean;
  canShare: boolean;
  canTrash: boolean;
  canUntrash: boolean;
}

export interface GDriveModify {
  displayName: string;
  emailAddress: string;
  kind: string;
  me: boolean;
  permissionId: string;
  photoLink: string;
}

export interface GdriveShareMetadata {
  securityUpdateEligible: boolean;
  securityUpdateEnabled: boolean;
}

export interface GDrivePermission {
  deleted: boolean;
  displayName: string;
  emailAddress: string;
  id: string;
  kind: string;
  pendingOwner: boolean;
  photoLink: string;
  role: string;
  type: string;
}

export interface GDriveHttpError {
  error: AppError;
}

export interface GDriveError {
  json: GDriveHttpError;
  text: string;
}

/**
 * Type del archivo usado para balance
 */
export interface GDriveBalanceFile {
  balance: BalanceState;
}

/**
 * Type del archivo usado para la listas/catalogo
 */
export type GDriveCatalogFile = CatalogState;
