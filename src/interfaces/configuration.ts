import { BalanceTransactionType } from "./state";

export interface MessageTitle {
  title?: string;
  message?: string;
}

export type ValidationError = {
  field: string;
  message: string;
  validation: string;
};

export interface FormError {
  [key: string]: string | undefined;
}

export interface AppFileTypeProps {
  id: string;
  name: string;
  prefix: string;
}

export interface TagColorProps {
  id: string;
  name: string;
  color: string;
}

export interface TransactionTypeProps {
  id: BalanceTransactionType;
  name: string;
  color: string;
  active: boolean;
}

export interface GenericItem {
  id: string; // id milisegundos
  name: string;
  color?: string;
  disabled?: boolean;
}

export interface AmountOptions {
  max: string;
  init: string;
}
