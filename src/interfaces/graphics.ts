export type PieSlice = {
  id: string;
  value: number;
  name: string;
  data: string;
  color: string;
};
