import { transactionTypes } from "configuration/constants";
import { DatesRange } from "./calendar";
import { AmountOptions, TransactionTypeProps } from "./configuration";
import { BalanceSource } from "./state";

export interface MovementFilter {
  date?: DatesRange;
  transactionType?: typeof transactionTypes;
}

export interface MovementDates {
  start?: Date;
  end?: Date;
}
export interface MovementListFilter {
  date?: MovementDates;
  transactionType?: TransactionTypeProps[];
}

export type CategoryDetailParams = BalanceSource & {
  amounts?: AmountOptions;
};
