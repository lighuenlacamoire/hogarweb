import { ModalMessage } from "./buttons";
import { GenericItem } from "./configuration";
import { GoogleUserResult } from "./google";
import { GDriveFile } from "./googleDrive";
import { AppError } from "./services";
import { CharacterDTO } from "./starwarsService";

export interface ActionContent {
  type: string;
  payload?: any;
}

export interface ActionDispatch {
  type: string;
}

/**
 * Type generico para los Action
 */
export type Action = ActionContent;

export interface MessageTitle {
  title?: string;
  message?: string;
}

export interface StorageState {
  mainFolder?: GDriveFile;
  balanceFile?: GDriveFile;
  catalogFile?: GDriveFile;
}

/**
 * State de Balance
 */
export interface BalanceState {
  movements: BalanceMovement[];
  categories: BalanceSource[];
  sources: BalanceSource[];
}

export interface BalanceSource {
  id: string; // id milisegundos
  name: string;
  color?: string;
  disabled?: boolean;
}

export interface BalanceMovement {
  id: string; // Id milisegundos
  debitId: string;
  detail: string;
  date: string;
  dateNumber: number; // fecha en milisegundos
  user: {
    id: string; // id del usuario de google
    name: string; // nombre del usuario de google
  };
  transactionType: BalanceTransactionType;
  creditId: string;
  amount: string;
}

export enum BalanceTransactionType {
  EXPENSE = "Expense",
  INCOME = "Income",
  TRANSFER = "Transfer",
}

export interface StatusState {
  loading?: boolean;
  active?: boolean;
  failure?: MessageTitle;
  closeApp: boolean;
}

export interface EmployeeState {
  list: CharacterDTO[];
}

export interface NotificationState {
  message?: ModalMessage;
}
export interface AuthorizationState {
  user?: GoogleUserResult;
  error?: AppError;
  accessToken?: string;
  lastLogin?: number;
  lastRefresh?: number;
}

export interface CatalogState {
  catalog?: CatalogData;
  categories: GenericItem[];
}

export interface CatalogData {
  id: string; // id milisegundos
  name: string;
  color?: string;
  disabled?: boolean;
  user: {
    id: string; // id del usuario de google
    name: string; // nombre del usuario de google
  };
  items?: CatalogItem[];
}

export interface CatalogItem {
  id: string; // id milisegundos
  name: string;
  order: number;
  categoryId?: string;
  checked: boolean;
}
