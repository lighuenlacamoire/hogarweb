import { useEffect, useState } from "react";

/**
 * Evitar la ejecución excesiva de funciones en respuesta a eventos que se producen con frecuencia, como la actualización del estado en un componente
 * @param {T} value valor a consultar
 * @param {number} delay tiempo
 * @returns {T} valor
 */
const useDebounce = <T>(value: T, delay = 500): T => {
  const [debouncedValue, setDebouncedValue] = useState<T>(value);

  useEffect(() => {
    const timer = setTimeout(() => {
      setDebouncedValue(value);
    }, delay);

    return () => clearTimeout(timer);
  }, [value, delay]);
  return debouncedValue;
};

export default useDebounce;
