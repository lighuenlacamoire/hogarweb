/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React, { useState } from "react";
import { BrowserRouter } from "react-router-dom";
import "./App.css";
import { Header, Main, Sidebar } from "components/Layout";
import { RootRouter } from "routers";
import { Pages } from "configuration/constants";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "redux/store";
import { platform } from "styles";
import { ModalPopUp } from "components/Modal";
import { clearMessage } from "redux/actions/notification";

const App = (): JSX.Element => {
  const dispatch = useDispatch();
  const { accessToken } = useSelector(
    (state: RootState) => state.authorization,
  );
  const [click, setClick] = useState(false);

  const handleClick = () => {
    setClick(!click);
  };
  return (
    <BrowserRouter basename={process.env.PUBLIC_URL || Pages.HOMEPAGE}>
      <Header expanded={click} onClick={handleClick} />
      <Main>
        {accessToken ? <Sidebar expanded={click} /> : null}
        <div
          className="Body"
          style={
            !accessToken
              ? {
                  backgroundColor: platform.colors.formSecondaryBG,
                }
              : {}
          }>
          <RootRouter />
        </div>
      </Main>
    </BrowserRouter>
  );
};

export default App;
