import axios, { AxiosError, AxiosRequestConfig, Method } from "axios";
import {
  GoogleOAuthFullLogin,
  GoogleOauthToken,
  GoogleUserResult,
} from "interfaces/google";
import { AppError, Header, OAuthRequest } from "interfaces/services";
import { appConfig, GDriveErrors, MimeTypes } from "configuration/constants";
import { getGoogleUrl } from "utils/google";
import { ApiCall, setRequestHeaders } from "../api";
import {
  GDriveError,
  GDriveFile,
  GDriveFilesSearch,
} from "interfaces/googleDrive";
import { getItem } from "utils/storage";

/**
 * Configura el header para las llamadas a los servicios de drive
 */
const configureGoogleHeader = async (): Promise<Header> => {
  const token = getItem("@accessTokenGoogle");
  return setRequestHeaders(undefined, token);
};

/**
 * Consulta las carpetas de la App del usuario y/o compartidas con él
 */
const getAppAllFolders = async (): Promise<GDriveFilesSearch> => {
  try {
    const formHeaders = await configureGoogleHeader();
    const data = await ApiCall<GDriveFilesSearch>(
      "https://www.googleapis.com/drive/v3/files",
      "GET",
      formHeaders,
      undefined,
      {
        q: `mimeType = '${MimeTypes.FOLDER}' 
                and name = '${appConfig.folderName}'
                and trashed = false`,
        fields: "files(id, name)",
        spaces: "drive",
      },
    );
    return data;
  } catch (error) {
    console.log("getAppAllFolders error", error);
    throw error;
  }
};

/**
 * Consulta las carpetas de la App del usuario
 */
const getAppOwnFolders = async (): Promise<GDriveFilesSearch> => {
  try {
    const formHeaders = await configureGoogleHeader();
    const data = await ApiCall<GDriveFilesSearch>(
      "https://www.googleapis.com/drive/v3/files",
      "GET",
      formHeaders,
      undefined,
      {
        q: `mimeType = '${MimeTypes.FOLDER}' 
                and name = '${appConfig.folderName}'
                and 'root' in parents
                and trashed = false`,
        fields: "files(id, name)",
        spaces: "drive",
      },
    );
    return data;
  } catch (error) {
    console.log("getAppOwnFolders error", error);
    throw error;
  }
};

/**
 * Obtiene los archivos que se encuentren en la carpeta
 * @param {string} folderId Id de la carpeta contenedora
 */
const getFilesFromFolderId = async (
  folderId: string,
): Promise<GDriveFilesSearch> => {
  try {
    const formHeaders = await configureGoogleHeader();
    const data = await ApiCall<GDriveFilesSearch>(
      "https://www.googleapis.com/drive/v3/files",
      "GET",
      formHeaders,
      undefined,
      {
        q: `mimeType = '${MimeTypes.JSON}' 
                and '${folderId}' in parents
                and trashed = false`,
        fields: "*",
        spaces: "drive",
      },
    );
    return data;
  } catch (error) {
    console.log("getFilesFromFolderId error", error);
    throw error;
  }
};

/**
 * Obtiene un archivo por su Id
 * @param {string} fileId Id del archivo
 */
const getFileById = async <T>(fileId: string): Promise<T> => {
  try {
    const formHeaders = await configureGoogleHeader();
    const responseFile = await ApiCall<T>(
      `https://www.googleapis.com/drive/v3/files/${fileId}`,
      "GET",
      formHeaders,
      undefined,
      {
        fileId: fileId,
        fields: "*",
        alt: "media",
        responseType: "json",
      },
    );
    console.log("responseFile", responseFile);
    //const responseFile = data as T;
    if (responseFile && Object.keys(responseFile).length > 0) {
      return responseFile;
    } else {
      throw new Error("El documento no tiene un formato valido");
    }
  } catch (error) {
    console.log("getFileById error", error);
    throw error;
  }
};

/**
 * Actualiza el archivo de gastos en Drive
 * @param {string} fileId Id del archivo
 * @param {string} fileName Nombre del archivo
 * @param {T} fileContent Contenido del archivo
 */
const putFileById = async <T>(
  fileId: string,
  fileName: string,
  fileContent: T,
): Promise<GDriveFile> => {
  const fileMimyType = MimeTypes.JSON;

  // Some random string that is unlikely to be in transmitted data:
  const boundary = "-batch-31415926579323846boundatydnfj111";
  const delimiter = "\r\n--" + boundary + "\r\n";
  const close_delim = "\r\n--" + boundary + "--";

  const metadata = {
    mimeType: fileMimyType,
    name: fileName,
  };

  const multipartRequestBody =
    delimiter +
    `Content-Type: ${fileMimyType}\r\n\r\n` +
    JSON.stringify(fileContent) +
    close_delim;
  try {
    const formHeaders = await configureGoogleHeader();
    const data = await ApiCall<GDriveFile>(
      `https://www.googleapis.com/upload/drive/v3/files/${fileId}`,
      "PATCH",
      {
        ...formHeaders,
        //'Content-Type': 'multipart/related; boundary="' + boundary + '"',
      },
      JSON.stringify(fileContent),
      {
        uploadType: "media",
        supportsAllDrives: true,
        fields: "*",
      },
    );
    return data;
  } catch (error) {
    console.log("putFileById error", error);
    throw error;
  }
};

/**
 * Crea un archivo de gastos
 * @param {string} folderId Id de la carpeta contenedora
 * @param {string} fileName Nombre para el archivo
 * @param {T} fileContent Contenido del archivo
 */
const postFileIfNotExists = async <T>(
  folderId: string,
  fileName: string,
  fileContent: T,
): Promise<GDriveFile> => {
  const fileMimyType = MimeTypes.JSON;

  // Some random string that is unlikely to be in transmitted data:
  const boundary = "-batch-31415926579323846boundatydnfj111";
  const delimiter = "\r\n--" + boundary + "\r\n";
  const close_delim = "\r\n--" + boundary + "--";

  const metadata = {
    mimeType: fileMimyType,
    name: fileName,
    parents: [folderId],
    description: "archivo de la app Hogar",
  };

  const multipartRequestBody =
    delimiter +
    `Content-Type: ${MimeTypes.JSON_UTF8}\r\n\r\n` +
    JSON.stringify(metadata) +
    delimiter +
    `Content-Type: ${fileMimyType}\r\n\r\n` +
    JSON.stringify(fileContent) +
    close_delim;
  try {
    const formHeaders = await configureGoogleHeader();
    const data = await ApiCall<GDriveFile>(
      "https://www.googleapis.com/upload/drive/v3/files",
      "POST",
      {
        ...formHeaders,
        "Content-Type": 'multipart/related; boundary="' + boundary + '"',
      },
      multipartRequestBody,
      {
        uploadType: "multipart",
        supportsAllDrives: true,
        fields: "*",
      },
    );
    return data;
  } catch (error) {
    console.log("postFileIfNotExists error", error);
    throw error;
  }
};
/**
 * Crea la carpeta de la app si la misma no existe
 */
const postFolderIfNotExists = async (): Promise<any> => {
  try {
    const formHeaders = await configureGoogleHeader();
    const data = await ApiCall<any>(
      "https://www.googleapis.com/drive/v3/files",
      "POST",
      formHeaders,
      {
        name: appConfig.folderName,
        mimeType: MimeTypes.FOLDER,
        parents: ["root"],
      },
      {
        fields: "*",
      },
    );
    // eslint-disable-next-line @typescript-eslint/no-unsafe-return
    return data;
  } catch (error) {
    console.log("postFolderIfNotExists error", error);
    throw error;
  }
};
export {
  getAppAllFolders,
  getAppOwnFolders,
  getFilesFromFolderId,
  getFileById,
  putFileById,
  postFileIfNotExists,
  postFolderIfNotExists,
};
