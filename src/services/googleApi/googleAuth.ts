import axios, { AxiosError, AxiosRequestConfig, Method } from "axios";
import { appConfig } from "configuration/constants";
import {
  GoogleOAuthFullLogin,
  GoogleOauthToken,
  GoogleUserResult,
} from "interfaces/google";
import { OAuthRequest, OAuthUrlProps } from "interfaces/services";
import { handleError } from "services/api";
import { saveItem } from "utils/storage";

/**
 * Obtiene la url para solicitar permisos de OAuth
 * @returns {string} Url
 */
const getPermissionsOAuth2Google = (): string => {
  const options: OAuthUrlProps = {
    authorizeUrl: `https://accounts.google.com/o/oauth2/v2/auth`,
    redirectUri: process.env.REACT_APP_OAUTH_GOOGLE_REDIRECT as string,
    clientId: process.env.REACT_APP_OAUTH_GOOGLE_CLIENT_ID as string,
    accessType: "offline",
    responseType: "code",
    prompt: "consent",
    scopes: appConfig.google.scopes,
    state: process.env.REACT_APP_OAUTH_GOOGLE_ENDPOINT as string,
  };

  return `${options.authorizeUrl}?redirect_uri=${
    options.redirectUri
  }&client_id=${options.clientId}${
    options.accessType ? `&access_type=${options.accessType}` : ""
  }&response_type=${options.responseType}${
    options.prompt ? `&prompt=${options.prompt}` : ""
  }&scope=${options.scopes.join(" ")}&state=${options.state}`;
};

const getGoogleOauthToken = async (code: string): Promise<GoogleOauthToken> => {
  const rootURl = "https://oauth2.googleapis.com/token";

  const options: OAuthRequest = {
    code,
    client_id: process.env.REACT_APP_OAUTH_GOOGLE_CLIENT_ID as string,
    client_secret: process.env.REACT_APP_OAUTH_GOOGLE_CLIENT_SECRET as string,
    redirect_uri: process.env.REACT_APP_OAUTH_GOOGLE_REDIRECT as string,
    grant_type: "authorization_code",
  };
  try {
    const requestConfig: AxiosRequestConfig = {
      method: "POST" as Method,
      url: rootURl,
      data: options,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    };
    const { data } = await axios.request<GoogleOauthToken>(requestConfig);
    /*const { data } = await axios.post<GoogleOauthToken>(
      rootURl,
      JSON.stringify(options),
      {
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
        },
      },
    );
    */

    return data;
  } catch (err: any) {
    console.log("axiosero", err);
    const error = err as AxiosError;
    console.log("axos2", error);
    console.log("Failed to fetch Google Oauth Tokens");
    throw new Error(err);
  }
};

const getGoogleUser = async (
  id_token: string,
  access_token: string,
): Promise<GoogleUserResult> => {
  try {
    const { data } = await axios.get<GoogleUserResult>(
      `https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=${access_token}`,
      {
        headers: {
          Authorization: `Bearer ${id_token}`,
        },
      },
    );

    return data;
  } catch (err: any) {
    console.log(err);
    throw Error(err);
  }
};

/**
 * Realiza el login por OAuth
 */
const getLogginOAuth2Google = async (
  code: string,
): Promise<GoogleOAuthFullLogin> => {
  const rootURl = "https://oauth2.googleapis.com/token";

  const options: OAuthRequest = {
    code,
    client_id: process.env.REACT_APP_OAUTH_GOOGLE_CLIENT_ID as string,
    client_secret: process.env.REACT_APP_OAUTH_GOOGLE_CLIENT_SECRET as string,
    redirect_uri: process.env.REACT_APP_OAUTH_GOOGLE_REDIRECT as string,
    grant_type: "authorization_code",
  };
  try {
    const requestConfig: AxiosRequestConfig = {
      method: "POST" as Method,
      url: rootURl,
      data: options,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    };
    /**
     * Obtiene el Token de acceso de google
     */
    const { data } = await axios.request<GoogleOauthToken>(requestConfig);
    console.log("token", data);
    saveItem("@accessTokenGoogle", data.access_token);
    const userInfo = await axios.get<GoogleUserResult>(
      `https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=${data.access_token}`,
      {
        headers: {
          Authorization: `Bearer ${data.id_token}`,
        },
      },
    );
    console.log("userinfo", userInfo);
    return { info: userInfo.data, token: data };
  } catch (err) {
    const newError = handleError(err);
    throw newError;
  }
};

export { getPermissionsOAuth2Google, getLogginOAuth2Google };
