import { IMainMethods } from "interfaces/services";
import { StarWarsListDTO, CharacterDTO } from "interfaces/starwarsService";
import { ApiCall } from "../api";

/**
 * Consulta el listado de Empleados en capacitacion
 * @param pages cantidad de paginas
 */
export const employeesListRequest = (pages?: string | null) =>
  ApiCall<StarWarsListDTO<CharacterDTO>>(
    "api/people",
    "GET",
    undefined,
    undefined,
  );
