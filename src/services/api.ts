import axios, { AxiosError, AxiosRequestConfig, Method } from "axios";
import { GDriveError, GDriveHttpError } from "interfaces/googleDrive";
import {
  AppError,
  Header,
  HttpResponse,
  OAuthUrlProps,
} from "interfaces/services";
import { logoutExecute } from "redux/actions/authorization";
import { store } from "redux/store";
import { deleteItem } from "utils/storage";

const config = {
  baseURL: "https://swapi.dev/", //Here we get the API host from the enviroment.

  //You can also add headers to the config, e.g: (for more information about config https://axios-http.com/docs/req_config)
  // headers: {
  //   Authorization: `Bearer ${yourToken}`
  // },
};

export const axiosInstance = axios.create(config);

export const getAuthorizeHref = ({
  authorizeUrl,
  clientId,
  redirectUri,
  responseType,
  scopes,
  state,
  prompt,
  accessType,
}: OAuthUrlProps): string => {
  return `${authorizeUrl}?redirect_uri=${redirectUri}&client_id=${clientId}${
    accessType ? `&access_type=${accessType}` : ""
  }&response_type=${responseType}${
    prompt ? `&prompt=${prompt}` : ""
  }&scope=${scopes.join(" ")}&state=${state}`;
};

/**
 * API Calls Handler
 * @param {string} endpoint Endpoint path
 * @param {string} method HTTP Method
 * @param {Header?} header Headers si los necesitase
 * @param {unknown?} body Request body
 * @param {unknown?} parameters Query params
 * @returns {T} respuesta del servicio
 */
export const ApiCall = async <T>(
  endpoint: string,
  method: string,
  header?: Header,
  body?: unknown,
  parameters?: unknown,
): Promise<T> => {
  try {
    const requestConfig: AxiosRequestConfig = {
      method: method as Method,
      url: endpoint,
      data: body,
      params: parameters,
      headers: header,
    };
    const response = await axios.request<T>(requestConfig);
    return response?.data;
  } catch (err) {
    const newError = handleError(err);
    if (newError.code == 401 || newError.status == 401) {
      store.dispatch(logoutExecute());
      deleteItem("@accessTokenGoogle");
    }
    throw newError;
  }
};

/**
 * Manejador de error
 * @param {unknown} error Error desconocido del llamado http con axios
 * @returns {AppError} error configurado para la app
 */
export const handleError = (error: unknown): AppError => {
  const erMessage = "Ha ocurrido un error";
  const erCode = 500;
  const erStatus = 500;
  const erTitle = "Error";

  const errorAxios = error as AxiosError;
  if (errorAxios) {
    if (errorAxios.response?.data) {
      const dataJson = errorAxios.response?.data as GDriveHttpError;
      if (dataJson && dataJson.error) {
        return {
          title: dataJson.error.title,
          message: dataJson.error.message,
          code: dataJson.error.code,
          status: dataJson.error.status,
        };
      }
    }

    return {
      title: erTitle,
      message: errorAxios.message,
      code: parseInt(errorAxios.code ?? "400", 10),
      status: errorAxios.status,
    };
  }

  const errorHttp = error as HttpResponse;
  if (errorHttp && errorHttp.code) {
    console.log("HttpResponse", errorHttp);
    return {
      title: erTitle,
      message: errorHttp.message,
      code: errorHttp.code,
      status: errorHttp.status,
    };
  }

  const errorGDrive = error as GDriveError;
  if (errorGDrive && errorGDrive.json && errorGDrive.json.error) {
    console.log("GDriveError", errorGDrive);
    const { error: erDrive } = errorGDrive.json;
    return {
      title: erTitle,
      message: erDrive.message,
      code: erDrive.code,
      status: erDrive.status,
    };
  }

  const errorGeneric = error as Error;
  if (errorGeneric && errorGeneric.message) {
    console.log("http Error", errorGeneric);
    if (errorGeneric.message === "NETWORK_ERROR") {
      //Alert.alert("Error de Conexion");
    }
    return {
      title: errorGeneric.name,
      message: errorGeneric.message,
      status: 400,
      code: 400,
    };
  }
  return {
    title: erTitle,
    code: erCode,
    status: erStatus,
    message: erMessage,
  };
};

/**
 * Crea los cabezales para el request HTTP
 * @param {Header} headers Headers en caso de ser necesario
 * @param {string} token token de la app
 */
export const setRequestHeaders = (
  header?: Header,
  token?: string | null,
): Header => {
  const formHeaders = header || headers;

  if (token && !header) {
    return {
      ...formHeaders,
      Authorization: `Bearer ${token}`,
    };
  }

  return formHeaders;
};

export const headers: Header = {
  Accept: "application/json",
  "Content-Type": "application/json",
};
