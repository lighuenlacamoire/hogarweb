export const saveItem = (key: string, value: any): void => {
  if (value && typeof value === "string") {
    localStorage.setItem(key, value);
  }
  localStorage.setItem(key, JSON.stringify(value));
};

export const getItem = (key: string): string | null => {
  return localStorage.getItem(key);
};

export const deleteItem = (key: string): void => {
  localStorage.removeItem(key);
};
