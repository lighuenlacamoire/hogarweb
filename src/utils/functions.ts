import { calendarSettings } from "configuration/constants";
import { ImageProps } from "interfaces/buttons";
import { DatesRange } from "interfaces/calendar";
import { BalanceMovement } from "interfaces/state";
import { platform } from "styles";
import { isAfter, isBefore, isSameDay, parse as parseDate } from "date-fns";
import { toAmountCulture, toDecimal } from "./formatter";
import {
  MovementDates,
  MovementFilter,
  MovementListFilter,
} from "interfaces/navigations";

/**
 * Filtra una lista de acuerdo a las key del objeto que se envia
 * @param {BalanceMovement[]} data Lista para filtrar
 * @param {Function} setData Funcion para actualizar la lista
 * @param {MovementFilter} dataFilter Objeto con keys para filtrar
 */
const setListFiltered = (
  data: BalanceMovement[],
  setData: (data: BalanceMovement[]) => void,
  dataFilter?: MovementListFilter,
) => {
  let array = [...data] || [];
  const filtersObject = dataFilter || {};

  if (Object.keys(filtersObject).length > 0) {
    Object.keys(filtersObject).forEach((key) => {
      const keyIndex: keyof typeof filtersObject =
        key as keyof typeof filtersObject;
      if (filtersObject[keyIndex]) {
        const ranges = filtersObject[keyIndex] as DatesRange;
        /**
         * Si el filtro es rango de fechas
         */
        if (ranges && ranges.start) {
          array = array.filter((item) => {
            const dateTo = parseDate(
              item[keyIndex],
              calendarSettings.formats.sortItems,
              new Date(),
            );
            // el campo de fecha es igual al de inicio o final del rango
            const itsSame =
              isSameDay(dateTo, ranges.start) || isSameDay(dateTo, ranges.end);
            // el campo de fecha es posterior al de inicio o anterior al de final
            const itsRange =
              isAfter(dateTo, ranges.start) && isBefore(dateTo, ranges.end);
            if (itsSame || itsRange) {
              return item;
            }
          });
        } else if (Array.isArray(filtersObject[keyIndex])) {
          /**
           * Si el filtro es un array entonces se filtro por id de opciones
           */
          array = array.filter((item) => {
            const apply =
              (filtersObject[keyIndex] as any[]).findIndex(
                (x) => x.active && x.id === item[keyIndex as keyof typeof item],
              ) !== -1;
            if (apply) {
              return item;
            }
          });
        }
      }
    });
  }

  setData(array);
};

/**
 * Setea el/los elementos de la lista segun este activado o no
 * @param {Array} data Lista de elementos
 * @param {Function} setData Funcion para actualizar la lista
 * @param {boolean} multiSelect Indica si puede haber mas de un seleccionado
 * @param {number} index Index del item en la lista
 */
const setListActive = <TEntity>(
  data: (TEntity & { active: boolean })[],
  setData: (data: (TEntity & { active: boolean })[]) => void,
  multiSelect: boolean,
  index: number,
): void => {
  const mod = data[index];
  let newArr = [...data];
  if (!multiSelect && mod.active === false) {
    newArr = newArr.map((x) => {
      return { ...x, active: false };
    });
    mod.active = true;
  } else if (multiSelect) mod.active = !mod.active;

  newArr[index] = mod;
  setData(newArr);
};

/**
 * Calcula la suma total de una propiedad dada que posean los elementos del array
 * @param {any[]} items Lista
 * @param {string} prop Propiedad del elemento de la lista
 */
const calculateSum = (items: any[], prop: string) =>
  items.reduce(
    (a, b) => (a + toDecimal(toAmountCulture(b[prop]))) as number,
    0,
  ) as number;

const wait = async (ms: number): Promise<unknown> => {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
};

export { setListActive, setListFiltered, wait, calculateSum };
