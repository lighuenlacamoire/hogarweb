/* eslint-disable import/no-duplicates */
import {
  addSeconds,
  addMinutes,
  addDays,
  addMonths,
  format,
  differenceInMinutes,
  differenceInSeconds,
  parse,
  startOfMonth,
  endOfMonth,
} from "date-fns";
import { es } from "date-fns/locale";
import { formatInTimeZone } from "date-fns-tz";
import { appConfig, calendarSettings } from "configuration/constants";
import { DatesRange } from "interfaces/calendar";

/**
 * Factoriza la fecha enviada al formato deseado
 * @param {Date | number | string} date Fecha para efecutar la funcion
 * @param {string} formatting Formato deseado para la fecha
 */
const factoringDate = (
  value: Date | number | string,
  formatting?: string,
): string => {
  try {
    const newValue = typeof value === "string" ? new Date(value || "") : value;
    const newFormat = formatting ?? calendarSettings.formats.formInput;
    return format(newValue, newFormat, {
      locale: es,
    });
  } catch (error) {
    console.log("factoringDate", error);
    return "";
  }
};

/**
 * Factoriza la fecha enviada al formato deseado
 * @param {Date | number | string} date Fecha para efecutar la funcion
 * @param {string} formatting Formato deseado para la fecha
 */
const factoringDateTime = (
  value: Date | number | string,
  formatting: string,
): string => {
  try {
    return formatInTimeZone(
      value,
      "America/Argentina/Buenos_Aires",
      formatting,
      {
        locale: es,
      },
    );
  } catch (error) {
    console.log("factoringDateTime", error);
    return "";
  }
};

/**
 * Ingrementa/Disminuye la fecha enviada de a acuerdo a la cantidad enviada y la unidad de tiempo
 * @param date Fecha para efecutar la funcion
 * @param quantity cantidad a incrementar o disminuir en la unidad enviada
 * @param unit Unidad de tiempo ['s','years'...]
 * @param formatting Formato deseado para la fecha
 */
export const addTime = (
  date: Date | string | null = null,
  quantity: number | null = 0,
  unit: string | null = calendarSettings.units.seconds,
  formatting: string | null = null,
): Date | string => {
  const newDate = new Date(date || "");
  let value = date ? newDate : new Date();
  const newValue = value;
  if (date && typeof date === "string") {
    value = toDate(date, formatting) ?? new Date();
  }
  if (unit === calendarSettings.units.months) {
    value = addMonths(newValue, quantity || 0);
  }
  if (unit === calendarSettings.units.days) {
    value = addDays(newValue, quantity || 0);
  }

  if (unit === calendarSettings.units.minutes) {
    value = addMinutes(newValue, quantity || 0);
  }

  if (unit === calendarSettings.units.seconds) {
    value = addSeconds(newValue, quantity || 0);
  }
  if (formatting && formatting.length > 0) {
    return factoringDate(value, formatting);
  }

  return value;
};

/**
 * Convierte string a fecha
 * @param value
 * @param formatting
 * @returns
 */
const toDate = (
  value?: string | null,
  formatting?: string | null,
): Date | undefined => {
  if (value && value.length > 5) {
    return parse(
      value,
      formatting || calendarSettings.formats.formInput,
      new Date(),
      { locale: es },
    );
  }
  return undefined;
};

const toNumber = (value: string): number => parseInt(value || "0", 10);

const toDecimal = (value: string): number => parseFloat(value || "0");

const toAmountCulture = (value: string): string =>
  value.replace(".", "").replace(",", ".");
/**
 * Restringe que el valor enviado sea tipo Numerico
 * @param {string} value valor que debe ser Numerico
 */
const restrictNumber = (value: string): string => value.replace(/[^0-9]/gm, "");

/**
 * Restringe que el valor enviado sea tipo Texto y caracteres permitidos
 * @param {string} value valor que debe ser Texto y caracteres permitidos
 */
const restrictText = (value: string): string =>
  value.replace(
    /[^a-zA-Z0-9áÁéÉíÍóÓúÚüÜñÑ~¡!@#$%^&*()_+=[\],.¿?"';: -{}/|\\<>]/gm,
    "",
  );

/**
 * Dada una fecha, obtiene el primer y el ultimo dia de ese mes
 * @param {Date} value fecha para obtener los valores
 */
const toDatesRange = (value: Date): DatesRange => {
  const firstOfMonth = startOfMonth(value);
  const lastOfMonth = endOfMonth(value);
  const lastOfMonthFix = addTime(
    lastOfMonth,
    -1,
    calendarSettings.units.days,
    null,
  ) as Date;
  return { start: firstOfMonth, end: lastOfMonth };
};

/**
 * Convierte el valor enviado al formato de moneda
 * @param {string} value valor para formatear
 */
const toCurrency = (value: string): string => {
  const newValue = value || "0,00";
  let numberFront = "";
  let numberBack = "";
  if (newValue.length > appConfig.balance.decimalCount + 1) {
    // 0000 o sea 00,00
    numberBack = newValue.slice(
      newValue.length - appConfig.balance.decimalCount,
      newValue.length,
    );
    const auxFront = newValue.slice(
      0,
      newValue.length - appConfig.balance.decimalCount,
    );
    numberFront = `${toNumber(auxFront)}`.replace(
      /(\d)(?=(\d{3})+(?!\d))/g,
      "$1.",
    );
  } else {
    numberBack =
      newValue && newValue.length > 2
        ? newValue.slice(newValue.length - 2, newValue.length)
        : newValue.padStart(2, "0");
    numberFront = "0";
  }

  return `${numberFront},${numberBack}`;
};

/**
 * Convierte el texto enviado en CapitalCase
 * @param {string} value texto para modificar
 */
const capitalize = (value: string): string =>
  value
    .split(" ")
    .map((x) => (x ? x[0].toUpperCase() + x.slice(1).toLowerCase() : ""))
    .join(" ");

export {
  factoringDate,
  factoringDateTime,
  toDate,
  toNumber,
  toDecimal,
  toAmountCulture,
  restrictNumber,
  restrictText,
  toCurrency,
  toDatesRange,
  capitalize,
};
