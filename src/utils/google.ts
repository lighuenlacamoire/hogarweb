import { getAuthorizeHref } from "services/api";
import { OAuthUrlProps } from "interfaces/services";

const getGoogleUrl = () => {
  const options: OAuthUrlProps = {
    authorizeUrl: `https://accounts.google.com/o/oauth2/v2/auth`,
    redirectUri: process.env.REACT_APP_OAUTH_GOOGLE_REDIRECT as string,
    clientId: process.env.REACT_APP_OAUTH_GOOGLE_CLIENT_ID as string,
    accessType: "offline",
    responseType: "code",
    prompt: "consent",
    scopes: [
      "https://www.googleapis.com/auth/userinfo.profile",
      "https://www.googleapis.com/auth/userinfo.email",
    ],
    state: process.env.REACT_APP_OAUTH_GOOGLE_ENDPOINT as string,
  };

  return getAuthorizeHref(options);
};

export { getGoogleUrl };
