import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "redux/store";
import { ModalPopUp } from "components/Modal";
import { clearMessage } from "redux/actions/notification";

const NotificationHandler = (): JSX.Element => {
  const { message } = useSelector((state: RootState) => state.notification);
  /** Dispatch de Redux */
  const dispatch = useDispatch();

  return (
    <ModalPopUp
      title={message?.title}
      content={message?.content}
      isVisible={message?.isVisible}
      selfClose={message?.selfClose}
      onBackdropPress={() => dispatch(clearMessage())}
      actions={message?.actions}
    />
  );
};

export default NotificationHandler;
