/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React from "react";
import {
  eachDayOfInterval,
  endOfISOWeek,
  isAfter,
  isBefore,
  isSameDay,
} from "date-fns";
import { containerStyles, platform, textstyles } from "styles";
import { calendarSettings } from "configuration/constants";
import { factoringDate } from "utils/formatter";
import { DatesProp } from "interfaces/calendar";

type Props = {
  mode: string;
  startDate?: Date;
  endDate?: Date;
  focusedInput?: string;
  focusedMonth: Date;
  startDayOfWeek: Date;
  onDatesChange: (event: DatesProp) => void;
  isDateBlocked: ((a: Date) => boolean | void) | undefined;
  onDisableClicked:
    | (<T>(a: T) => T | Promise<T> | void | Promise<void>)
    | undefined;
};

/**
 * Calendario: Dias de la semana
 */
const CalendarWeekDays = (props: Props): JSX.Element[] => {
  const {
    mode,
    startDate,
    endDate,
    focusedInput,
    focusedMonth,
    startDayOfWeek,
    onDatesChange,
    isDateBlocked,
    onDisableClicked,
  } = props;

  const endDayOfWeek = endOfISOWeek(startDayOfWeek);
  const daysRange = eachDayOfInterval({
    start: startDayOfWeek,
    end: endDayOfWeek,
  });

  const validateDates = (
    startDate?: Date,
    endDate?: Date,
    focusedInput?: string,
  ): DatesProp => {
    if (focusedInput === "startDate") {
      if (startDate && endDate) {
        return { startDate, endDate: undefined, focusedInput: "endDate" };
      }
      return { startDate, endDate, focusedInput: "endDate" };
    }

    if (focusedInput === "endDate") {
      if (endDate && startDate && isBefore(endDate, startDate)) {
        return {
          startDate: endDate,
          endDate: undefined,
          focusedInput: "endDate",
        };
      }
      return { startDate, endDate, focusedInput: "startDate" };
    }

    return { startDate, endDate, focusedInput };
  };

  /**
   * Selecciona la fecha al presionar
   * @param day Fecha
   */
  const onPress = (day: Date) => {
    // Consulta si la fecha esta bloqueada
    if (isDateBlocked && isDateBlocked(day) && onDisableClicked) {
      onDisableClicked(day);
    } else if (mode === calendarSettings.mode.range) {
      // Si es "Rango" de fechas...
      let isPeriodBlocked = false;
      const start = focusedInput === "startDate" ? day : startDate;
      const end = focusedInput === "endDate" ? day : endDate;
      if (start && end && isBefore(end, start)) {
        onDatesChange(validateDates(end, undefined, "startDate"));
        return;
      } else {
        // Si tengo fecha inicio y fecha final, calcula el rango
        // ...de esa manera verifica las fechas en dicho rango
        const daysSelectionRange =
          start && end
            ? eachDayOfInterval({
                start: start,
                end: end,
              })
            : undefined;
        // Verifica si existe un rango, si el mismo esta bloqueado
        if (
          start &&
          end &&
          daysSelectionRange &&
          daysSelectionRange.length > 0
        ) {
          daysSelectionRange.forEach((dayPeriod) => {
            if (isDateBlocked && isDateBlocked(dayPeriod)) {
              isPeriodBlocked = true;
            }
          });
        }
        // Si el preriodo esta bloqueado reinicia las fechas
        // ...de lo contrario actualiza los datos
        onDatesChange(
          isPeriodBlocked
            ? validateDates(end, undefined, "startDate")
            : validateDates(start, end, focusedInput),
        );
      }
    } else if (mode === calendarSettings.mode.single) {
      onDatesChange({
        currentDate: day,
        startDate: day,
        focusedInput: focusedInput || "startDate",
      });
    } else {
      onDatesChange({
        currentDate: day,
        startDate: day,
        focusedInput: focusedInput || "startDate",
      });
    }
  };

  /**
   * Indica si la fecha esta seleccionada
   * @param {Date} day Fecha enviada
   */
  const isDateSelected = (day: Date) => {
    if (mode === calendarSettings.mode.single) {
      return isDateStart(day);
    }
    return isDateStart(day) || isDateEnd(day);
  };

  /**
   * Indica si la fecha esta dentro del rango seleccionado
   * @param {Date} day Fecha enviada
   */
  const isDateRangeSelected = (day: Date) => {
    if (mode === calendarSettings.mode.single) {
      return false;
    } else {
      if (startDate && endDate) {
        // Consulta si la fecha es igual a la de inicio
        //...o si es posterior a la fecha de inicio
        const after = isDateStart(day) || isAfter(day, startDate);
        // Consulta si la fecha es igual a la de fin
        //...o si es anterior a la fecha de fin
        const before = isDateEnd(day) || isBefore(day, endDate);
        return after && before;
      } else {
        return isDateStart(day);
      }
    }
  };

  const isDateStart = (day: Date) => {
    return startDate && isSameDay(day, startDate);
  };

  const isDateEnd = (day: Date) => {
    return endDate && isSameDay(day, endDate);
  };

  // Dias de la semana
  // ...las fechas estan en horario 00
  return daysRange.map((day: Date) => {
    const isBlocked = isDateBlocked ? isDateBlocked(day) : false;
    const isRangeSelected = isDateRangeSelected(day);
    const isStart = isDateStart(day);
    const isEnd = isDateEnd(day);
    const isSelected = isDateSelected(day);
    const isOnMonth =
      focusedMonth.getFullYear() === day.getFullYear() &&
      focusedMonth.getMonth() === day.getMonth();

    const style: React.CSSProperties = {
      ...containerStyles.itemStretch,
      ...styles.dayName,
      // isBlocked && styles.dayBlocked,
      ...(isRangeSelected ? styles.daySelectedBetween : {}),
      ...(isStart ? styles.borderStart : {}),
      ...(isEnd ? styles.borderEnd : {}),
    };
    const styleText: React.CSSProperties = {
      ...textstyles.textInputForm,
      fontSize: platform.fontSizes.LARGE,
      ...(isRangeSelected && (isStart || isEnd) ? styles.daySelectedText : {}),
      ...(isSelected ? styles.daySelectedText : {}),
      // fechas bloqueadas
      ...(isBlocked ? { color: platform.colors.disabled } : {}),
      // las fechas fuera de mes
      ...(isOnMonth ? {} : { color: platform.colors.black }),
    };
    const borderContainer: React.CSSProperties = {
      ...styles.borderContainer,
      cursor: "pointer",
      backgroundColor:
        mode === calendarSettings.mode.single && isSelected
          ? styles.daySelected.backgroundColor
          : platform.colors.formPrimaryBG,

      ...(isBlocked ? { backgroundColor: "blue" } : {}),
      ...(isOnMonth ? {} : { backgroundColor: platform.colors.disabled }),
      ...(isStart ? styles.daySelected : null),
      ...(isEnd ? styles.daySelected : null),
    };
    const itsDisabled = isBlocked && !onDisableClicked;

    return (
      <div
        key={day.toISOString()}
        style={style}

        //disabled={itsDisabled || false}
      >
        <div style={borderContainer} onClick={() => onPress(day)}>
          <span style={styleText}>{factoringDate(day, "dd")}</span>
        </div>
      </div>
    );
  });
};

export default CalendarWeekDays;

export const styles = {
  dayName: {
    marginTop: 2,
    marginBottom: 2,
    paddingTop: 6,
    paddingBottom: 6,
  } as React.CSSProperties,
  daySelected: {
    backgroundColor: platform.colors.primary, // dia seleccionado color de fondo
    borderRadius: 40,
  } as React.CSSProperties,
  daySelectedBetween: {
    backgroundColor: "rgba(91, 200, 172, 0.25)",
  } as React.CSSProperties,
  daySelectedText: {
    color: platform.colors.white,
  } as React.CSSProperties,
  borderContainer: {
    borderRadius: 16,
    width: 32,
    height: 32,
    alignItems: "center",
    justifyContent: "center",
    display: "flex",
  } as React.CSSProperties,
  borderStart: {
    borderTopLeftRadius: 40,
    borderBottomLeftRadius: 40,
  } as React.CSSProperties,
  borderEnd: {
    borderTopRightRadius: 40,
    borderBottomRightRadius: 40,
  } as React.CSSProperties,
};
