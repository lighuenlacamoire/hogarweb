import React from "react";
import { eachDayOfInterval, isSameDay } from "date-fns";
import { containerStyles, platform, textstyles } from "styles";
import { factoringDate } from "utils/formatter";

type Props = {
  currentDate?: Date;
  startDate: Date;
  endDate: Date;
};

/**
 * Calendario: Nombre de los dias
 */
const CalendarDayNames = (props: Props): JSX.Element[] => {
  const { currentDate, startDate, endDate } = props;
  const rangeOfDays = eachDayOfInterval({
    start: startDate,
    end: endDate,
  });

  // Dias de la semana
  return rangeOfDays.map((day) => (
    <div key={day.toISOString()} style={containerStyles.itemStretch}>
      <span
        style={{
          ...textstyles.textInputForm,
          ...styles.dayText,
          ...(currentDate && isSameDay(currentDate, day)
            ? { color: platform.colors.primary }
            : {}),
        }}>
        {factoringDate(day, "E").slice(0, 3)}
      </span>
    </div>
  ));
};

export default CalendarDayNames;

const styles = {
  dayText: {
    width: 32,
    textAlign: "center",
    color: platform.colors.text,
    textTransform: "capitalize",
  } as React.CSSProperties,
};
