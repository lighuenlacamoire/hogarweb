import React, { useState } from "react";
import { isBefore, isAfter, isSunday } from "date-fns";
import CalendarMonth from "./CalendarMonth";
import { DatesProp } from "interfaces/calendar";
import Icon from "components/Icon";
import { addTime, factoringDate } from "utils/formatter";
import { calendarSettings } from "configuration/constants";
import { containerStyles, platform, textstyles } from "styles";

type BlockDate = {
  after: Date;
  time: number;
  unit: string;
};

type Props = {
  mode: string;
  headFormat: string;
  startDate?: Date;
  endDate?: Date;
  blockBefore?: BlockDate;
  blockAfter?: BlockDate;
  onConfirm: (startDate?: Date, endDate?: Date) => void;
  onCancel: () => void;
  headerTitle: string;
  headerLegend: string;
};

const CalendarBody = (props: Props) => {
  const {
    mode,
    headFormat,
    startDate,
    endDate,
    onConfirm,
    onCancel,
    blockBefore,
    blockAfter,
    headerTitle,
    headerLegend,
  } = props;
  const [focusedMonth, setFocusedMonth] = useState<Date>(
    startDate || new Date(),
  );
  const [focusedInput, setFocusedInput] = useState<string | undefined>(
    "startDate",
  );
  const [selectState, setSelectState] = useState("monthAndDate");
  const [selectedYear, setSelectedYear] = useState<number>(
    (startDate || new Date()).getFullYear(),
  );
  const [headFormatD, setHeadFormatD] = useState(
    headFormat ??
      (mode === calendarSettings.mode.range ? "MMM dd,YYYY" : "ddd, MMM D"),
  );

  const [clearSingle, setClearSingle] = useState(
    startDate ? factoringDate(startDate, headFormatD) : "",
  );
  const [clearStart, setClearStart] = useState(
    startDate ? factoringDate(startDate, headFormatD) : "",
  );
  const [clearEnd, setClearEnd] = useState(
    endDate ? factoringDate(endDate, headFormatD) : "",
  );
  const [clStartDate, setClStartDate] = useState<Date | undefined>(startDate);
  const [clEndDate, setClEndDate] = useState<Date | undefined>(endDate);

  const changedMode = selectState === "year" ? "monthAndDate" : "year";

  const onSwipe = (value: number) => {
    const newMonth = addTime(
      focusedMonth,
      value,
      calendarSettings.units.months,
      null,
    ) as Date;

    setFocusedMonth(newMonth);
    setSelectedYear(newMonth.getFullYear());
  };

  const isDateBlocked = (newDate: Date): boolean => {
    if (!blockBefore && !blockAfter) {
      return false;
    }

    let beforeOk = false;
    let afterOk = false;
    let after = null;

    let bef = null;

    if (blockBefore) {
      if (blockBefore.after && mode === calendarSettings.mode.range) {
        beforeOk = startDate ? isBefore(newDate, startDate) : false;
      } else {
        const before = addTime(
          new Date(),
          blockBefore.time,
          blockBefore.unit,
          undefined,
        );
        beforeOk = isBefore(newDate, before as Date);
      }
    }
    if (blockAfter) {
      after = addTime(new Date(), blockAfter.time, blockAfter.unit, undefined);
      bef = after;

      if (blockBefore && blockBefore.after && startDate) {
        after = addTime(
          startDate,
          blockBefore.time,
          blockBefore.unit,
          undefined,
        );
      }
      afterOk =
        isAfter(newDate, after as Date) || isAfter(newDate, bef as Date);
    }
    if (endDate) {
      afterOk = false; // isAfter(date, bef);
      beforeOk = false;
    }
    return beforeOk || afterOk || isSunday(newDate);
  };

  const formatingHeader = (day?: Date): string => {
    if (day) {
      return factoringDate(day, headFormatD);
    }
    return "";
  };

  const onDatesChange = (event: DatesProp): void => {
    const {
      startDate: date1,
      endDate: date2,
      focusedInput: focus,
      currentDate: date3,
    } = event;
    if (mode === calendarSettings.mode.single) {
      setClearSingle(formatingHeader(date1));
      setClStartDate(date3);
      onConfirm(date3, undefined);
      onCancel();
    } else {
      setFocusedInput(focus);
      setClStartDate(date1);
      setClEndDate(date2);
      setClearStart(formatingHeader(date1));
      setClearEnd(formatingHeader(date2));
      if (date1 && date2) {
        onConfirm(date1, date2);
        onCancel();
      }
    }
  };

  return (
    <div>
      <div style={styles.headCoverContainer}>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-end",
            alignItems: "center",
          }}
          onClick={() => setSelectState(changedMode)}>
          <span style={{ ...textstyles.headerTitleStyle, flex: 0.64 }}>
            {headerTitle || `Calendario ${selectedYear}`}
          </span>
          <Icon
            name={selectState === "year" ? "chevron-down" : "chevron-up"}
            size={22}
            color={platform.colors.white}
          />
        </div>
        {headerLegend ? (
          <span style={styles.headerLegend}>{headerLegend}</span>
        ) : null}
        {mode === calendarSettings.mode.range && (
          <div style={containerStyles.itemSpacingElements}>
            <span
              //numberOfLines={1}
              //ellipsizeMode="middle"
              style={{
                ...textstyles.headerTitleStyle,
                ...styles.titleDates,
                ...(clearStart ? {} : { opacity: 0.5 }),
              }}>
              {clearStart || calendarSettings.start}
            </span>
            <span
              style={{
                ...textstyles.headerTitleStyle,
                ...styles.titleDates,
                ...styles.headerSeparator,
                ...(clearStart ? {} : { opacity: 0.5 }),
              }}>
              -
            </span>
            <span
              //numberOfLines={1}
              //ellipsizeMode="middle"
              style={{
                ...textstyles.headerTitleStyle,
                ...styles.titleDates,
                ...(clearEnd ? {} : { opacity: 0.5 }),
              }}>
              {clearEnd || calendarSettings.end}
            </span>
          </div>
        )}
      </div>
      {selectState === "monthAndDate" && (
        <div style={styles.calendar}>
          <div
            style={{
              ...containerStyles.itemSpacingElements,
              padding: 10,
            }}>
            <div
              onClick={() => onSwipe(-1)}
              style={{
                paddingInline: "12px",
                cursor: "pointer",
              }}>
              <Icon
                size={22}
                name="chevron-left"
                color={platform.colors.text}
              />
            </div>
            <div>
              <span
                style={{
                  ...textstyles.headerTitleStyle,
                  color: platform.colors.text,
                }}>
                {factoringDate(focusedMonth, "MMMM")}
              </span>
            </div>
            <div
              onClick={() => onSwipe(1)}
              style={{
                paddingInline: "12px",
                cursor: "pointer",
              }}>
              <Icon
                size={22}
                name="chevron-right"
                color={platform.colors.text}
              />
            </div>
          </div>
          {/**
             * 
            <ScrollView
              horizontal
              ref={this.scrollRef}
              style={{
                flexDirection: 'row',
                marginHorizontal: 16,
                paddingVertical: 8,
              }}>
              {rangeMonths.map((month, index) => {
                const isSelectedMonth =
                  month.getMonth() === currentMonth &&
                  month.getFullYear() === currentYear;
                return (
                  <TouchableOpacity
                    key={month.toISOString()}
                    onPress={() => this.changeMonth(month, index)}>
                    <span
                      style={[
                        textstyles.textButtonLegend,
                        {
                          marginRight: 16,
                          textTransform: 'capitalize',
                          color: isSelectedMonth
                            ? platform.colors.primary
                            : platform.colors.text,
                        },
                      ]}>
                      {factoringDate(month, 'MMMM')}
                    </span>
                  </TouchableOpacity>
                );
              })}
             
            </ScrollView>
             */}
          <CalendarMonth
            mode={mode}
            startDate={clStartDate}
            endDate={clEndDate}
            focusedInput={focusedInput}
            focusedMonth={focusedMonth}
            onSwipe={onSwipe}
            onDatesChange={onDatesChange}
            isDateBlocked={isDateBlocked}
            onDisableClicked={() => console.log()}
          />
        </div>
      )}
      {selectState === "year" && (
        <div style={styles.calendarYear}>
          {/**
             * 
            <Picker
              selectedValue={selectedYear}
              onValueChange={this.changeYear}>
              {rangeArray.map((value) => {
                return (
                  <Picker.Item
                    key={value}
                    label={String(value)}
                    value={value}
                  />
                );
              })}
            </Picker>
             */}
        </div>
      )}
    </div>
  );
};
export default CalendarBody;

export const styles = {
  calendar: {} as React.CSSProperties,
  calendarYear: {
    backgroundColor: platform.colors.formPrimaryBG,
    height: "75%",
    justifyContent: "center",
  } as React.CSSProperties,
  titleDates: {
    flex: 0.45,
    fontSize: 24,
    textAlign: "center",
  } as React.CSSProperties,
  headCoverContainer: {
    alignSelf: "center",
    width: "100%",
    justifyContent: "center",
    padding: 12,
    borderTopLeftRadius: platform.generic.borderRadius,
    borderTopRightRadius: platform.generic.borderRadius,
    backgroundColor: platform.colors.primary,
  } as React.CSSProperties,
  headerLegend: {
    color: platform.colors.white,
    opacity: 1,
    fontSize: 14,
  } as React.CSSProperties,
  headerSeparator: {
    flex: 0.1,
    paddingHorizontal: 3,
    textAlign: "center",
  } as React.CSSProperties,
  headerSingleDate: {
    flex: 1,
    textAlign: "left",
  } as React.CSSProperties,
};
