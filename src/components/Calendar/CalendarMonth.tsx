/* eslint-disable import/no-duplicates */
/* eslint-disable @typescript-eslint/no-explicit-any */
import React, { useRef } from "react";
import {
  eachWeekOfInterval,
  startOfISOWeek,
  endOfISOWeek,
  startOfMonth,
  endOfMonth,
} from "date-fns";
import { es } from "date-fns/locale";
import CalendarWeek from "./CalendarWeek";
import CalendarDayNames from "./CalendarDayNames";
import { DatesProp } from "interfaces/calendar";

type Props = {
  mode: string;
  startDate?: Date;
  endDate?: Date;
  focusedInput?: string;
  focusedMonth: Date;
  onSwipe: (value: number) => void;
  onDatesChange: (event: DatesProp) => void;
  isDateBlocked: ((a: Date) => boolean | void) | undefined;
  onDisableClicked:
    | (<T>(a: T) => T | Promise<T> | void | Promise<void>)
    | undefined;
};

/**
 * Calendario: Mes completo
 */
const CalendarMonth = (props: Props) => {
  const {
    mode,
    startDate,
    endDate,
    focusedInput,
    focusedMonth,
    onSwipe,
    onDatesChange,
    isDateBlocked,
    onDisableClicked,
  } = props;

  const firstOfMonth = startOfISOWeek(startOfMonth(focusedMonth));
  const lastOfMonth = endOfISOWeek(endOfMonth(focusedMonth));
  const endDayOfWeek = endOfISOWeek(firstOfMonth);

  const weeks: JSX.Element[] = [];
  const dayNames: JSX.Element[] = CalendarDayNames({
    startDate: firstOfMonth,
    endDate: endDayOfWeek,
  });

  const rangeOfWeeks = eachWeekOfInterval(
    { start: firstOfMonth, end: lastOfMonth },
    { locale: es, weekStartsOn: 1 },
  );

  const handleAnimation = () => {
    /*
    animatedOpacity.setValue(0);
    Animated.timing(animatedOpacity, {
      toValue: 1,
      useNativeDriver: false,
      easing: Easing.linear,
      duration: 600,
    }).start();
    */
  };
  const onSwipeLeft = () => {
    handleAnimation();
    onSwipe(1);
  };

  const onSwipeRight = () => {
    handleAnimation();
    onSwipe(-1);
  };
  //const { onTouchStart, onTouchEnd } = useSwipe(onSwipeLeft, onSwipeRight, 6);
  // Semanas del mes
  rangeOfWeeks.forEach((week) => {
    weeks.push(
      <CalendarWeek
        key={week.toString()}
        mode={mode}
        startDate={startDate}
        endDate={endDate}
        focusedInput={focusedInput}
        focusedMonth={focusedMonth}
        startDayOfWeek={week}
        onDatesChange={onDatesChange}
        isDateBlocked={isDateBlocked}
        onDisableClicked={onDisableClicked}
      />,
    );
  });

  return (
    <div
      style={styles.month}
      //onTouchStart={onTouchStart}
      //onTouchEnd={onTouchEnd}
    >
      <div style={styles.week}>{dayNames}</div>
      {weeks}
    </div>
  );
};

export default CalendarMonth;

export const styles = {
  month: { marginBottom: 10 } as React.CSSProperties,
  week: {
    display: "flex",
    flexDirection: "row",
    marginTop: 12,
    marginBottom: 12,
  } as React.CSSProperties,
};
