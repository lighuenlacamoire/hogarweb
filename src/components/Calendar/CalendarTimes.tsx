import React from "react";
import { eachMinuteOfInterval, set } from "date-fns";
import { platform, textstyles } from "styles";
import { factoringDate } from "utils/formatter";

type Props = {
  currentDate: Date;
  onPress: (time: Date) => void;
};

const CalendarTimes = (props: Props): JSX.Element[] => {
  const { currentDate, onPress } = props;
  const timeSince = set(currentDate, { hours: 9, minutes: 0 });
  const timeUntil = set(currentDate, { hours: 18, minutes: 0 });

  const timeRange = eachMinuteOfInterval(
    { start: timeSince, end: timeUntil },
    { step: 30 },
  );

  const selectedTime = factoringDate(currentDate, "HH:mm");
  // Dias de la semana
  return timeRange.map((time) => {
    const showTime = factoringDate(time, "HH:mm");
    return (
      <div
        key={time.toISOString()}
        style={{
          paddingTop: "4px",
          paddingBottom: "4px",
          paddingInline: "8px",
          marginInlineEnd: "12px",
          borderRadius: "20px",
          ...(selectedTime === showTime
            ? { backgroundColor: platform.colors.primary }
            : { backgroundColor: platform.colors.formPrimaryBG }),
        }}
        onClick={() => onPress(time)}>
        <span
          style={{
            ...textstyles.textInputForm,
            ...(selectedTime === showTime
              ? { color: platform.colors.formPrimaryBG }
              : { color: "blue" }),
          }}>
          {showTime}
        </span>
      </div>
    );
  });
};

export default CalendarTimes;
