import React, { useState } from "react";
import Icon from "components/Icon";
import { factoringDate } from "utils/formatter";
import { containerStyles, platform, textstyles } from "styles";
import { View } from "components/ReactNative";

type Props = {
  defaultHeader: string;
  focusedMonth: Date;
  onPress: (value: number) => void;
};

const CalendarMonthSlide = (props: Partial<Props>) => {
  const { focusedMonth, defaultHeader, onPress } = props;
  const onValueChange = (value: number) => {
    if (onPress) {
      onPress(value);
    }
  };

  return (
    <View
      style={{
        ...containerStyles.itemSpacingElements,
        position: "relative",
        paddingTop: 10,
        paddingBottom: 10,
        paddingLeft: 10,
        paddingRight: 10,
      }}>
      <div
        onClick={() => onValueChange(-1)}
        style={{ paddingInline: "12px", cursor: "pointer" }}>
        <Icon size={22} name="chevron-left" color={platform.colors.text} />
      </div>
      <span
        style={{ ...textstyles.headerTitleStyle, color: platform.colors.text }}>
        {focusedMonth ? factoringDate(focusedMonth, "MMMM") : defaultHeader}
      </span>
      {defaultHeader ? (
        <div
          onClick={() => {
            onValueChange(0);
          }}
          style={{
            position: "absolute",
            right: 40,
            paddingInline: "12px",
            cursor: "pointer",
          }}>
          <Icon size={22} name="history" color={platform.colors.text} />
        </div>
      ) : null}
      <div
        onClick={() => onValueChange(1)}
        style={{ paddingInline: "12px", cursor: "pointer" }}>
        <Icon size={22} name="chevron-right" color={platform.colors.text} />
      </div>
    </View>
  );
};

export default CalendarMonthSlide;
