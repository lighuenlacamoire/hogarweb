import React from "react";
import { Modal, Button } from "react-bootstrap";

// Componentes
import CalendarBody from "./CalendarBody";
import { platform } from "styles";
import { DateBlockProp } from "interfaces/calendar";

// Config

type Props = {
  dateSplitter?: string;
  returnFormat?: string;
  outFormat?: string;
  headFormat?: string;
  headerTitle?: string;
  headerLegend?: string;
  placeholder?: string;
  mode: string;
  blockBefore?: DateBlockProp;
  blockAfter?: DateBlockProp;
  isVisible: boolean;
  onConfirm: (startDate?: Date, endDate?: Date) => void;
  onCancel: () => void;
  startDate?: Date;
  endDate?: Date;
};

const CalendarModal = (props: Props) => {
  const {
    dateSplitter,
    returnFormat,
    outFormat,
    mode,
    placeholder,
    blockBefore,
    blockAfter,
    headFormat,
    headerTitle,
    headerLegend,
    isVisible,
    onConfirm,
    onCancel,
    startDate,
    endDate,
  } = props;

  return (
    <Modal
      show={isVisible}
      animation={true}
      onHide={onCancel}
      style={styles.modalContainer}>
      <div style={styles.modalContent}>
        <CalendarBody
          headFormat={headFormat || ""}
          headerTitle={headerTitle || ""}
          headerLegend={headerLegend || ""}
          onConfirm={onConfirm}
          onCancel={onCancel}
          blockAfter={blockAfter}
          blockBefore={blockBefore}
          startDate={startDate}
          endDate={endDate}
          mode={mode}
        />
      </div>
    </Modal>
  );
};

export default CalendarModal;

export const styles = {
  placeholderText: {
    color: "#c9c9c9",
    fontSize: 18,
  } as React.CSSProperties,
  contentInput: {
    alignItems: "center",
    justifyContent: "center",
  } as React.CSSProperties,
  contentText: {
    fontSize: 18,
  } as React.CSSProperties,
  modalContainer: {
    justifyContent: "flex-start",
    marginHorizontal: 0,
    marginTop: 36,
  } as React.CSSProperties,
  modalContent: {
    marginHorizontal: 8,
    borderRadius: platform.generic.borderRadius,
    backgroundColor: platform.colors.listBackgroundLigher, // '#e0e0eb',
  } as React.CSSProperties,
  modalFooter: {
    paddingHorizontal: 15,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center", // alineacion vertical
    marginVertical: 10,
  } as React.CSSProperties,
  modalHighlightText: {
    textTransform: "uppercase",
    fontSize: 15,
    color: platform.colors.primary,
    marginHorizontal: 10,
  } as React.CSSProperties,
};
