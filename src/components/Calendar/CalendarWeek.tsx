/* eslint-disable @typescript-eslint/no-explicit-any */
import { DatesProp } from "interfaces/calendar";
import React from "react";
import CalendarWeekDays from "./CalendarWeekDays";

type Props = {
  mode: string;
  startDate?: Date;
  endDate?: Date;
  focusedInput?: string;
  focusedMonth: Date;
  startDayOfWeek: Date;
  onDatesChange: (event: DatesProp) => void;
  isDateBlocked: ((a: Date) => boolean | void) | undefined;
  onDisableClicked:
    | (<T>(a: T) => T | Promise<T> | void | Promise<void>)
    | undefined;
};

const CalendarWeek = (props: Props) => {
  const {
    mode,
    startDate,
    endDate,
    focusedInput,
    focusedMonth,
    startDayOfWeek,
    onDatesChange,
    isDateBlocked,
    onDisableClicked,
  } = props;
  const days: JSX.Element[] = CalendarWeekDays({
    mode,
    startDate,
    endDate,
    focusedInput,
    focusedMonth,
    startDayOfWeek,
    onDatesChange,
    isDateBlocked,
    onDisableClicked,
  });

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "row",
      }}>
      {days}
    </div>
  );
};

export default CalendarWeek;
