/* eslint-disable @typescript-eslint/ban-ts-comment */
// @ts-nocheck
import React, { useEffect, useState } from "react";

import * as scale from "d3-scale";
import * as shape from "d3-shape";
import { PieSlice } from "interfaces/graphics";
import { platform, textstyles } from "styles";
import PieItem from "./PieItem";

const d3 = {
  scale,
  shape,
};

type Props = {
  centerTitle: string;
  centerValue: string;
  size: number;
  onItemSelected: (item?: PieSlice, index?: number) => void;
  data: PieSlice[];
};

const emptyPie = [
  {
    id: "1",
    value: 100,
    data: "",
    name: "",
    color: platform.colors.placeholder,
  },
];

const Pie = (props: Props) => {
  const { onItemSelected, data, size, centerValue, centerTitle } = props;

  const [sliceSelected, setSliceSeleced] = useState<PieSlice>();

  const realSize = size + 140;
  const hasData = data && data.length > 0;
  console.log("pie", hasData);
  const newData: PieSlice[] = hasData ? data : emptyPie;
  const arcs = d3.shape.pie().value((x: PieSlice) => x.value)(newData);
  const hightlightedArc = d3.shape
    .arc()
    .outerRadius(size / 4 + 10)
    .padAngle(0.02)
    .innerRadius(50);

  const arc = d3.shape
    .arc()
    .outerRadius(size / 4 - 5)
    .padAngle(0.02)
    .innerRadius(50);

  const createPieArc = (item: PieSlice, index: number) => {
    const arcData = arcs[index];
    const startPath: string = arc(arcData);
    const endPath: string = hightlightedArc(arcData);

    return [startPath, endPath];
  };

  const onPieItemSelected = (item?: PieSlice, index?: number) => {
    setSliceSeleced(item);
    onItemSelected(item, index);
  };

  const onGenerateGraphic = () => {
    try {
      const resume = sliceSelected?.data || centerValue;
      const subtitle = sliceSelected?.name || centerTitle;
      const graphic = (
        <div
          className={"movement-pie-div"}
          style={{
            position: "relative",
            display: "flex",
            height: `${realSize}px`,
          }}>
          <span
            style={{
              ...textstyles.textBodyLegend,
              position: "absolute",
              display: "flex",
              top: "45%",
              left: "50%",
              transform: "translate(-50%, -55%)",
              height: "35px",
              fontSize: platform.fontSizes.LARGE,
              color: platform.colors.placeholder,
            }}>
            {subtitle}
          </span>
          <span
            style={{
              ...textstyles.textBodyLegend,
              position: "absolute",
              display: "flex",
              top: "52%",
              left: "50%",
              transform: "translate(-50%, -48%)",
              height: "35px",
              fontSize: platform.fontSizes.TITLE,
            }}>
            {resume}
          </span>
          <svg
            className={"movement-pie-svg"}
            style={{}}
            width={realSize}
            height={realSize}
            viewBox={"-120 -120 240 240"}>
            {newData.map((item, index) => {
              const outputRanges = createPieArc(item, index);

              return (
                <PieItem
                  key={`${index}${item.id}${item.name}`}
                  item={item}
                  index={index}
                  sliceSelected={sliceSelected}
                  outputRanges={outputRanges}
                  onPieItemSelected={onPieItemSelected}
                />
              );
            })}
          </svg>
        </div>
      );
      return graphic;
    } catch {
      return (
        <div
          style={{
            width: size,
            height: size,
            marginTop: "20px",
            justifyContent: "center",
            alignItems: "center",
          }}>
          <span style={textstyles.textBodyLegend}>Ups</span>
        </div>
      );
    }
  };

  useEffect(() => {
    setSliceSeleced(undefined);
  }, [data]);

  return (
    <div
      className="movement-pie-container"
      style={{ display: "flex", alignSelf: "center" }}>
      {onGenerateGraphic()}
      {hasData ? (
        <div
          style={{
            display: "flex",
            alignItems: "flex-start",
            flexDirection: "column",
            paddingTop: "20px",
          }}>
          {newData.map((item, index) => {
            const fontWeight = sliceSelected?.id == item.id ? "bold" : "normal";
            return (
              <div
                key={`${index}${item.id}${item.name}`}
                style={{
                  display: "flex",
                  marginBottom: "5px",
                  justifyContent: "center",
                  flexDirection: "row",
                  alignItems: "center",
                  cursor: "pointer",
                  marginVertical: 2,
                }}
                onClick={() =>
                  onPieItemSelected(
                    sliceSelected?.id === item.id ? undefined : item,
                    index,
                  )
                }>
                <div
                  style={{
                    width: "30px",
                    height: "30px",
                    borderRadius: "15px",
                    backgroundColor: item.color,
                    marginRight: "4px",
                    opacity:
                      sliceSelected && sliceSelected.id !== item.id ? 0.5 : 1,
                  }}
                />
                <span
                  style={{
                    ...textstyles.textInputForm,
                    fontWeight: fontWeight,
                  }}>
                  {`${item.name}: ${item.data} (${item.value}%)`}
                </span>
              </div>
            );
          })}
        </div>
      ) : null}
    </div>
  );
};
export default Pie;
