import React, { useEffect, useRef } from "react";

import { PieSlice } from "interfaces/graphics";

type Props = {
  item: PieSlice;
  index: number;
  onPieItemSelected: (item?: PieSlice, index?: number) => void;
  sliceSelected: PieSlice;
  outputRanges: string[];
};

const PieItem = (props: Props) => {
  const { onPieItemSelected, sliceSelected, item, outputRanges, index } = props;

  const renderSlice = () => {
    try {
      return (
        <path
          onClick={() => {
            /**
             * Selecciona la porcion y en caso de estar
             * ...seleccionada, vuelve al inicio
             */
            onPieItemSelected(
              sliceSelected?.id === item.id ? undefined : item,
              index,
            );
          }}
          d={sliceSelected?.id === item.id ? outputRanges[1] : outputRanges[0]}
          style={{
            cursor: "pointer",
            transition: "all 0.3s ease-in-out",
            ...(sliceSelected?.id === item.id
              ? {
                  transform: `matrix(${outputRanges[1]})`,
                }
              : { transform: `matrix(${outputRanges[0]})` }),
          }}
          fill={item.color}
          opacity={sliceSelected && sliceSelected.id !== item.id ? 0.5 : 1}
        />
      );
    } catch (er) {
      return (
        <path
          onClick={() => {
            /**
             * Selecciona la porcion y en caso de estar
             * ...seleccionada, vuelve al inicio
             */
            onPieItemSelected(
              sliceSelected?.id === item.id ? undefined : item,
              index,
            );
          }}
          d={outputRanges[sliceSelected?.id === item.id ? 1 : 0]}
          fill={item.color}
          opacity={sliceSelected && sliceSelected.id !== item.id ? 0.5 : 1}
        />
      );
    }
  };

  return renderSlice();
};
export default PieItem;
