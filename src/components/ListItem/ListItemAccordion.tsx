import { platform, textstyles, containerStyles } from "styles";
import React, { useRef, useState } from "react";
import Icon from "components/Icon";
import { View, Text } from "components/ReactNative";

type Props = {
  containerStyle: React.CSSProperties;
  content: JSX.Element | string;
  children: JSX.Element;
};
/**
 * divst Item tipo Accordion
 * @param {StyleProp<ViewStyle>} containerStyle Estilo para el boton
 * @param {JSX.Element | string} content Texto o contenido de tipo <Text />
 * @param {JSX.Element} children Contenido en el accordion
 */
const ListItemAccordion = (props: Partial<Props>) => {
  const { containerStyle, content, children } = props;
  const isComponent = typeof content !== "string";

  const [expanded, setExpanded] = useState(false);

  const toggleListItem = () => {
    setExpanded(!expanded);
  };

  return (
    <div
      style={{
        marginTop: "4px",
        border: containerStyles.shadowDefault.border,
        paddingInline: 0, // por si llega a estar en una row
        ...containerStyle,
      }}>
      <div
        style={{
          ...containerStyles.listItemContainer,
          cursor: "pointer",
          height: platform.generic.height,
          paddingInline: platform.generic.paddingSpaces,
        }}
        onClick={toggleListItem}>
        {isComponent ? (
          content
        ) : (
          <span style={textstyles.textBodyLegend}>{content}</span>
        )}
        <div
          style={{
            display: "flex",
            MozTransition: "all 0.4s ease-in-out",
            WebkitTransition: "all 0.4s ease-in-out",
            transition: "all 0.4s ease-in-out",
            ...(expanded
              ? { transform: "rotate(-180deg)" }
              : { transform: "rotate(0deg)" }),
          }}>
          <Icon name="chevron-down" size={24} color={platform.colors.text} />
        </div>
      </div>
      <div
        style={{
          overflow: "hidden",
          backgroundColor: platform.colors.listBackgroundLigher,
          ...(expanded
            ? {
                maxHeight: "500px",
                transition: "max-height 1s ease-in-out",
                MozTransition: "max-height 1s ease-in-out",
                WebkitTransition: "max-height 1s ease-in-out",
              }
            : {
                maxHeight: "0px",
                transition: "max-height 0.8s cubic-bezier(0, 1, 0, 1)",
                MozTransition: "max-height 0.8s cubic-bezier(0, 1, 0, 1)",
                WebkitTransition: "max-height 0.8s cubic-bezier(0, 1, 0, 1)",
              }),
        }}>
        {children}
      </div>
    </div>
  );
};
export default ListItemAccordion;
