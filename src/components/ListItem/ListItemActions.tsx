import { IconProps } from "interfaces/buttons";
import { textstyles, platform, containerStyles } from "styles";
import React from "react";
import { View, Text } from "components/ReactNative";
import Icon from "components/Icon";

type IconAction = IconProps & {
  onPress:
    | (<T>(value: T) => void)
    | ((e: React.MouseEvent<HTMLElement>) => void);
};

type Props = {
  containerStyle: React.CSSProperties;
  text: string;
  actions: JSX.Element[];
};

/**
 * Componente list item de acciones
 */
const ListItemActions = (props: Props): JSX.Element => {
  const { containerStyle, text, actions } = props;

  return (
    <div
      style={{
        ...containerStyles.listItemContainer,
        ...containerStyle,
      }}>
      <Text
        style={{
          ...textstyles.textBodyLegend,
          fontSize: platform.fontSizes.MEDIUM,
        }}>
        {text}
      </Text>
      <div style={{ display: "flex" }}>{actions}</div>
    </div>
  );
};

export default ListItemActions;
