import { ImageProps } from "interfaces/buttons";
import { textstyles, platform, containerStyles } from "styles";
import React from "react";
import { Text } from "components/ReactNative";
import Icon from "components/Icon";
import { CheckBox, RadioBox } from "components/Check";

type Props = {
  containerStyle: React.CSSProperties;
  content: JSX.Element | string;
  onPress: <T>(value: T) => void;
  onLongPress: <T>(value: T) => void;
  right: JSX.Element;
  icon: Partial<ImageProps>;
};

/**
 * Componente list item
 */
const ListItemContent = (props: Partial<Props>): JSX.Element => {
  const { containerStyle, content, icon, right, onPress, onLongPress } = props;
  const isComponent = typeof content !== "string";

  return (
    <div
      style={{
        ...containerStyles.listItemContainer,
        cursor: "pointer",
        ...containerStyle,
      }}
      onClick={onPress}
      onDoubleClick={onLongPress}>
      {isComponent ? (
        content
      ) : (
        <Text style={textstyles.textBodyLegend}>{content}</Text>
      )}
      {right}
      {icon && icon.name ? (
        <Icon
          size={icon?.size || 24}
          color={icon?.color || platform.colors.text}
          name={icon?.name}
          isSvg={icon?.isSvg}
        />
      ) : null}
    </div>
  );
};

export default ListItemContent;
