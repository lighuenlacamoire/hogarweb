import { ImageProps } from "interfaces/buttons";
import React from "react";
//import IconSVG from "./IconSVG";
import IconTTF from "./IconTTF";
import iconset from "../../assets/fonts/tabler-icons.json";
/**
 * Icono generico
 * @param {string} name Nombre
 * @param {string} color color
 * @param {number} size size
 * @param {boolean} isSvg es SVG true o usa fontawesome false
 */
const Icon = (props: Partial<ImageProps>) => {
  const { name = "plus", size = 24, color = "#333", isSvg = false } = props;

  /*if (isSvg) {
    const IconCustom = IconSVG[name];
    const iconColor = color.toString();
    if (IconCustom) {
      return <IconCustom color={iconColor} fontSize={size} />;
    }
  }
  */
  return (
    <IconTTF
      iconSet={iconset}
      icon={name}
      color={color.toString()}
      size={size}
    />
  );

  /*
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width="48"
      height="48"
      viewBox="0 0 48 48">
      <path
        fill="#4caf50"
        d="M44,24c0,11.045-8.955,20-20,20S4,35.045,4,24S12.955,4,24,4S44,12.955,44,24z"></path>
      <path fill="#fff" d="M21,14h6v20h-6V14z"></path>
      <path fill="#fff" d="M14,21h20v6H14V21z"></path>
    </svg>
  );
  */
  //return <FontAwesomeIcon name={name} color={color} size={size} {...props} />;
};

export default Icon;
