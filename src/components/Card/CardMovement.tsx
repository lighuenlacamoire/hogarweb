import {
  BalanceMovement,
  BalanceSource,
  BalanceTransactionType,
} from "interfaces/state";
import { platform, textstyles, containerStyles } from "styles";
import React, { useState } from "react";
import {
  calendarSettings,
  Pages,
  transactionTypes,
} from "configuration/constants";
import { factoringDate } from "utils/formatter";
import { useNavigate } from "react-router-dom";

type Props = {
  movement: BalanceMovement;
  sources?: BalanceSource[];
  categories?: BalanceSource[];
};

/**
 * Card de Movimiento
 */
const CardMovement = (props: Props) => {
  const { movement, sources = [], categories = [] } = props;
  const [borderColor, setBorderColor] = useState(
    platform.colors.borderContainer,
  );
  const [borderWidth, setBorderWidth] = useState(platform.generic.borderWidth);

  const transactionSelected = transactionTypes.find(
    (item) => item.id === movement.transactionType,
  );
  /** Navegacion */
  const navigate = useNavigate();

  const itsExpense =
    transactionSelected?.id === BalanceTransactionType.EXPENSE ? true : false;
  let creditSelected: BalanceSource | undefined = undefined;
  let debitSelected: BalanceSource | undefined = undefined;
  let tagColor: string | undefined = undefined;
  const styleTextColor: React.CSSProperties = itsExpense
    ? {}
    : { color: platform.colors.white };

  if (transactionSelected?.id === BalanceTransactionType.EXPENSE) {
    creditSelected = categories.find((item) => item.id === movement.creditId);
    debitSelected = sources.find((item) => item.id === movement.debitId);
    tagColor = creditSelected?.color;
  } else {
    creditSelected = sources.find((item) => item.id === movement.creditId);
    debitSelected = sources.find((item) => item.id === movement.debitId);
    tagColor =
      transactionSelected?.id === BalanceTransactionType.INCOME
        ? platform.colors.incomeAmount
        : platform.colors.transferAmount;
  }

  /**
   * Modifica el borde en la card, ancho y color
   * @param {boolean} pressed Indica si fue presionado o no
   * @param {string} value Color para el borde
   */
  const ChangeBorder = (pressed: boolean, value?: string) => {
    const vColor =
      itsExpense && value ? value : platform.colors.borderContainer;
    setBorderColor(vColor);
    setBorderWidth(pressed ? "5px" : platform.generic.borderWidth);
  };

  return (
    <div
      onClick={() =>
        navigate(`${Pages.MOVEMENTDETAILPAGE}/${movement.id}`, {
          state: movement,
        })
      }
      style={{
        ...containerStyles.shadowDefault,
        maxWidth: "380px",
        width: "360px",
        height: "75px",
        position: "relative",
        cursor: "pointer",
        borderWidth: borderWidth,
        borderColor: borderColor,
        backgroundColor: itsExpense ? platform.colors.white : tagColor,
      }}>
      {itsExpense ? (
        <div
          style={{
            height: "inherit",
            width: "10px",
            position: "absolute",
            backgroundColor: creditSelected?.color,
          }}
        />
      ) : null}
      <div style={{ paddingInline: "14px" }}>
        <div
          style={{
            ...containerStyles.itemSpacingElements,
            position: "relative",
          }}>
          <span
            style={{
              ...textstyles.textBodyLegend,
              ...styleTextColor,
              letterSpacing: "0",
              fontSize: platform.fontSizes.MEDIUM,
              width: "30%",
            }}>
            {creditSelected?.name}
          </span>
          <span
            style={{
              ...textstyles.textInputForm,
              ...styleTextColor,
              fontSize: platform.fontSizes.SMALL,
              position: "absolute",
              left: "42%",
            }}>
            {factoringDate(
              movement.dateNumber,
              calendarSettings.formats.showDate,
            )}
          </span>
          <span
            style={{
              ...textstyles.textInputForm,
              color: platform.colors.placeholder,
              ...styleTextColor,
            }}>
            {debitSelected?.name}
          </span>
        </div>
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
          }}>
          <span
            style={{
              ...textstyles.textInputForm,
              flex: 1,
              ...styleTextColor,
            }}>
            {movement.detail}
          </span>
          <span
            style={{
              ...textstyles.textBodyLegend,
              color: transactionSelected?.color,
              ...styleTextColor,
            }}>{`$ ${movement.amount}`}</span>
        </div>
      </div>
    </div>
  );
};

export default CardMovement;
