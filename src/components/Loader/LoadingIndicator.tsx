import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { ThreeCircles } from "react-loader-spinner";
import { RootState } from "redux/store";
import { platform } from "styles";

const LoadingIndicator = () => {
  /** Dispatch de Redux */
  const { loading } = useSelector((state: RootState) => state.status);
  const [activity, setActivity] = useState(loading);
  /**
   * Verifica si existe algun elemento cargando
   *
   */
  const isLoading = () => {
    setActivity(loading);
  };

  useEffect(() => {
    isLoading();
  }, [loading]);
  /*

.loading-overlay {
  align-items: center;
  position: fixed;
  width: 100%;
  height: 100%;
  left: 0;
  top: 0;
  z-index: 1;
}

.loading-container {
  height: 100%;
  background-color: rgba(0, 0, 0, 0.2);
  display: flex;
  align-items: center;
  justify-content: center;
}

*/
  return activity ? (
    <div
      style={{
        height: "100%",
        width: "100%",
        alignItems: "center",
        position: "fixed",
        left: 0,
        top: 0,
        zIndex: 9,
      }}>
      <div
        style={{
          height: "100%",
          alignItems: "center",
          justifyContent: "center",
          display: "flex",
          backgroundColor: "rgba(0, 0, 0, 0.2)",
        }}>
        <ThreeCircles
          color={platform.colors.primary}
          wrapperStyle={{}}
          wrapperClass=""
          visible={activity}
          ariaLabel="three-circles-rotating"
          outerCircleColor=""
          innerCircleColor=""
          middleCircleColor=""
        />
      </div>
    </div>
  ) : null;
};

export default LoadingIndicator;
