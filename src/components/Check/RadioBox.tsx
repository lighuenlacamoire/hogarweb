import React from "react";
import { platform } from "styles";

type Props = {
  size?: number;
  color?: string;
  checked: boolean;
  onPress?:
    | (<T>(value: T) => void)
    | ((e: React.MouseEvent<HTMLElement>) => void);
  onChange?:
    | (<T>(value: T) => void)
    | ((e: React.ChangeEvent<HTMLElement>) => void);
};

const RadioBox = ({ checked, size, color, onPress, onChange }: Props) => {
  const vSize = `${size ?? 20}px`;
  const vColor = color ?? platform.colors.primary;

  return (
    <svg
      width={vSize}
      height={vSize}
      viewBox="0 0 20 20"
      style={{
        fill: "none",
        verticalAlign: "middle",
      }}>
      <circle
        cx="10"
        cy="10"
        r="9"
        style={{
          strokeWidth: 2,
          stroke: "#C8CCD4",
        }}></circle>
      <path
        d="M10,7 C8.34314575,7 7,8.34314575 7,10 C7,11.6568542 8.34314575,13 10,13 C11.6568542,13 13,11.6568542 13,10 C13,8.34314575 11.6568542,7 10,7 Z"
        style={{
          stroke: vColor,
          strokeWidth: 6,
          strokeDasharray: 19,
          transition: "all 0.4s ease",
          transitionDelay: "0.3s",
          ...(checked ? { strokeDashoffset: 38 } : { strokeDashoffset: 19 }),
        }}></path>
      <path
        d="M10,1 L10,1 L10,1 C14.9705627,1 19,5.02943725 19,10 L19,10 L19,10 C19,14.9705627 14.9705627,19 10,19 L10,19 L10,19 C5.02943725,19 1,14.9705627 1,10 L1,10 L1,10 C1,5.02943725 5.02943725,1 10,1 L10,1 Z"
        style={{
          stroke: vColor,
          strokeWidth: 2,
          strokeDasharray: 57,
          transition: "all 0.4s ease",
          ...(checked ? { strokeDashoffset: 0 } : { strokeDashoffset: 57 }),
        }}></path>
    </svg>
  );
};

export default RadioBox;
