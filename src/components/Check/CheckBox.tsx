import React from "react";
import { platform } from "styles";

type Props = {
  size?: number;
  color?: string;
  checked: boolean;
  onPress?:
    | (<T>(value: T) => void)
    | ((e: React.MouseEvent<HTMLElement>) => void);
  onChange?:
    | (<T>(value: T) => void)
    | ((e: React.ChangeEvent<HTMLElement>) => void);
};

const CheckBox = ({ checked, size, color, onPress, onChange }: Props) => {
  const vSize = `${size ?? 20}px`;
  const vColor = color ?? platform.colors.primary;

  const handleChange = (e: React.ChangeEvent<HTMLElement>) => {
    if (onChange) {
      onChange(e);
    }
  };

  return (
    <svg width={vSize} height={vSize} viewBox="0 0 24 24">
      <g
        fill="none"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeWidth="2">
        <rect
          style={{
            transition: "stroke 0.3s",
            ...(checked ? { stroke: vColor } : { stroke: "#C8CCD4" }),
          }}
          x="1"
          y="1"
          width="22"
          height="22"
        />
        <polyline
          style={{
            stroke: vColor,
            strokeDasharray: "30 146",
            transition:
              "stroke 0.3s, stroke-dashoffset 0.6s cubic-bezier(0.42,-0.2,0.58,1.2)",
            ...(checked
              ? { strokeDashoffset: "-91" }
              : { strokeDashoffset: "30" }),
          }}
          points="23,1 1,1 1,23 23,23 23,4"
        />
        <polyline
          style={{
            stroke: vColor,
            strokeDasharray: "17.38 149.68",
            transition:
              "stroke 0.3s, stroke-dashoffset 0.6s cubic-bezier(0.42,-0.2,0.58,1.2)",
            ...(checked
              ? { strokeDashoffset: "-6" }
              : { strokeDashoffset: "103.38" }),
          }}
          points="23,4 10,17 5,12 18,12"
        />
      </g>
    </svg>
  );
};

export default CheckBox;
