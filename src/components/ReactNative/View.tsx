import React from "react";
import { reactnativeStyles } from "styles";

type Props = {
  style?: React.CSSProperties;
  children?: React.ReactNode;
};

/**
 * React-Native View
 * @param {React.CSSProperties} style Estilos
 * @param {React.ReactNode} children Contenido
 * @returns JSX.element
 */
const View = ({ style, children }: Props) => {
  return (
    <div style={{ ...reactnativeStyles.viewContainer, ...style }}>
      {children}
    </div>
  );
};

export default View;
