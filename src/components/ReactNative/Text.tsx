import React from "react";

type Props = {
  style?: React.CSSProperties;
  className?: string;
  children?: React.ReactNode | string;
};

const Text = ({ style, className, children }: Props) => {
  return (
    <span className={className} style={style}>
      {children}
    </span>
  );
};

export default Text;
