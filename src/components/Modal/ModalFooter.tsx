import { ButtonReactive } from "components/Button";
import { genericMessages } from "configuration/messages";
import { ModalActions } from "interfaces/buttons";
import React from "react";
import { Modal } from "react-bootstrap";
import { containerStyles } from "styles";

type Props = {
  actions?: ModalActions;
};

/**
 * Modal: footer
 */
const ModalFooter = (props: Props): JSX.Element => {
  const { actions } = props;

  return (
    <Modal.Footer style={containerStyles.footerDouble}>
      {actions?.primary ? (
        <ButtonReactive
          content={actions?.primary?.content || genericMessages.accept}
          onPress={actions?.primary?.onPress}
        />
      ) : null}
      {actions?.secondary ? (
        <ButtonReactive
          inverse
          content={actions?.secondary?.content || genericMessages.cancel}
          onPress={actions?.secondary?.onPress}
        />
      ) : null}
    </Modal.Footer>
  );
};
export default ModalFooter;
