import { ButtonReactive } from "components/Button";
import { View } from "components/ReactNative";
import { genericMessages } from "configuration/messages";
import { ModalMessage } from "interfaces/buttons";
import React from "react";
import { Modal, Button } from "react-bootstrap";
import { containerStyles, textstyles } from "styles";

/**
 * Modal generico
 */
const ModalPopUp = (props: ModalMessage) => {
  const {
    header,
    title,
    content,
    selfClose,
    isVisible,
    onBackdropPress,
    actions,
  } = props;

  const itsString = content && typeof content === "string";
  const hasFooter = actions && Object.keys(actions).length > 0;
  return (
    <Modal animation={true} show={isVisible} onHide={onBackdropPress} centered>
      <Modal.Header
        closeButton
        style={{ backgroundColor: "var(--color-primary)" }}>
        {header && typeof header !== "string" ? (
          <View
            style={{
              flexDirection: "row",
              alignItems: "flex-start",
              marginBottom: 8,
            }}>
            <View style={{ flex: 1 }}>
              {header}
              {title ? (
                <label style={textstyles.textBodyLegend}>{title}</label>
              ) : null}
            </View>
          </View>
        ) : (
          <label style={textstyles.headerTitleStyle}>{title}</label>
        )}
      </Modal.Header>
      <Modal.Body className="d-flex justify-content-center">
        {itsString ? (
          <label
            style={{
              ...textstyles.textInputForm,
              marginBottom: "var(--generic-space-container)",
            }}>
            {content}
          </label>
        ) : (
          content
        )}
      </Modal.Body>
      {hasFooter ? (
        <Modal.Footer style={containerStyles.footerDouble}>
          {actions?.primary ? (
            <ButtonReactive
              content={actions?.primary?.content || genericMessages.accept}
              onPress={actions?.primary?.onPress}
            />
          ) : null}
          {actions?.secondary ? (
            <ButtonReactive
              inverse
              content={actions?.secondary?.content || genericMessages.cancel}
              onPress={actions?.secondary?.onPress}
            />
          ) : null}
        </Modal.Footer>
      ) : null}
    </Modal>
  );
};

export default ModalPopUp;
