import { Pages } from "configuration/constants";
import {
  authenticationMessages,
  genericMessages,
} from "configuration/messages";
import { ModalMessage } from "interfaces/buttons";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { logoutExecute } from "redux/actions/authorization";
import { clearMessage, setMessage } from "redux/actions/notification";
import { RootState } from "redux/store";
import { platform, textstyles } from "styles";
import { deleteItem } from "utils/storage";

type SidebarProps = { expanded: boolean };

const Sidebar = ({ expanded }: SidebarProps): JSX.Element => {
  /** Dispatch de Redux */
  const dispatch = useDispatch();
  /** Navegacion */
  const navigate = useNavigate();
  const { accessToken } = useSelector(
    (state: RootState) => state.authorization,
  );
  const isMobile = window.innerWidth < 500;
  const spanStyle: React.CSSProperties = {
    ...textstyles.headerTitleStyle,
    fontSize: platform.fontSizes.LARGE,
    textTransform: "capitalize",
  };
  const astyle: React.CSSProperties = {
    width: "100%",
    backgroundColor: "var(--color-sidebar)",
    textDecoration: "none",
    padding: "1rem var(--generic-space-components)",
    cursor: "pointer",
    display: "flex",
  };

  const logout = () => {
    const newMessage: ModalMessage = {
      title: genericMessages.requestConfirmation.title,
      content: genericMessages.requestConfirmation.text,
      selfClose: false,
      isVisible: true,
      actions: {
        primary: {
          content: genericMessages.yes,
          onPress: () => {
            dispatch(clearMessage());
            dispatch(logoutExecute());
            deleteItem("@accessTokenGoogle");
          },
        },
        secondary: {
          content: genericMessages.no,
          onPress: () => {
            dispatch(clearMessage());
          },
        },
      },
    };
    dispatch(setMessage(newMessage));
  };

  return (
    <div className={`sidebar-menu${expanded ? " open" : ""}`}>
      <Link style={astyle} to={Pages.HOMEPAGE}>
        <span style={spanStyle}>Home</span>
      </Link>
      <Link style={astyle} to={Pages.ACCOUNTSTATEMENTSPAGE}>
        <span style={spanStyle}>Estado de cuenta</span>
      </Link>
      <Link style={astyle} to={Pages.MOVEMENTSTATISTICSPAGE}>
        <span style={spanStyle}>Estadisticas</span>
      </Link>
      <Link style={astyle} to={Pages.MOVEMENTLISTPAGE}>
        <span style={spanStyle}>Movimientos</span>
      </Link>
      <a
        style={astyle}
        onClick={() =>
          navigate(`${Pages.MOVEMENTDETAILPAGE}/0`, {
            state: {},
          })
        }>
        <span style={spanStyle}>Nuevo Movimiento</span>
      </a>
      <Link style={astyle} to={Pages.SOURCELISTPAGE}>
        <span style={spanStyle}>Cajas</span>
      </Link>
      <a
        style={astyle}
        onClick={() =>
          navigate(`${Pages.SOURCEDETAILPAGE}/0`, {
            state: {},
          })
        }>
        <span style={spanStyle}>Nueva Caja</span>
      </a>
      <Link style={astyle} to={Pages.CATEGORYLISTPAGE}>
        <span style={spanStyle}>Gastos</span>
      </Link>
      <a
        style={astyle}
        onClick={() =>
          navigate(`${Pages.CATEGORYDETAILPAGE}/0`, {
            state: {},
          })
        }>
        <span style={spanStyle}>Nuevo gasto</span>
      </a>
      <Link style={astyle} to={Pages.CATALOGLISTPAGE}>
        <span style={spanStyle}>Lista</span>
      </Link>
      <Link style={astyle} to="/projects">
        <span style={spanStyle}>Projects</span>
      </Link>
      {accessToken && (
        <a style={astyle} onClick={logout}>
          <span style={spanStyle}>{authenticationMessages.logout}</span>
        </a>
      )}
    </div>
  );
};

export default Sidebar;
