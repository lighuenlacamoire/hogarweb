import React from "react";

interface MainProps {
  children?: React.ReactNode;
}

const Main = ({ children }: MainProps): JSX.Element => {
  return <div className="Main">{children}</div>;
};

export default Main;
