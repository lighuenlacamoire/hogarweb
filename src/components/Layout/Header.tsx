import React, { useState } from "react";

type HeaderProps = {
  onClick: () => void;
  expanded: boolean;
};

const Header = ({ onClick, expanded }: HeaderProps): JSX.Element => {
  const expandedClass = `Headerbar-Icon ${expanded ? "open" : ""}`;
  return (
    <header className="Headerbar">
      <div
        style={{
          width: "50px",
          marginLeft: "8px",
          border: "1px solid rgba(255,255,255,0.3)",
          justifyContent: "center",
          alignItems: "center",
          display: "flex",
          paddingTop: "3px",
          boxShadow: "0 2px 10px 0 rgba(0,0,0,0.3)",
        }}>
        <div className={expandedClass} onClick={onClick}>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    </header>
  );
};
export default Header;
