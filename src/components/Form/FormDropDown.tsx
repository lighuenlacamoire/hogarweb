import Icon from "components/Icon";
import { genericMessages } from "configuration/messages";
import React, { useState } from "react";
import { containerStyles, platform, textstyles } from "styles";
import { setListActive } from "utils/functions";

type OptionProp = any & {
  id: number | string;
  name: string;
  active: boolean;
};

type Props = {
  isAccordion: boolean;
  multiSelect: boolean;
  placeholder: string;
  options: OptionProp[];
  onAction: () => void;
  onOptionsChange: (options: OptionProp[]) => void;
  error: string;
};

const FormDropDown = (props: Partial<Props>) => {
  const {
    isAccordion,
    placeholder,
    options,
    onAction,
    onOptionsChange,
    error,
  } = props;

  const selected = options?.find((item) => item.active);

  const [expanded, setExpanded] = useState(false);

  const toggleListItem = () => {
    setExpanded(!expanded);
  };

  return (
    <div style={containerStyles.formControl}>
      <div
        style={{
          ...containerStyles.listItemContainer,
          cursor: "pointer",
          paddingInline: platform.generic.paddingSpaces,
        }}
        onClick={toggleListItem}>
        <span
          style={{
            ...textstyles.textInputForm,
            color: selected?.name
              ? platform.colors.text
              : platform.colors.placeholder,
          }}>
          {selected?.name || placeholder}
        </span>
        <div
          style={{
            display: "flex",
            MozTransition: "all 0.4s ease-in-out",
            WebkitTransition: "all 0.4s ease-in-out",
            transition: "all 0.4s ease-in-out",
            ...(expanded
              ? { transform: "rotate(-180deg)" }
              : { transform: "rotate(0deg)" }),
          }}>
          <Icon name="chevron-down" size={24} color={platform.colors.text} />
        </div>
      </div>
      <div
        style={{
          position: "absolute",
          top: "100%",
          left: 0,
          width: "100%",
          marginTop: "4px",
          zIndex: 1,
          boxShadow: "0 5px 10px 0 rgba(152, 152, 152, 0.6)",
          ...(expanded
            ? {
                maxHeight: "300px",
                overflow: "auto",
                transition: "max-height 1s ease-in-out",
                MozTransition: "max-height 1s ease-in-out",
                WebkitTransition: "max-height 1s ease-in-out",
              }
            : {
                maxHeight: "0px",
                overflow: "hidden",
                transition: "max-height 0.8s cubic-bezier(0, 1, 0, 1)",
                MozTransition: "max-height 0.8s cubic-bezier(0, 1, 0, 1)",
                WebkitTransition: "max-height 0.8s cubic-bezier(0, 1, 0, 1)",
              }),
          /*
  transform: scaleY(0);
  transform-origin: top;
  font-weight: 400;
  transition: 0.2s ease-in-out;
    */
        }}>
        {onAction ? (
          <div
            onClick={() => {
              toggleListItem();
              onAction();
            }}
            style={{
              ...containerStyles.listItemContainer,
              cursor: "pointer",
              backgroundColor: platform.colors.white,
              paddingInline: platform.generic.paddingSpaces,
              height: "40px",
              borderBottom: "1px solid #d6d6d6",
            }}>
            <span
              style={{
                ...textstyles.textBodyLegend,
                color: platform.colors.primary,
              }}>
              {options && options.length > 0
                ? genericMessages.manage
                : genericMessages.create}
            </span>
            <Icon
              size={22}
              name={options && options.length > 0 ? "search" : "plus"}
              color={platform.colors.primary}
            />
          </div>
        ) : null}
        {options?.map((item, index) => (
          <div
            key={`${index}${item.id}`}
            onClick={() => {
              if (onOptionsChange) {
                setListActive(options ?? [], onOptionsChange, false, index);
              }
              toggleListItem();
            }}
            style={{
              cursor: "pointer",
              display: "flex",
              paddingInline: platform.generic.paddingSpaces,
              height: "40px",
              alignItems: "center",
              borderBottom: "1px solid #d6d6d6",
              transition: "0.3s",
              ...(item.id === selected?.id
                ? {
                    backgroundColor: platform.colors.primary,
                    color: platform.colors.white,
                  }
                : {
                    backgroundColor: platform.colors.white,
                    color: platform.colors.text,
                  }),
            }}>
            {item.name}
          </div>
        ))}
      </div>
    </div>
  );
};

export default FormDropDown;
