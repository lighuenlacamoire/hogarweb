import { CalendarModal } from "components/Calendar";
import Icon from "components/Icon";
import { calendarSettings } from "configuration/constants";
import { platform, textstyles } from "styles";
import { factoringDate } from "utils/formatter";
import React, { useState } from "react";

type Props = {
  containerStyle: React.CSSProperties;
  style: React.CSSProperties;
  header: string;
  name: string;
  legend: string;
  value?: Date;
  onValueChange: (value?: Date) => void;
  placeholder: string;
  disabled: boolean;
  message: string;
  success: string;
  error: string;
};

/**
 * Componente de ItemInput
 * @param {React.CSSProperties} containerStyle
 * @param {React.CSSProperties} style
 * @param {string} header
 * @param {string} name
 * @param {string} legend
 * @param {Date?} value
 * @param {Date} onValueChange
 * @param {string} placeholder
 * @param {boolean} disabled
 * @param {string} message
 * @param {string} success
 * @param {string} error
 */
const ItemCalendar = (props: Partial<Props>): JSX.Element => {
  const {
    containerStyle,
    header,
    legend,
    value,
    onValueChange,
    placeholder,
    style,
    disabled,
    message,
    success,
    error,
  } = props;
  const [calendarSpawn, setCalendarSpawn] = useState(false);

  return (
    <div style={{ ...containerStyle, flex: 1 }}>
      <CalendarModal
        headFormat="MMMM dd" // formato fecha en el modal
        // returnFormat={calendarSettings.formats.formInput} // formato de fecha devuelto en onconfirm
        outFormat={calendarSettings.formats.formInput} // formato fecha en el input
        startDate={value}
        mode={calendarSettings.mode.single}
        onConfirm={(startDate?: Date) => {
          if (onValueChange) {
            onValueChange(startDate);
          }
          setCalendarSpawn(false);
        }}
        onCancel={() => setCalendarSpawn(false)}
        isVisible={calendarSpawn}
      />
      {header ? (
        <span
          style={{
            ...textstyles.textInputForm,
            color: platform.colors.text,
            ...(error ? { color: platform.colors.error } : {}),
          }}>
          {header}
        </span>
      ) : null}
      {legend ? (
        <span
          style={{
            ...textstyles.textInputForm,
            color: platform.colors.text,
            ...(error ? { color: platform.colors.error } : {}),
          }}>
          {legend}
        </span>
      ) : null}
      <div
        onClick={() => setCalendarSpawn(true)}
        style={{
          marginTop: "4px",
          flexDirection: "row",
          height: "48px",
          cursor: "pointer",
          borderRadius: platform.generic.borderRadius,
          borderWidth: platform.generic.borderWidth,
          borderColor: platform.colors.text,
          paddingInline: platform.generic.paddingSpaces,
          alignItems: "center",

          ...style,
          ...(success ? { borderColor: platform.colors.success } : {}),
          ...(error ? { borderColor: platform.colors.error } : {}),
          ...(disabled ? { backgroundColor: platform.colors.secondary } : {}),
        }}>
        <span
          style={{
            ...textstyles.textInputForm,
            color: value ? platform.colors.text : platform.colors.placeholder,
          }}>
          {value
            ? factoringDate(value, calendarSettings.formats.formInput)
            : placeholder}
        </span>
      </div>
      {message ? (
        <span
          style={{
            ...textstyles.textCaption,
            color: platform.colors.border,
            // textAlignVertical: "center",
          }}>
          {message}
        </span>
      ) : null}
      {error && error.length > 0 ? (
        <div style={{ flexDirection: "row", alignItems: "center" }}>
          <Icon name="warning-circle" size={14} color={platform.colors.error} />
          <span
            style={{
              ...textstyles.textCaption,
              marginLeft: 5,
              color: platform.colors.error,
              //textAlignVertical: "center",
            }}>
            {error}
          </span>
        </div>
      ) : null}
    </div>
  );
};

export default ItemCalendar;
