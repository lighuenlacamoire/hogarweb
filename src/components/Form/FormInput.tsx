import React, { ChangeEvent, ChangeEventHandler, RefObject } from "react";
import { containerStyles, platform, textstyles } from "styles";
import { restrictNumber } from "utils/formatter";

type Props = {
  name: string;
  value: string;
  onValueChange: (value: string) => void;
  maxLength: number;
  placeholder: string;
  isNumeric: boolean;
  disabled: boolean;
  success: string;
  error: string;
};
/**
 * Input de formulario
 */
const FormInput = (props: Partial<Props>) => {
  const {
    value,
    onValueChange,
    maxLength,
    placeholder,
    isNumeric,
    disabled,
    success,
    error,
  } = props;

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    if (onValueChange) {
      onValueChange(
        isNumeric ? restrictNumber(event.target.value) : event.target.value,
      );
      //onValueChange(event.target.value);
    }
  };

  return (
    <div style={containerStyles.formControl}>
      <input
        className="form-control-input"
        placeholder={placeholder}
        value={value}
        style={{
          ...textstyles.textInputForm,
          border: 0,
          outline: 0,
          outlineOffset: 0,
          width: "100%",
          paddingInline: platform.generic.paddingSpaces,
          color:
            value && value.length > 0
              ? platform.colors.text
              : platform.colors.placeholder,
        }}
        onChange={handleChange}
      />
    </div>
  );
};

export default FormInput;
