import Icon from "components/Icon";
import React from "react";
import { platform, textstyles } from "styles";

type Props = {
  children: React.ReactNode;
  header?: string;
  icon?: string;
  style?: React.CSSProperties;
  className?: string;
};

const FormContainer = ({ header, icon, children, style, className }: Props) => {
  return (
    <div
      style={{
        paddingInline: platform.generic.containerSpaces,
        backgroundColor: platform.colors.formSecondaryBG,
        flex: 1,
        display: "flex",
        justifyContent: "center",
      }}>
      <div
        className={className ?? "col-xs-12 col-md-10 col-lg-10"}
        style={{
          margin: "40px auto",
          ...style,
        }}>
        {header ? (
          <div
            style={{
              padding: platform.generic.paddingSpaces,
              alignItems: "center",
              backgroundColor: platform.colors.primary,
              display: "flex",
              boxShadow: "0 1px 5px rgba(0,0,0,0.65)",
              borderRadius: "4px 4px 0 0",
              borderTop: "1px solid #23e0ba",
              borderBottom: "5px solid #16a085",
            }}>
            {icon ? (
              <Icon name={icon} size={24} color={platform.colors.white} />
            ) : null}
            <label
              style={{
                ...textstyles.headerTitleStyle,
                ...(icon ? { marginLeft: platform.generic.paddingSpaces } : {}),
              }}>
              {header}
            </label>
          </div>
        ) : null}
        <div
          style={{
            padding: "24px",
            height: "fit-content",
            background: "var(--color-white)",
            border: "1px solid #e1e2f0",
            boxShadow:
              "0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24)",
            transition: "all 0.3s ease",
          }}>
          {children}
        </div>
      </div>
    </div>
  );
};
export default FormContainer;
