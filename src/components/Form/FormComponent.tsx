import React from "react";
import { platform, textstyles } from "styles";

type Props = {
  header: string;
  name: string;
  legend: string;
  message: string;
  success: string;
  error: string;
  children: React.ReactNode;
};

const FormComponent = (props: Partial<Props>) => {
  const { header, legend, message, success, error, children } = props;

  return (
    <div
      style={{
        display: "grid",
        boxSizing: "border-box",
        marginTop: platform.generic.paddingSpaces,
      }}>
      <label
        style={{
          ...textstyles.textInputForm,
          fontWeight: "500",
          color: platform.colors.text,
        }}>
        {header}
      </label>
      {children}
    </div>
  );
};

export default FormComponent;
