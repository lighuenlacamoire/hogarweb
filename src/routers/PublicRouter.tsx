import { Pages } from "configuration/constants";
import React from "react";
import { useSelector } from "react-redux";
import { useNavigate, useLocation, Navigate, Outlet } from "react-router-dom";
import { RootState } from "redux/store";

const PublicRouter = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const { accessToken } = useSelector(
    (state: RootState) => state.authorization,
  );
  return accessToken ? <Navigate to={Pages.HOMEPAGE} replace /> : <Outlet />;
};

export default PublicRouter;
