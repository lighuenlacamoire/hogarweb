import { Pages } from "configuration/constants";
import React from "react";
import { useSelector } from "react-redux";
import { useNavigate, useLocation, Navigate, Outlet } from "react-router-dom";
import { RootState } from "redux/store";

const PrivateRouter = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const { accessToken } = useSelector(
    (state: RootState) => state.authorization,
  );
  return accessToken ? <Outlet /> : <Navigate to={Pages.LOGINPAGE} replace />;
};

export default PrivateRouter;
