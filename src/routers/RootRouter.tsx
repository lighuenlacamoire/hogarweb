import { Pages } from "configuration/constants";
import {
  CategoryListPage,
  CategoryDetailPage,
  MovementDetailPage,
  MovementListPage,
  MovementStatisticsPage,
  AccountStatementsPage,
  SourceDetailPage,
  SourceListPage,
} from "pages/Balance";
import { CatalogListPage } from "pages/Catalog";
import { NotFoundPage } from "pages/Error";
import { SettingsDetailPage } from "pages/Home";
import { LoginCallback, LoginPage } from "pages/Login";
import React from "react";
import { Route, Routes } from "react-router-dom";
import PrivateRouter from "./PrivateRouter";
import PublicRouter from "./PublicRouter";

const RootRouter = () => {
  return (
    <Routes>
      <Route element={<PublicRouter />}>
        <Route
          path={Pages.LOGINPAGE}
          element={<LoginPage />}
          errorElement={<NotFoundPage />}
        />
        <Route path={Pages.LOGINCALLBACK} element={<LoginCallback />} />
        <Route path="*" element={<NotFoundPage />} />
      </Route>
      <Route element={<PrivateRouter />}>
        <Route path={Pages.HOMEPAGE} element={<SettingsDetailPage />} />
        <Route
          path={Pages.MOVEMENTSTATISTICSPAGE}
          element={<MovementStatisticsPage />}
        />
        <Route path={Pages.MOVEMENTLISTPAGE} element={<MovementListPage />} />
        <Route
          path={`${Pages.MOVEMENTDETAILPAGE}/:movementId`}
          element={<MovementDetailPage />}
        />
        <Route path={Pages.SOURCELISTPAGE} element={<SourceListPage />} />
        <Route
          path={`${Pages.SOURCEDETAILPAGE}/:sourceId`}
          element={<SourceDetailPage />}
        />
        <Route path={Pages.CATEGORYLISTPAGE} element={<CategoryListPage />} />
        <Route
          path={`${Pages.CATEGORYDETAILPAGE}/:categoryId`}
          element={<CategoryDetailPage />}
        />
        <Route
          path={Pages.ACCOUNTSTATEMENTSPAGE}
          element={<AccountStatementsPage />}
        />
        <Route path={Pages.CATALOGLISTPAGE} element={<CatalogListPage />} />
        <Route path="*" element={<NotFoundPage />} />
      </Route>
    </Routes>
  );
};

export default RootRouter;
