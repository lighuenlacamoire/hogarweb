import {
  AppFileTypeProps,
  TagColorProps,
  TransactionTypeProps,
} from "interfaces/configuration";
import { BalanceTransactionType } from "interfaces/state";

/** Rutas de navegacion de la App */
enum Pages {
  LOGINPAGE = "/Login",
  LOGINCALLBACK = "/oauth/google",
  HOMEPAGE = "/",
  MOVEMENTLISTPAGE = "/Movimientos",
  MOVEMENTDETAILPAGE = "/Movimiento",
  MOVEMENTSTATISTICSPAGE = "/Estadisticas",
  SOURCELISTPAGE = "/Cajas",
  SOURCEDETAILPAGE = "/Caja",
  CATEGORYLISTPAGE = "/Gastos",
  CATEGORYDETAILPAGE = "/Gasto",
  ACCOUNTSTATEMENTSPAGE = "/Resumen",
  CATALOGLISTPAGE = "/Lista",
  CATALOGITEMPAGE = "/ListaItem",
}

enum MimeTypes {
  BINARY = "application/octet-stream",
  CSV = "text/csv",
  FOLDER = "application/vnd.google-apps.folder",
  JSON = "application/json",
  JSON_UTF8 = "application/json; charset=UTF-8",
  JSON_FILE = "application/vnd.google-apps.script+json",
  PDF = "application/pdf",
  TEXT = "text/plain",
}

const AppFileType: { [key: string]: AppFileTypeProps } = {
  BALANCE: { id: "1", name: "Cuentas", prefix: "bal_" },
  CATALOG: { id: "2", name: "Listas", prefix: "cat_" },
};

const calendarSettings = {
  // Calendar
  current: "Fecha",
  start: "Desde",
  end: "Hasta",
  mode: {
    single: "single",
    range: "range",
  },
  formats: {
    standard: "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'",
    formInput: "yyyy-MM-dd",
    showDetail: "dd/MM/yyyy HH:mm",
    showDate: "dd/MM/yyyy",
    sortItems: "yyyyMMdd",
  },
  units: {
    years: "y",
    quarters: "Q",
    months: "M",
    weeks: "w",
    days: "day",
    hours: "h",
    minutes: "m",
    seconds: "s",
    milliseconds: "ms",
  },
};
/** Configuracion especifica de la App */
const appConfig = {
  folderName: "Hogar",
  timeOut: 3000,
  balance: {
    delimiter: ".", // Miles
    separator: ",", // Decimales
    decimalCount: 2,
  },
  google: {
    redirectScheme: "com.hogar",
    clientId:
      "1042440503983-p7l057k1it02rlhs6f2l9jr9oudhcasj.apps.googleusercontent.com",
    clientSecret: "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",
    webClientId:
      "1042440503983-p7l057k1it02rlhs6f2l9jr9oudhcasj.apps.googleusercontent.com",
    scopes: [
      "https://www.googleapis.com/auth/userinfo.profile",
      "https://www.googleapis.com/auth/userinfo.email",
      "https://www.googleapis.com/auth/drive",
      "https://www.googleapis.com/auth/drive.appfolder",
      /*
        'https://www.googleapis.com/auth/drive',
        'https://www.googleapis.com/auth/drive.file',
        'https://www.googleapis.com/auth/drive.readonly',
        'https://www.googleapis.com/auth/drive.metadata',
        'https://www.googleapis.com/auth/drive.metadata.readonly',
        'https://www.googleapis.com/auth/drive.photos.readonly',
        */
    ],
  },
};

const tagColors: TagColorProps[] = [
  { id: "1", name: "Red", color: "rgba(244, 67, 54, 0.8)" }, // #F44336
  { id: "2", name: "Pink", color: "rgba(233, 30, 99, 0.8)" }, // #E91E63
  { id: "3", name: "Purple", color: "rgba(156, 39, 176, 0.8)" }, // #9C27B0
  { id: "4", name: "Deep Purple", color: "rgba(103, 58, 183, 0.8)" }, // #673AB7
  { id: "5", name: "Indigo", color: "rgba(63, 81, 181, 0.8)" }, // #3F51B5
  { id: "6", name: "Blue", color: "rgba(33, 150, 243, 0.8)" }, // #2196F3
  { id: "7", name: "Light Blue", color: "rgba(3, 169, 244, 0.8)" }, // #03A9F4
  { id: "8", name: "Cyan", color: "rgba(0, 188, 212, 0.8)" }, // #00BCD4
  { id: "9", name: "Teal", color: "rgba(0, 150, 136, 0.8)" }, // #009688
  { id: "10", name: "Green", color: "rgba(76, 175, 80, 0.8)" }, // #4CAF50
  { id: "11", name: "Light Green", color: "rgba(139, 195, 74, 0.8)" }, // #8BC34A
  { id: "12", name: "Lime", color: "rgba(205, 220, 57, 0.8)" }, // #CDDC39
  { id: "13", name: "Yellow", color: "rgba(255, 235, 59, 0.8)" }, // #FFEB3B
  { id: "14", name: "Amber", color: "rgba(255, 193, 7, 0.8)" }, // #FFC107
  { id: "15", name: "Light Orange", color: "rgba(255, 152, 0, 0.8)" }, // #FF9800
  { id: "16", name: "Orange", color: "rgba(255, 87, 34, 0.8)" }, // #FF5722
  { id: "17", name: "Brown", color: "rgba(121, 85, 72, 0.8)" }, // #795548
  { id: "18", name: "Grey", color: "rgba(158, 158, 158, 0.8)" }, // #9E9E9E
  { id: "19", name: "Dark Gray", color: "rgba(96, 125, 139, 0.8)" }, // #607D8B
];

const transactionTypes: TransactionTypeProps[] = [
  {
    id: BalanceTransactionType.EXPENSE,
    name: "Gasto",
    color: "#F44336",
    active: false,
  },
  {
    id: BalanceTransactionType.INCOME,
    name: "Ingreso",
    color: "#4CAF50",
    active: false,
  },
  {
    id: BalanceTransactionType.TRANSFER,
    name: "Transferencia",
    color: "#2196F3",
    active: false,
  },
];

const GDriveErrors = [
  {
    key: "unsupportedOutputFormat",
    locationType: "parameter",
    message: "Unsupported Output Format",
    title: "Archivo invalido",
    text: "formato de archivo no valido",
    code: 400,
  },
  {
    key: "authError",
    locationType: "header",
    message: "Invalid Credentials",
    title: "Autenticacion",
    text: "Fallo de credenciales.",
    code: 401,
  },
];

export {
  Pages,
  MimeTypes,
  AppFileType,
  appConfig,
  calendarSettings,
  GDriveErrors,
  tagColors,
  transactionTypes,
};
