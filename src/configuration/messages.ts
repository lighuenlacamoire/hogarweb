const genericMessages = {
  yes: "Si",
  no: "No",
  ok: "OK",
  back: "Atras",
  next: "Siguiente",
  continue: "Continuar",
  finish: "Finalizar",
  accept: "Aceptar",
  cancel: "Cancelar",
  apply: "Aplicar",
  edit: "Editar",
  new: "Nuevo",
  manage: "Gestionar",
  administer: "Administrar",
  check: "Checkear",
  search: "Buscar",
  clean: "Limpiar",
  save: "Guardar",
  delete: "Eliminar",
  create: "Crear",
  update: "Actualizar",
  refresh: "Actualizar",
  reset: "Reiniciar",
  list: {
    empty: "No contiene elementos",
    duplicated: (key: string, value: string) =>
      `Ya existe un elemento con el ${key} "${value}"`,
  },
  requestConfirmation: {
    title: "Confirmación",
    text: "¿Está seguro de que desea continuar?",
  },
};

const authenticationMessages = {
  login: "Iniciar sesión",
  loginGoogle: "Iniciar sesión con Google",
  logout: "Cerrar sesión",
};

const employeeMessages = {
  height: "Altura",
  gender: "Genero",
  created: "Fecha de creación",
};

const driveMessages = {
  folderInDrive: "Carpeta en Drive",
  fileExpenses: "Archivo de Gastos",
  fileCatalogues: "Archivo de Listas",
};

const navigationMessages = {
  headers: {
    loginPage: "Loguearse",
    settingsDetailPage: (value: string) => `Bienvenido ${value}`,
    accountStatementsPage: "Estado de cuenta",
    catalogListPage: "Listas",
    movementListPage: "Movimientos",
    movementDetailPage: (value: string) => `${value} movimiento`,
  },
};

export {
  genericMessages,
  employeeMessages,
  driveMessages,
  authenticationMessages,
  navigationMessages,
};
