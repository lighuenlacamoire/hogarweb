import { ListItemContent } from "components/ListItem";
import { Pages } from "configuration/constants";
import { genericMessages } from "configuration/messages";
import { GDriveCatalogFile } from "interfaces/googleDrive";
import { CatalogItem } from "interfaces/state";
import { AppError } from "interfaces/services";
import { catalogCharge, catalogItemUpdate } from "redux/actions/catalog";
import { setLoading } from "redux/actions/status";
import { RootState } from "redux/store";
import { getFileById, putFileById } from "services/googleApi/googleFiles";
import { platform, textstyles } from "styles";
import { Text, View } from "components/ReactNative";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { CheckBox } from "components/Check";

const CatalogListPage = () => {
  const { catalogFile } = useSelector((state: RootState) => state.storage);
  const { catalog, categories } = useSelector(
    (state: RootState) => state.catalog,
  );
  const { user } = useSelector((state: RootState) => state.authorization);
  /** Dispatch de Redux */
  const dispatch = useDispatch();

  const [list, setList] = useState<CatalogItem[]>([]);

  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = useCallback(() => {
    setRefreshing(true);
    requestFileById().finally(() => setRefreshing(false));
  }, []);
  /**
   * Renderiza cada Item del array
   * @param {CatalogItem} item item del array
   * @param {number} index
   */
  const renderItem = (item: CatalogItem, index: number) => (
    <ListItemContent
      key={`${index}${item.name}`}
      content={
        <Text
          style={{
            ...textstyles.textBodyLegend,
            transition: "text-decoration 2s ease",
            ...(item.checked
              ? {
                  textDecoration: "line-through solid #333",
                }
              : { textDecoration: "none" }),
          }}>
          {item.name}
        </Text>
      }
      onPress={() => updateItem({ ...item, checked: !item.checked })}
      //onLongPress={() =>
      //  navigation.navigate(Pages.CATALOGITEMPAGE, {
      //    selected: item,
      //  })
      //}
      right={<CheckBox checked={item.checked} />}
    />
  );

  const updateItem = async (itemSelected: CatalogItem) => {
    // Modificacion
    const idxItem = list.findIndex((item) => item.id === itemSelected.id);
    if (idxItem !== -1) {
      list[idxItem] = itemSelected;
    }

    if (catalog && catalog.id) {
      const newFile: GDriveCatalogFile = {
        catalog: {
          ...catalog,
          items: list.sort((a, b) => (a.name < b.name ? -1 : 1)),
        },
        categories: categories,
      };
      dispatch(catalogItemUpdate(itemSelected));
      await putFileById(
        catalogFile?.id || "",
        catalogFile?.name || "",
        newFile,
      );
    }
  };

  const updateItems = () => {
    setList(catalog?.items || []);
  };
  /**
   * Obtiene el archivo y guarda su contenido en redux
   */
  const requestFileById = async () => {
    if (catalogFile?.id) {
      dispatch(setLoading(Pages.CATALOGLISTPAGE, true));
      getFileById<GDriveCatalogFile>(catalogFile.id)
        .then((response) => {
          const { catalog } = response;
          dispatch(catalogCharge(catalog));
        })
        .catch((error: AppError) => {
          dispatch(catalogCharge(undefined));
          console.log(error);
        })
        .finally(() => {
          dispatch(setLoading(Pages.CATALOGLISTPAGE, false));
        });
    }
  };
  useEffect(() => {
    updateItems();
  }, [catalog?.items]);

  useEffect(() => {
    requestFileById();
  }, [catalogFile]);

  return (
    <View style={{ flex: 1 }}>
      {list && list.length > 0 ? (
        <div
          style={{
            flex: 1,
            backgroundColor: platform.colors.formPrimaryBG,
            paddingTop: 4,
          }}>
          {list.map(renderItem)}
        </div>
      ) : (
        <View>
          <Text style={{ ...textstyles.textBodyLegend, padding: "12px" }}>
            {genericMessages.list.empty}
          </Text>
        </View>
      )}
    </View>
  );
};

export default CatalogListPage;
