import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "../../App.css";
import { Button, Form, Col, Row } from "react-bootstrap";
import { setLoading } from "redux/actions/status";
import { useDispatch, useSelector } from "react-redux";
import { employeesListRequest } from "services/starwarsApi/employees";
import { RootState } from "redux/store";
import { CharacterDTO, StarWarsListDTO } from "interfaces/starwarsService";
import { Pages } from "configuration/constants";
import { employeesSetList } from "redux/actions/employees";
import CardsList from "./CardsList";
import { genericMessages } from "configuration/messages";
import { Pager } from "components/Pagination";

/**
 * Pantalla de empleados
 */
const Employee = (): JSX.Element => {
  /** Dispatch de Redux */
  const dispatch = useDispatch();
  /** Navegacion */
  const navigate = useNavigate();
  /** Lista de redux */
  const list: CharacterDTO[] = [];
  //const { list } = useSelector((state: RootState) => state.employees);
  /** Lista de redux filtrada */
  const [filteredList, setFilteredList] = useState<CharacterDTO[]>([]);
  /** Datos del Api */
  const [data, setData] = useState<StarWarsListDTO<CharacterDTO>>();
  const [showModal, setShowModal] = useState(false);
  const [selectedItem, setSelectedItem] = useState<any>();
  const [searchText, setSearchText] = useState<string>();

  /**
   * Actualiza la lista de redux
   * @param {Array} newList
   */
  const updateList = (newList: CharacterDTO[]) => {
    dispatch(employeesSetList(newList));
  };

  /**
   * Actualiza la lista filtrada segun la lista que se este utilizando en redux
   * @param {string} text
   */
  const updateFilteredList = (text?: string) => {
    const value = text || searchText;
    const listNew = [...(list || [])];
    let listFiltered: CharacterDTO[];
    if (value && value.length > 0) {
      listFiltered = listNew.filter((item) =>
        item.name
          .toLocaleLowerCase()
          .replace(/[\u0300-\u036f]/g, "")
          .includes(value.toLowerCase()),
      );
      setFilteredList(listFiltered);
    } else {
      setFilteredList(list);
    }
  };

  /**
   * Api Call: consulta de registro
   * @param {string} params
   */
  const searchList = (params?: string | null) => {
    dispatch(setLoading(Pages.CATALOGLISTPAGE, true));
    employeesListRequest(params)
      .then(async (response) => {
        console.log("si", response);
        if (response && response.results) {
          updateList(response.results);
          setData(response);
        }
      })
      .catch((error) => {
        console.log("no", error);
        setData(undefined);
        //alert(`Fallo: ${error}`);
      })
      .finally(() => dispatch(setLoading(Pages.CATALOGLISTPAGE, false)));
  };

  /**
   * Pop del modal para verificar la eliminacion del item
   * @param item
   */
  const deleteItem = (item: CharacterDTO) => {
    setSelectedItem(item);
    setShowModal(true);
  };

  /**
   * Elimina el item de la lista de redux
   * @param {Object} item
   */
  const removeItem = () => {
    setShowModal(false);
    dispatch(setLoading(Pages.CATALOGLISTPAGE, true));
    try {
      if (selectedItem && selectedItem.name) {
        const newList = [...list];
        const idx = newList.findIndex((m) => m.name === selectedItem.name);
        if (idx !== -1) {
          newList.splice(idx, 1);
          updateList(newList);
        }
      }
    } catch (e) {
      console.log(e);
    } finally {
      setSelectedItem(undefined);
      dispatch(setLoading(Pages.CATALOGLISTPAGE, false));
    }
  };

  const trimPage = (params?: string) => {
    if (params) {
      return params.split("=")[1];
    }
    return null;
  };

  useEffect(() => {
    updateFilteredList();
  }, [JSON.stringify(list)]);

  useEffect(() => {
    searchList();
  }, []);

  return (
    <div className="search-form-gs">
      <h1>Listado</h1>
      <Row md="12">
        <Col md="3">
          <Form.Group controlId="searchGroup">
            <Form.Control
              className="border-gs search-controls"
              type="text"
              name="searchText"
              value={searchText}
              onChange={(e) => setSearchText(e.target.value)}
            />
          </Form.Group>
        </Col>
        <Col md="2">
          <Button
            className="border-gs search-controls"
            variant="primary"
            onClick={() => updateFilteredList()}>
            {genericMessages.search}
          </Button>
        </Col>
        <Col md="2">
          <Button
            className="border-gs search-controls"
            variant="info"
            onClick={() => searchList()}>
            {genericMessages.reset}
          </Button>
        </Col>
        <Col>
          <Col>
            <span>{`Total: ${data?.count || 0}`}</span>
          </Col>
          <Col>
            <span>{`Lista actual: ${list?.length || 0}`}</span>
          </Col>
          <Col>
            <span>{`Filtrados: ${filteredList?.length || 0}`}</span>
          </Col>
        </Col>
      </Row>
      <Row>
        <Col>
          {filteredList && filteredList.length > 0 ? (
            <CardsList onDelete={deleteItem} list={filteredList} />
          ) : null}
        </Col>
      </Row>
      <Pager
        total={data?.count}
        onPageFirst={data?.previous ? () => searchList() : undefined}
        onPagePrevious={
          data?.previous
            ? () => searchList(trimPage(data?.previous))
            : undefined
        }
        onPageNext={
          data?.next ? () => searchList(trimPage(data?.next)) : undefined
        }
      />
    </div>
  );
};

export default Employee;
