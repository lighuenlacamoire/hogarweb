import React from "react";
import { CharacterDTO } from "interfaces/starwarsService";
import Card from "./Card";

type Props = {
  list: CharacterDTO[];
  onDelete: (item: CharacterDTO) => void;
};

/**
 * Componente de lista
 * @param list Lista de empleados
 * @param onDelete Elimina un item de la lista
 */
const CardsList = ({ list, onDelete }: Props): JSX.Element => {
  return (
    <div className="d-flex flex-column scroll-form">
      {list && list.length > 0
        ? list.map((item) => (
            <Card key={item.name} item={item} onDelete={onDelete} />
          ))
        : null}
    </div>
  );
};

export default CardsList;
