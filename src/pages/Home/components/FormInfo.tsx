import { textstyles } from "styles";
import React from "react";
import { Text } from "components/ReactNative";

type Props = {
  containerStyle: React.CSSProperties;
};

/**
 * Formulario: informacion de uso
 */
const FormInfo = (props: Partial<Props>): JSX.Element => {
  const { containerStyle } = props;

  return (
    <div style={containerStyle}>
      <Text style={textstyles.textInputForm}>
        - En la presente pantalla, debe seleccionar una carpeta de Drive para la
        app, en caso de no tenerla (la misma se llama Hogar), puede generar una.
        - Luego debe seleccionar o crear, un archivo para ser utilizado para el
        registro de las operaciones de gastos
      </Text>
    </div>
  );
};

export default FormInfo;
