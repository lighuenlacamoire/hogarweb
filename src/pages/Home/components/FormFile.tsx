import { FormComponent, FormInput } from "components/Form";
import { ModalFooter } from "components/Modal";
import { genericMessages } from "configuration/messages";
import { AppFileTypeProps, FormError } from "interfaces/configuration";
import { restrictText } from "utils/formatter";
import React, { useState } from "react";
import { View } from "components/ReactNative";

type Props = {
  fileData: AppFileTypeProps;
  onPress: (value: string) => void;
  onCancel: () => void;
  containerStyle: React.CSSProperties;
};

/**
 * Formulario: creacion de archivo
 */
const FormFile = (props: Partial<Props>): JSX.Element => {
  const { containerStyle, onPress, onCancel, fileData } = props;
  /** Form: Nombre del archivo */
  const [formFileName, setFormFileName] = useState("");

  const [formErrors, setFormErrors] = useState<FormError>();

  /**
   * Limpia el error asignado a una key
   * @param {string} key Clave de error a limpiar
   */
  const cleanFormError = (key: string) => {
    const errorsNew: FormError = { ...formErrors, [key]: undefined };
    setFormErrors(errorsNew);
  };

  const handleSubmit = () => {
    try {
      let errorsNew: FormError = {};
      if (!(formFileName && formFileName.length > 3)) {
        errorsNew = {
          ...errorsNew,
          formFileName: "Debe ingresar el nombre del archivo",
        };
      }

      if (errorsNew && Object.keys(errorsNew).length > 0) {
        setFormErrors(errorsNew);
      } else {
        if (onPress) {
          const newName = `${fileData?.prefix}${formFileName.trim()}`;
          onPress(newName);
        }
      }
    } catch (error) {
      console.log("FormFile -- handleSubmit", error);
    }
  };

  return (
    <div style={{ flex: 1, height: 220, ...containerStyle }}>
      <div>
        <FormComponent
          header={`Nombre del archivo: ${fileData?.name}`}
          error={formErrors?.formFileName}>
          <FormInput
            name="formFileName"
            value={formFileName}
            placeholder={"Ingrese el nombre"}
            onValueChange={(value: string) => {
              setFormFileName(restrictText(value));
              cleanFormError("formFileName");
            }}
            error={formErrors?.formFileName}
          />
        </FormComponent>
      </div>

      <ModalFooter
        actions={{
          primary: {
            content: genericMessages.accept,
            onPress: () => {
              handleSubmit();
            },
          },
          secondary: {
            content: genericMessages.cancel,
            onPress: onCancel,
          },
        }}
      />
    </div>
  );
};

export default FormFile;
