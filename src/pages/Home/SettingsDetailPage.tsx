import { ButtonReactive } from "components/Button";
import { RadioBox } from "components/Check";
import { FormContainer } from "components/Form";
import Icon from "components/Icon";
import {
  ListItemAccordion,
  ListItemActions,
  ListItemContent,
} from "components/ListItem";
import { Text } from "components/ReactNative";
import { AppFileType, Pages } from "configuration/constants";
import {
  driveMessages,
  genericMessages,
  navigationMessages,
} from "configuration/messages";
import { ModalMessage } from "interfaces/buttons";
import { AppFileTypeProps } from "interfaces/configuration";
import {
  GDriveBalanceFile,
  GDriveCatalogFile,
  GDriveFile,
} from "interfaces/googleDrive";
import { AppError } from "interfaces/services";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { logoutExecute } from "redux/actions/authorization";
import { catalogCharge } from "redux/actions/catalog";
import { clearMessage, setMessage } from "redux/actions/notification";
import { setLoading } from "redux/actions/status";
import { StorageSelect } from "redux/actions/storage";
import { RootState } from "redux/store";
import {
  getAppAllFolders,
  getAppOwnFolders,
  getFileById,
  getFilesFromFolderId,
  postFileIfNotExists,
  postFolderIfNotExists,
} from "services/googleApi/googleFiles";
import { containerStyles, platform, textstyles } from "styles";
import { capitalize } from "utils/formatter";
import { deleteItem } from "utils/storage";
import FormFile from "./components/FormFile";
import FormInfo from "./components/FormInfo";

const SettingsDetailPage = () => {
  /** Dispatch de Redux */
  const dispatch = useDispatch();
  /** Datos del usuario que esta logueado */
  const { user } = useSelector((state: RootState) => state.authorization);
  /** Carpeta y archivos preseleccionados */
  const { mainFolder, balanceFile, catalogFile } = useSelector(
    (state: RootState) => state.storage,
  );
  /** Lista de carpetas */
  const [appFolders, setAppFolders] = useState<
    (GDriveFile & { active: boolean })[]
  >([]);
  /** Lista de archivos de gastos */
  const [appBalanceFiles, setAppBalanceFiles] = useState<
    (GDriveFile & { active: boolean })[]
  >([]);
  /** Lista de archivos de gastos */
  const [appCatalogFiles, setAppCatalogFiles] = useState<
    (GDriveFile & { active: boolean })[]
  >([]);

  const skipSettings = mainFolder?.id && balanceFile?.id ? true : false;

  /** Busca las carpetas */
  const requestFolders = async () => {
    dispatch(setLoading(Pages.HOMEPAGE, true));
    try {
      //await signIn();
      /* VERIFICAR
      dispatch(StorageSelect('balanceFile', undefined));
      dispatch(StorageSelect('catalogFile', undefined));
      //dispatch(StorageSelect('mainFolder', undefined));
      */
      const folders: (GDriveFile & { active: boolean })[] = [];

      const { files } = await getAppOwnFolders();
      files.map((file) => {
        folders.push({ ...file, active: mainFolder?.id === file.id });
      });
      const allFolders = await getAppAllFolders();
      if (allFolders.files && allFolders.files.length > 0) {
        allFolders.files.map((folder) => {
          const idx = files.findIndex((m) => m.id === folder.id);
          if (idx === -1) {
            folders.push({
              ...folder,
              name: `${folder.name} (Compartido)`,
              active: mainFolder?.id === folder.id,
            });
          }
        });
      }
      setAppFolders(folders);
      if (mainFolder?.id) {
        await requestFilesByFolderId(mainFolder?.id);
      }
    } catch (er) {
      console.log("requestFolders", er);
      setAppFolders([]);
      setAppBalanceFiles([]);
      setAppCatalogFiles([]);
    } finally {
      dispatch(setLoading(Pages.HOMEPAGE, false));
    }
  };
  /**
   * Selecciona una carpeta de drive
   * @param {GDriveFile} file Datos del archivo de drive
   */
  const selectFolder = async (file: GDriveFile & { active: boolean }) => {
    dispatch(setLoading(Pages.HOMEPAGE, true));
    try {
      const mod = appFolders.map((item) => ({
        ...item,
        active: item.id === file.id,
      }));
      setAppFolders(mod);
      await requestFilesByFolderId(file.id, true);
      dispatch(StorageSelect("mainFolder", file));
      dispatch(StorageSelect("balanceFile", undefined));
      dispatch(StorageSelect("catalogFile", undefined));
    } catch (er) {
      console.log("selectFolder", er);
    } finally {
      dispatch(setLoading(Pages.HOMEPAGE, false));
    }
  };

  /**
   * Genera una copia del archivo de drive de gastos
   * @param fileId Id del archivo a copiar
   */
  const copyBalanceFile = (fileId: string) => {
    console.log("copyBalanceFile", fileId);
    createFileForm(AppFileType.BALANCE, fileId);
  };

  /**
   * Selecciona el archivo de drive para gastos
   * @param file Datos del archivo que se selecciona
   */
  const selectBalanceFile = (file?: GDriveFile & { active: boolean }) => {
    dispatch(setLoading(Pages.HOMEPAGE, true));
    const mod = appBalanceFiles.map((item) => ({
      ...item,
      active: item.id === file?.id,
    }));
    setAppBalanceFiles(mod);
    dispatch(StorageSelect("balanceFile", file));
    dispatch(setLoading(Pages.HOMEPAGE, false));
  };

  const selectCatalogFile = (file?: GDriveFile & { active: boolean }) => {
    dispatch(setLoading(Pages.HOMEPAGE, true));
    const mod = appCatalogFiles.map((item) => ({
      ...item,
      active: item.id === file?.id,
    }));
    setAppCatalogFiles(mod);
    dispatch(StorageSelect("catalogFile", file));
    dispatch(setLoading(Pages.HOMEPAGE, false));
  };

  /**
   * Crea la carpeta de la app en Drive
   */
  const createFolder = async () => {
    dispatch(setLoading(Pages.HOMEPAGE, true));
    postFolderIfNotExists()
      .then(() => {
        requestFolders();
      })
      .catch((error: AppError) => {
        handleError(error);
      })
      .finally(() => {
        dispatch(setLoading(Pages.HOMEPAGE, false));
      });
  };

  /**
   * Crea un archivo
   * @param {string} fileName Nombre del archivo
   * @param {AppFileTypeProps} fileData tipo de archivo
   * @param {string} fileId id del archivo a copiar
   */
  const createFile = async (
    fileName: string,
    fileData: AppFileTypeProps,
    fileId?: string,
  ) => {
    const folder = appFolders.find((x) => x.active);
    if (folder?.id && fileData.id) {
      let newFile: any = {};
      if (fileData.id === AppFileType.BALANCE.id) {
        newFile = {
          balance: {
            movements: [],
            categories: [],
            sources: [],
          },
        } as GDriveBalanceFile;
        if (fileId) {
          const res = await getFileById<GDriveBalanceFile>(fileId);
          newFile = {
            balance: {
              ...res.balance,
              movements: [],
            },
          };
        }
      } else if (fileData.id === AppFileType.CATALOG.id) {
        newFile = {
          catalog: {
            id: `${new Date().getTime()}`,
            name: capitalize(fileName.slice(4)),
            user: {
              id: user?.id || "", // id del usuario de google
              name: user?.name || "", // nombre del usuario de google
            },
            items: [],
          },
          categories: [],
        } as GDriveCatalogFile;
      }

      dispatch(setLoading(Pages.HOMEPAGE, true));
      postFileIfNotExists(folder.id, fileName, newFile)
        .then(async (response) => {
          await requestFilesByFolderId(folder.id);
        })
        .catch((error) => {
          handleError(error);
        })
        .finally(() => {
          dispatch(setLoading(Pages.HOMEPAGE, false));
        });
    } else {
      handleError({
        title: "Carpeta de Drive",
        message:
          "Por favor seleccione una carpeta de drive para poder crear el archivo en ella.",
      });
    }
  };
  const logout = async () => {
    dispatch(logoutExecute());
    deleteItem("@accessTokenGoogle");
  };

  /**
   * Obtiene los archivos de la carpeta seleccionada
   * @param {string} folderId Id de la carpeta
   * @param {boolean|undefined} clear si debe limpiar o no
   */
  const requestFilesByFolderId = async (folderId: string, clear?: boolean) => {
    dispatch(setLoading(Pages.HOMEPAGE, true));
    getFilesFromFolderId(folderId)
      .then((response) => {
        if (response && response.files) {
          setAppCatalogFiles(
            response.files
              .filter((x) => x.name.startsWith(AppFileType.CATALOG.prefix))
              .map((file) => ({
                ...file,
                active: clear ? false : catalogFile?.id === file.id,
              })),
          );
          setAppBalanceFiles(
            response.files
              .filter((x) => x.name.startsWith(AppFileType.BALANCE.prefix))
              .map((file) => ({
                ...file,
                active: clear ? false : balanceFile?.id === file.id,
              })),
          );
        }
      })
      .catch((error) => {
        console.log("requestFilesByFolderId", error);
        /* VERIFICAR
        setAppCatalogFiles([]);
        setAppBalanceFiles([]);
        */
      })
      .finally(() => {
        dispatch(setLoading(Pages.HOMEPAGE, false));
      });
  };

  const handleSubmit = () => {
    console.log("a");
  };

  /**
   * Abre el modal para crear un nuevo archivo
   * @param {AppFileTypeProps} fileT Tipo de archivo a crear
   * @param {string | undefined} fileId Id del archivo a copiar
   */
  const createFileForm = (fileT: AppFileTypeProps, fileId?: string) => {
    const form = (
      <FormFile
        fileData={fileT}
        onPress={(value: string) => {
          dispatch(clearMessage());
          createFile(value, fileT, fileId);
        }}
        onCancel={() => dispatch(clearMessage())}
      />
    );
    const newMessage: ModalMessage = {
      title: "Creación de archivo",
      content: form,
      selfClose: false,
      isVisible: true,
      actions: undefined,
    };
    dispatch(setMessage(newMessage));
  };

  /**
   * Muestra el error en el modal
   * @param error Error del servicio
   */
  const handleError = (error: AppError) => {
    const newMessage: ModalMessage = {
      title: error.title || "Error",
      content: error.message,
      selfClose: false,
      isVisible: true,
      actions: {
        primary: {
          content: genericMessages.ok,
          onPress: () => {
            dispatch(clearMessage());
          },
        },
      },
    };
    dispatch(setMessage(newMessage));
  };

  const showUserGuide = () => {
    const form = <FormInfo />;
    const newMessage: ModalMessage = {
      title: "Información",
      content: form,
      selfClose: false,
      isVisible: true,
      actions: {
        primary: {
          content: genericMessages.ok,
          onPress: () => {
            dispatch(clearMessage());
          },
        },
      },
    };
    dispatch(setMessage(newMessage));
  };

  useEffect(() => {
    requestFolders();
  }, [user]);

  return (
    <FormContainer
      header={
        skipSettings
          ? "Configuración"
          : navigationMessages.headers.settingsDetailPage(user?.name || "")
      }
      icon="settings"
      className="col-xs-12 col-md-9 col-lg-6">
      <div className="row">
        <ListItemAccordion content={driveMessages.folderInDrive}>
          <>
            <div
              onClick={() => createFolder()}
              style={{
                ...containerStyles.listItemContainer,
                height: "var(--form-control-body-height)",
                cursor: "pointer",
                paddingInline: platform.generic.paddingSpaces,
              }}>
              <span
                style={{
                  ...textstyles.textBodyLegend,
                  color: platform.colors.primary,
                }}>
                {appFolders && appFolders.length > 0
                  ? genericMessages.manage
                  : genericMessages.create}
              </span>
              <Icon
                size={20}
                name={appFolders && appFolders.length > 0 ? "search" : "plus"}
                color={platform.colors.primary}
              />
            </div>
            {appFolders && appFolders.length > 0 ? (
              appFolders.map((item) => (
                <ListItemContent
                  key={item.id}
                  containerStyle={{
                    height: platform.generic.height,
                    paddingInline: platform.generic.paddingSpaces,
                  }}
                  content={item.name}
                  onPress={() => selectFolder(item)}
                  right={<RadioBox size={20} checked={item.active} />}
                />
              ))
            ) : (
              <Text style={{ ...textstyles.textBodyLegend, padding: 12 }}>
                {genericMessages.list.empty}
              </Text>
            )}
          </>
        </ListItemAccordion>
        <ListItemAccordion content={driveMessages.fileExpenses}>
          <>
            <div
              onClick={() => createFileForm(AppFileType.BALANCE)}
              style={{
                ...containerStyles.listItemContainer,
                height: "var(--form-control-body-height)",
                cursor: "pointer",
                paddingInline: platform.generic.paddingSpaces,
              }}>
              <span
                style={{
                  ...textstyles.textBodyLegend,
                  color: platform.colors.primary,
                }}>
                {appBalanceFiles && appBalanceFiles.length > 0
                  ? genericMessages.manage
                  : genericMessages.create}
              </span>
              <Icon
                size={20}
                name={
                  appBalanceFiles && appBalanceFiles.length > 0
                    ? "search"
                    : "plus"
                }
                color={platform.colors.primary}
              />
            </div>
            {appBalanceFiles && appBalanceFiles.length > 0 ? (
              appBalanceFiles.map((item) => (
                <ListItemActions
                  key={item.id}
                  containerStyle={{
                    height: platform.generic.height,
                    paddingInline: platform.generic.paddingSpaces,
                  }}
                  text={item.name.slice(4)}
                  actions={[
                    <div
                      key={item.name.slice(4)}
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        cursor: "pointer",
                      }}
                      onClick={() => selectBalanceFile(item)}>
                      <RadioBox size={20} checked={item.active} />
                    </div>,
                    <div
                      key={`${item.id}`}
                      className="pulsed"
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        height: 20 + 14,
                        width: 20 + 14,
                        borderRadius: (20 + 14) / 2,
                        cursor: "pointer",
                      }}
                      onClick={() => copyBalanceFile(item.id)}>
                      <Icon
                        size={22}
                        color={platform.colors.text}
                        name={"copy"}
                      />
                    </div>,
                  ]}
                />
              ))
            ) : (
              <Text style={{ ...textstyles.textBodyLegend, padding: 12 }}>
                {genericMessages.list.empty}
              </Text>
            )}
          </>
        </ListItemAccordion>
        <ListItemAccordion content={driveMessages.fileCatalogues}>
          <>
            <div
              onClick={() => createFileForm(AppFileType.CATALOG)}
              style={{
                ...containerStyles.listItemContainer,
                height: "var(--form-control-body-height)",
                cursor: "pointer",
                paddingInline: platform.generic.paddingSpaces,
              }}>
              <span
                style={{
                  ...textstyles.textBodyLegend,
                  color: platform.colors.primary,
                }}>
                {appCatalogFiles && appCatalogFiles.length > 0
                  ? genericMessages.manage
                  : genericMessages.create}
              </span>
              <Icon
                size={20}
                name={
                  appCatalogFiles && appCatalogFiles.length > 0
                    ? "search"
                    : "plus"
                }
                color={platform.colors.primary}
              />
            </div>
            {appCatalogFiles && appCatalogFiles.length > 0 ? (
              appCatalogFiles.map((item) => (
                <ListItemContent
                  key={item.id}
                  containerStyle={{
                    height: platform.generic.height,
                    paddingInline: platform.generic.paddingSpaces,
                  }}
                  content={item.name.slice(4)}
                  onPress={() => selectCatalogFile(item)}
                  right={<RadioBox size={20} checked={item.active} />}
                />
              ))
            ) : (
              <Text style={{ ...textstyles.textBodyLegend, padding: 12 }}>
                {genericMessages.list.empty}
              </Text>
            )}
          </>
        </ListItemAccordion>
      </div>
      <div style={{ ...containerStyles.footerDouble, marginTop: "20px" }}>
        <ButtonReactive
          content={genericMessages.search}
          inverse
          onPress={requestFolders}
        />
        <ButtonReactive
          content={genericMessages.apply}
          onPress={handleSubmit}
        />
      </div>
    </FormContainer>
  );
};
export default SettingsDetailPage;
