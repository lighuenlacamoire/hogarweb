import { ButtonReactive } from "components/Button";
import {
  authenticationMessages,
  genericMessages,
} from "configuration/messages";
import React, { useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { getPermissionsOAuth2Google } from "services/googleApi/googleAuth";
import { ModalPopUp } from "components/Modal";
import { View } from "components/ReactNative";
import { containerStyles, platform } from "styles";
import { ViewForm } from "components/Container";

const LoginPage = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const [showModal, setShowModal] = useState(false);
  const from = ((location.state as any)?.from?.pathname as string) || "/";

  return (
    <ViewForm style={{ marginInline: platform.generic.paddingSpaces }}>
      <ModalPopUp
        isVisible={showModal}
        title="Confirmacion"
        content="Esta seguro que desea continuar?"
        onBackdropPress={() => setShowModal(false)}
        actions={{
          primary: {
            onPress: () => setShowModal(false),
          },
          secondary: {
            onPress: () => setShowModal(false),
          },
        }}
      />
      <h1>Bievenido</h1>
      <div style={containerStyles.footerDouble}>
        <ButtonReactive
          content={authenticationMessages.loginGoogle}
          onPress={() => {
            const url = getPermissionsOAuth2Google();
            window.location.href = url;
          }}
        />
        <ButtonReactive
          content={genericMessages.cancel}
          inverse
          onPress={() => setShowModal(true)}
        />
      </div>
    </ViewForm>
  );

  return (
    <View>
      <ModalPopUp
        isVisible={showModal}
        header="Confirmacion"
        content="Esta seguro que desea continuar?"
        onBackdropPress={() => setShowModal(false)}
        actions={{
          primary: {
            onPress: () => setShowModal(false),
          },
          secondary: {
            onPress: () => setShowModal(false),
          },
        }}
      />
      <ButtonReactive
        content={authenticationMessages.login}
        onPress={() => {
          const url = getPermissionsOAuth2Google();
          window.location.href = url;
        }}
      />
      <a href={getPermissionsOAuth2Google()}>{authenticationMessages.login}</a>
    </View>
  );
};

export default LoginPage;
