import { Pages } from "configuration/constants";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { loginSuccess } from "redux/actions/authorization";
import { setLoading } from "redux/actions/status";
import { getLogginOAuth2Google } from "services/googleApi/googleAuth";

const LoginCallback = () => {
  /** Dispatch de Redux */
  const dispatch = useDispatch();
  /** Navegacion */
  const navigate = useNavigate();
  const location = useLocation();
  const [permissionCode, setPermissionCode] = useState(
    new URLSearchParams(location.search).get("code"),
  );
  dispatch(setLoading(Pages.LOGINCALLBACK, true));
  //const permissionCode = new URLSearchParams(location.search).get("code");
  //const permissionError = new URLSearchParams(location.search).get("error");

  const getAccessToken = async () => {
    if (permissionCode) {
      getLogginOAuth2Google(permissionCode)
        .then(async (response) => {
          dispatch(loginSuccess(response.info, response.token.access_token));
          navigate(Pages.HOMEPAGE);
        })
        .catch((er) => console.log("er", er))
        .finally(() => dispatch(setLoading(Pages.LOGINCALLBACK, false)));
    } else {
      navigate(Pages.LOGINPAGE);
      dispatch(setLoading(Pages.LOGINCALLBACK, false));
    }
  };

  useEffect(() => {
    getAccessToken();
  }, [permissionCode]);

  return <div></div>;
};

export default LoginCallback;
