import { ButtonReactive } from "components/Button";
import Icon from "components/Icon";
import { Pages, tagColors } from "configuration/constants";
import { RootState } from "redux/store";
import { platform, containerStyles } from "styles";
import { capitalize, restrictText, toCurrency } from "utils/formatter";
import React, { RefObject, useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GDriveBalanceFile } from "interfaces/googleDrive";
import { setLoading } from "redux/actions/status";
import { genericMessages } from "configuration/messages";
import { ModalMessage } from "interfaces/buttons";
import { clearMessage, setMessage } from "redux/actions/notification";
import { FormComponent, FormContainer, FormInput } from "components/Form";
import { BalanceSource, BalanceState } from "interfaces/state";
import { AppError } from "interfaces/services";
import { useLocation, useNavigate } from "react-router-dom";
import { getFileById, putFileById } from "services/googleApi/googleFiles";
import { CategoryDetailParams } from "interfaces/navigations";

const CategoryDetailPage = () => {
  /** Referencia del archivo en redux */
  const { balanceFile } = useSelector((state: RootState) => state.storage);

  /** Datos del archivo que buscaremos en la Api */
  const [dataBalance, setDataBalance] = useState<BalanceState>();

  /** Dispatch de Redux */
  const dispatch = useDispatch();
  /** Navegacion */
  const navigate = useNavigate();

  const location = useLocation();
  /** Parametros de navegacion */
  const params = location?.state;

  /** Form: Nombre */
  const [formName, setFormName] = useState(params?.name || "");
  /** Form: Color */
  const [formColor, setFormColor] = useState(params?.color || "#2196F3");
  /** Form: Monto Maximo */
  const [formAmountMax, setFormAmountMax] = useState(
    params?.amounts?.max || "",
  );
  /** Form: Monto Inicial/default */
  const [formAmountInit, setFormAmountInit] = useState(
    params?.amounts?.init || "",
  );

  /** Indica si es 'Creacion' o 'Modificacion' */
  const isEditable = params?.id && params?.name;

  /**
   * Submit: Crea Modifica o Elimina el item
   * @param {boolean} isDelete Indica si debe borrar el item
   */
  const handleSubmit = async (isDelete?: boolean) => {
    dispatch(setLoading(Pages.CATEGORYDETAILPAGE, true));
    try {
      const { balance } = await getFileById<GDriveBalanceFile>(
        balanceFile?.id || "",
      );
      let newList: BalanceSource[] = [];

      // Borrado
      if (isDelete) {
        newList = [...balance.categories];
        const idx = newList.findIndex((item) => item.id === params?.id);
        if (idx !== -1) {
          newList.splice(idx, 1);
        }
      } else {
        // Creacion o Modificacion
        const newName = capitalize(formName);
        const duplicated = dataBalance?.categories.find(
          (x) => x.name === newName,
        );

        if (duplicated?.id && duplicated?.id !== params?.id) {
          const newMessage: ModalMessage = {
            title: "Validacion",
            content: genericMessages.list.duplicated("nombre", duplicated.name),
            selfClose: false,
            isVisible: true,
            actions: {
              primary: {
                content: genericMessages.ok,
                onPress: () => {
                  dispatch(clearMessage());
                },
              },
            },
          };
          dispatch(setMessage(newMessage));
          return;
        }
        const amountMax =
          formAmountMax != "0,00" &&
          formAmountMax != "0" &&
          formAmountMax != "0.00"
            ? formAmountMax
            : undefined;
        const newItem: CategoryDetailParams = {
          id: params?.id || `${new Date().getTime()}`,
          name: newName,
          color: formColor,
          ...(amountMax
            ? { amounts: { max: formAmountMax, init: formAmountInit } }
            : {}),
        };
        if (isEditable) {
          // Modificacion
          newList = balance.categories.map((item) => {
            if (item.id === params.id) {
              return newItem;
            }
            return item;
          });
        } else {
          // Creacion
          newList = [...balance.categories, newItem];
        }
      }

      const newFile: GDriveBalanceFile = {
        balance: {
          ...balance,
          categories: newList.sort((a, b) => (a.name < b.name ? -1 : 1)),
        },
      };

      await putFileById(
        balanceFile?.id || "",
        balanceFile?.name || "",
        newFile,
      );

      setDataBalance(newFile.balance);

      navigate(-1);
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(setLoading(Pages.CATEGORYDETAILPAGE, false));
    }
  };

  /**
   * Obtiene el archivo y guarda su contenido en hooks
   */
  const requestFileById = async () => {
    dispatch(setLoading(Pages.CATEGORYDETAILPAGE, true));
    getFileById<GDriveBalanceFile>(balanceFile?.id || "")
      .then((response) => {
        setDataBalance(response.balance);
      })
      .catch((error: AppError) => {
        setDataBalance(undefined);
        handleError(error);
      })
      .finally(() => {
        dispatch(setLoading(Pages.CATEGORYDETAILPAGE, false));
      });
  };

  /**
   * Muestra el error en el modal
   * @param {AppError} error Error del servicio
   */
  const handleError = (error: AppError) => {
    const newMessage: ModalMessage = {
      title: error.title || "Error",
      content: error.message,
      selfClose: false,
      isVisible: true,
      actions: {
        primary: {
          content: genericMessages.ok,
          onPress: () => {
            dispatch(clearMessage());
          },
        },
      },
    };
    dispatch(setMessage(newMessage));
  };

  useEffect(() => {
    requestFileById();
  }, [balanceFile]);

  return (
    <FormContainer
      header={`Gastos: ${params?.id ? "Edicion" : "Creacion"}`}
      icon="coins">
      <div className="row">
        <div className="col-xs-12 col-md-6 col-lg-6">
          <FormComponent header="Nombre">
            <FormInput
              name="formName"
              value={formName}
              placeholder={"Ingrese el nombre"}
              onValueChange={(value: string) => {
                setFormName(restrictText(value));
                // cleanError('formFullName');
              }}
            />
          </FormComponent>
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12 col-md-12 col-lg-12">
          <FormComponent header="Etiqueta de color">
            <div
              style={{
                flexDirection: "row",
                flexWrap: "wrap",
                flex: 1,
                display: "flex",
              }}>
              {tagColors.map((item) => (
                <div
                  key={item.color}
                  onClick={() => setFormColor(item.color)}
                  style={{
                    backgroundColor: item.color,
                    width: 36,
                    height: 36,
                    borderRadius: 18,
                    margin: 4,
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                    cursor: "pointer",
                  }}>
                  {item.color === formColor ? (
                    <Icon
                      size={24}
                      name="check"
                      color={platform.colors.white}
                    />
                  ) : null}
                </div>
              ))}
            </div>
          </FormComponent>
        </div>
      </div>
      <div className="row">
        <div className="col-xs-12 col-md-6 col-lg-6">
          <FormComponent header="Monto Inicial/por defecto">
            <FormInput
              name="formAmountInit"
              value={formAmountInit}
              isNumeric
              placeholder={"Ingrese el monto"}
              onValueChange={(value: string) => {
                setFormAmountInit(toCurrency(value));
              }}
            />
          </FormComponent>
        </div>
        <div className="col-xs-12 col-md-6 col-lg-6">
          <FormComponent header="Monto maximo">
            <FormInput
              name="formAmountMax"
              value={formAmountMax}
              isNumeric
              placeholder={"Ingrese el monto"}
              onValueChange={(value: string) => {
                setFormAmountMax(toCurrency(value));
              }}
            />
          </FormComponent>
        </div>
      </div>
      <div
        className="row"
        style={{ marginTop: 32, marginLeft: 0, marginRight: 0 }}>
        <div className="row" style={{ flexDirection: "row-reverse" }}>
          <div className="col-xs-12 col-md-4 col-lg-4">
            <ButtonReactive
              content={
                isEditable ? genericMessages.save : genericMessages.continue
              }
              onPress={() => handleSubmit()}
            />
          </div>
          {isEditable ? (
            <div className="col-xs-12 col-md-4 col-lg-4">
              <ButtonReactive
                content={genericMessages.delete}
                color={"#F44336"}
                onPress={() => handleSubmit(true)}
              />
            </div>
          ) : null}
        </div>
      </div>
    </FormContainer>
  );

  /*
  return (
    <View style={containerStyles.bodyPage}>
      <ScrollContainer
        containerStyle={{ padding: platform.generic.paddingSpaces }}>
        <FormComponent header="Nombre">
          <FormInput
            refInput={refFormName as RefObject<TextInput>}
            name="formName"
            value={formName}
            placeholder={"Ingrese el nombre"}
            onValueChange={(value: string) => {
              setFormName(restrictText(value));
              // cleanError('formFullName');
            }}
            //error={formErrors?.formFullName}
            textInputProps={{
              onSubmitEditing: () => handleSubmit(),
              blurOnSubmit: false,
            }}
          />
        </FormComponent>
        <FormComponent header="Etiqueta de color">
          <View style={{ flexDirection: "row", flexWrap: "wrap", flex: 1 }}>
            {tagColors.map((item) => (
              <TouchableOpacity
                key={item.color}
                onPress={() => setFormColor(item.color)}
                style={{
                  backgroundColor: item.color,
                  width: 36,
                  height: 36,
                  borderRadius: 18,
                  margin: 4,
                  alignItems: "center",
                  justifyContent: "center",
                }}>
                {item.color === formColor ? (
                  <Icon size={24} name="check" color={platform.colors.white} />
                ) : null}
              </TouchableOpacity>
            ))}
          </View>
        </FormComponent>
        <FormComponent header="Monto Inicial/por defecto">
          <FormInput
            name="formAmountInit"
            value={formAmountInit}
            isNumeric
            placeholder={"Ingrese el monto"}
            onValueChange={(value: string) => {
              setFormAmountInit(toCurrency(value));
            }}
          />
        </FormComponent>
        <FormComponent
          header="Monto maximo"
          containerStyle={[{ marginBottom: 50 }]}>
          <FormInput
            name="formAmountMax"
            value={formAmountMax}
            isNumeric
            placeholder={"Ingrese el monto"}
            onValueChange={(value: string) => {
              setFormAmountMax(toCurrency(value));
            }}
          />
        </FormComponent>
      </ScrollContainer>
      <View
        style={[
          isEditable
            ? containerStyles.footerDouble
            : containerStyles.footerStatic,
        ]}>
        <ButtonReactive
          content={isEditable ? genericMessages.save : genericMessages.continue}
          onPress={() => handleSubmit()}
        />
        {isEditable ? (
          <ButtonReactive
            content={genericMessages.delete}
            color={platform.colors.expenseAmount}
            onPress={() => handleSubmit(true)}
          />
        ) : null}
      </View>
    </View>
  );
  */
};

export default CategoryDetailPage;
