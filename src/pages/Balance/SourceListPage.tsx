import { Pages } from "configuration/constants";
import { BalanceSource } from "interfaces/state";
import { RootState } from "redux/store";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { ListItemContent } from "components/ListItem";
import { platform, containerStyles } from "styles";
import { genericMessages } from "configuration/messages";
import { setLoading } from "redux/actions/status";
import { GDriveBalanceFile } from "interfaces/googleDrive";
import { AppError } from "interfaces/services";
import { ModalMessage } from "interfaces/buttons";
import { clearMessage, setMessage } from "redux/actions/notification";
import { useNavigate } from "react-router-dom";
import { getFileById } from "services/googleApi/googleFiles";
import Icon from "components/Icon";

const SourceListPage = () => {
  const { balanceFile } = useSelector((state: RootState) => state.storage);
  /** Dispatch de Redux */
  const dispatch = useDispatch();
  /** Navegacion */
  const navigate = useNavigate();

  const [list, setList] = useState<BalanceSource[]>([]);

  /**
   * Renderiza cada Item del array
   * @param {BalanceSource} item item del array
   * @param {number} index indice del array
   */
  const renderItem = (item: BalanceSource, index: number) => (
    <ListItemContent
      containerStyle={{
        ...containerStyles.shadowDefault,
        maxWidth: "320px",
        width: "300px",
        height: "50px",
        paddingInline: platform.generic.paddingSpaces,
        backgroundColor: platform.colors.white,
        borderRadius: platform.generic.borderRadius,
        borderWidth: platform.generic.borderWidth,
        borderColor: platform.colors.text,
      }}
      key={`${index}${item.id}`}
      content={item.name}
      onPress={() =>
        navigate(`${Pages.SOURCEDETAILPAGE}/${item.id}`, { state: item })
      }
      right={<Icon size={22} name="check" />}
    />
  );
  /**
   * Muestra el error en el modal
   * @param error Error del servicio
   */
  const handleError = (error: AppError) => {
    const newMessage: ModalMessage = {
      title: error.title || "Error",
      content: error.message,
      selfClose: false,
      isVisible: true,
      actions: {
        primary: {
          content: genericMessages.ok,
          onPress: () => {
            dispatch(clearMessage());
          },
        },
      },
    };
    dispatch(setMessage(newMessage));
  };

  /**
   * Obtiene el archivo y guarda su contenido en redux
   */
  const requestFileById = async () => {
    dispatch(setLoading(Pages.MOVEMENTLISTPAGE, true));
    getFileById<GDriveBalanceFile>(balanceFile?.id || "")
      .then((response) => {
        const { balance } = response;
        setList(balance.sources.sort((a, b) => (a.name < b.name ? -1 : 1)));
      })
      .catch((error: AppError) => {
        setList([]);
        handleError(error);
      })
      .finally(() => {
        dispatch(setLoading(Pages.MOVEMENTLISTPAGE, false));
      });
  };

  useEffect(() => {
    requestFileById();
  }, [balanceFile]);

  return (
    <div
      style={{
        backgroundColor: platform.colors.listBackground,
        paddingTop: "20px",
        paddingBottom: "20px",
        flex: 1,
      }}>
      <div className="cards-list">{list.map(renderItem)}</div>
    </div>
  );
};

export default SourceListPage;
