import { ButtonReactive } from "components/Button";
import {
  FormCalendarModal,
  FormComponent,
  FormContainer,
  FormDropDown,
  FormInput,
} from "components/Form";
import {
  calendarSettings,
  Pages,
  transactionTypes,
} from "configuration/constants";
import { genericMessages } from "configuration/messages";
import { ModalMessage } from "interfaces/buttons";
import { AmountOptions, FormError } from "interfaces/configuration";
import { GDriveBalanceFile } from "interfaces/googleDrive";
import { CategoryDetailParams } from "interfaces/navigations";
import { AppError } from "interfaces/services";
import {
  BalanceMovement,
  BalanceSource,
  BalanceState,
  BalanceTransactionType,
} from "interfaces/state";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useLocation, useNavigate } from "react-router-dom";
import { clearMessage, setMessage } from "redux/actions/notification";
import { setLoading } from "redux/actions/status";
import { RootState } from "redux/store";
import { getFileById, putFileById } from "services/googleApi/googleFiles";
import { platform, textstyles } from "styles";
import {
  factoringDate,
  restrictText,
  toAmountCulture,
  toCurrency,
  toDecimal,
} from "utils/formatter";

/**
 * Pantalla: Movimiento - Creacion/Edicion
 */
const MovementDetailPage = () => {
  /** Referencia del archivo en redux */
  const { balanceFile } = useSelector((state: RootState) => state.storage);
  /** Datos del usuario logueado */
  const { user } = useSelector((state: RootState) => state.authorization);

  /** Datos del archivo que buscaremos en la Api */
  const [dataBalance, setDataBalance] = useState<BalanceState>();

  /** Dispatch de Redux */
  const dispatch = useDispatch();
  /** Navegacion */
  const navigate = useNavigate();

  const location = useLocation();
  /** Parametros de navegacion */
  const params = location?.state;

  const [formTransactionTypes, setFormTransactionTypes] = useState(
    transactionTypes.map((item) => ({
      ...item,
      active: item.id === params?.transactionType,
    })),
  );

  /** Indica si es 'Creacion' o 'Modificacion' */
  const isEditable = params?.id && params?.amount;
  const TypeSelected = formTransactionTypes.find((x) => x.active);
  /** Form: Detalle */
  const [formDetail, setFormDetail] = useState(params?.detail || "");
  /** Form: Monto */
  const [formAmount, setFormAmount] = useState(params?.amount || "");
  const [formAmountLimits, setFormAmountLimits] = useState<AmountOptions>();
  /** Form: Categoria */
  const [formCredit, setformCredit] =
    useState<(CategoryDetailParams & { active: boolean })[]>();
  /** Form: Caja */
  const [formDebit, setformDebit] =
    useState<(BalanceSource & { active: boolean })[]>();
  /** Form: Fecha */
  const [formDate, setFormDate] = useState<Date>(
    params?.dateNumber ? new Date(params?.dateNumber) : new Date(),
  );
  /** Form: errores */
  const [formErrors, setFormErrors] = useState<FormError>();

  /**
   * Obtiene el archivo y guarda su contenido en hooks
   */
  const requestFileById = async () => {
    dispatch(setLoading(Pages.MOVEMENTDETAILPAGE, true));
    getFileById<GDriveBalanceFile>(balanceFile?.id || "")
      .then((response) => {
        setDataBalance(response.balance);
      })
      .catch((error: AppError) => {
        setDataBalance(undefined);
        handleError(error);
      })
      .finally(() => {
        dispatch(setLoading(Pages.MOVEMENTDETAILPAGE, false));
      });
  };

  const requestCategory = (
    data: (CategoryDetailParams & { active: boolean })[],
  ) => {
    const creditSelected = data.find((item) => item.active);
    setFormAmountLimits(creditSelected?.amounts);
    if (creditSelected?.amounts?.init) {
      setFormAmount(toCurrency(creditSelected?.amounts?.init));
    }
  };

  /** Actualiza el listado de cajas */
  const requestSources = () => {
    const creditSelected = formCredit?.find((item) => item.active);
    if (TypeSelected?.id === BalanceTransactionType.EXPENSE) {
      /** es un gasto */
      const newList = dataBalance?.categories.map((item) => ({
        ...item,
        active: item.id === (creditSelected?.id ?? params?.creditId),
      }));
      setformCredit(newList);
    } else {
      /** es un ingreso o transferencia */
      const newList = dataBalance?.sources.map((item) => ({
        ...item,
        active: item.id === (creditSelected?.id ?? params?.creditId),
      }));
      setformCredit(newList);
    }

    const debitSelected = formDebit?.find((item) => item.active);
    const newList = dataBalance?.sources.map((item) => ({
      ...item,
      active: item.id === (debitSelected?.id ?? params?.debitId),
    }));
    setformDebit(newList);
  };

  /**
   * Limpia el error asignado a una key
   * @param {String} key Clave de error a limpiar
   */
  const cleanFormError = (key: string) => {
    const errorsNew: FormError = { ...formErrors, [key]: undefined };
    setFormErrors(errorsNew);
  };

  /**
   * Submit: Crea Modifica o Elimina el item
   * @param {boolean} isDelete Indica si debe borrar el item
   */
  const handleSubmit = async (isDelete?: boolean) => {
    dispatch(setLoading(Pages.MOVEMENTDETAILPAGE, true));
    try {
      const { balance } = await getFileById<GDriveBalanceFile>(
        balanceFile?.id || "",
      );
      let newList: BalanceMovement[] = [];
      // Borrado
      if (isDelete) {
        newList = [...balance.movements];
        const idx = newList.findIndex((item) => item.id === params?.id);
        if (idx !== -1) {
          newList.splice(idx, 1);
        }
      } else {
        // Creacion o Modificacion

        let errorsNew: FormError = {};

        const transactionSelected = formTransactionTypes.find(
          (item) => item.active,
        );
        const creditSelected = formCredit?.find((item) => item.active);
        const debitSelected = formDebit?.find((item) => item.active);

        if (!(creditSelected && creditSelected.id)) {
          let errorCredit = "";
          if (TypeSelected?.id === BalanceTransactionType.EXPENSE) {
            errorCredit = "Debe seleccionar una categoria";
          } else {
            errorCredit = "Debe seleccionar una caja";
          }
          errorsNew = {
            ...errorsNew,
            formCredit: errorCredit,
          };
        }
        if (
          !(debitSelected && debitSelected.id) &&
          TypeSelected?.id === BalanceTransactionType.EXPENSE
        ) {
          errorsNew = {
            ...errorsNew,
            formDebit: "Debe seleccionar una caja",
          };
        }
        if (!(formAmount && formAmount.length > 3)) {
          errorsNew = {
            ...errorsNew,
            formAmount: "Debe ingresar el monto",
          };
        }

        if (formAmount && formAmount.length > 0 && formAmountLimits?.max) {
          const amountValue = toDecimal(toAmountCulture(formAmount));
          const amountMax = toDecimal(toAmountCulture(formAmountLimits?.max));
          if (amountValue > amountMax) {
            errorsNew = {
              ...errorsNew,
              formAmount: `El monto ingresado es mayor al maximo ${formAmountLimits?.max}`,
            };
          }
        }

        if (errorsNew && Object.keys(errorsNew).length > 0) {
          setFormErrors(errorsNew);

          const newMessage: ModalMessage = {
            title: "Validacion",
            content: "Revise las validaciones por favor.",
            selfClose: false,
            isVisible: true,
            actions: {
              primary: {
                content: genericMessages.ok,
                onPress: () => {
                  dispatch(clearMessage());
                },
              },
            },
          };
          dispatch(setMessage(newMessage));
          return;
        }
        const newItem: BalanceMovement = {
          id: params?.id || `${new Date().getTime()}`,
          creditId: creditSelected?.id || "",
          detail: formDetail,
          date: factoringDate(
            formDate.getTime(),
            calendarSettings.formats.sortItems,
          ),
          dateNumber: formDate.getTime(), // fecha en milisegundos
          user: {
            id: user?.id || "", // id del usuario de google
            name: user?.name || "", // nombre del usuario de google
          },
          transactionType:
            transactionSelected?.id || BalanceTransactionType.EXPENSE,
          debitId: debitSelected?.id || "",
          amount: formAmount,
        };
        if (isEditable) {
          // Modificacion
          newList = balance.movements.map((item) => {
            if (item.id === params.id) {
              return newItem;
            }
            return item;
          });
        } else {
          // Creacion
          newList = [...balance.movements, newItem];
        }
      }
      const newFile: GDriveBalanceFile = {
        balance: {
          ...balance,
          movements: newList,
        },
      };
      await putFileById(
        balanceFile?.id || "",
        balanceFile?.name || "",
        newFile,
      );

      setDataBalance(newFile.balance);

      navigate(-1);
      //navigation.goBack();
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(setLoading(Pages.MOVEMENTDETAILPAGE, false));
    }
  };

  /**
   * Muestra el error en el modal
   * @param {AppError} error Error del servicio
   */
  const handleError = (error: AppError) => {
    const newMessage: ModalMessage = {
      title: error.title || "Error",
      content: error.message,
      selfClose: false,
      isVisible: true,
      actions: {
        primary: {
          content: genericMessages.ok,
          onPress: () => {
            dispatch(clearMessage());
          },
        },
      },
    };
    dispatch(setMessage(newMessage));
  };

  useEffect(() => {
    requestSources();
  }, [dataBalance, TypeSelected]);

  useEffect(() => {
    requestFileById();
  }, [balanceFile]);

  return (
    <FormContainer
      header={`Movimiento: ${params?.id ? "Edicion" : "Creacion"}`}
      icon="clipboard-data">
      <div className="row">
        <div className="col-xs-12 col-md-6 col-lg-6">
          <FormComponent header="Tipo">
            <FormDropDown
              options={formTransactionTypes}
              onOptionsChange={(data) => {
                setFormTransactionTypes(data);
              }}
              placeholder={"Seleccione un tipo de transaccion"}
            />
          </FormComponent>
        </div>
        {TypeSelected && TypeSelected.id ? (
          <div className="col-xs-12 col-md-6 col-lg-6">
            <FormComponent header="Fecha">
              <FormCalendarModal
                name="formDate"
                valueStart={formDate}
                placeholder={"yyyy-MM-dd"}
                onValueChange={(value?: Date) => {
                  setFormDate(value || new Date());
                  // cleanError('formFullName');
                }}
                //error={formErrors?.formFullName}
              />
            </FormComponent>
          </div>
        ) : null}
      </div>
      {TypeSelected && TypeSelected.id ? (
        <div className="row">
          {TypeSelected &&
          TypeSelected.id === BalanceTransactionType.EXPENSE ? (
            <div className="col-xs-12 col-md-6 col-lg-6">
              <FormComponent header="Categoria">
                <FormDropDown
                  onAction={() => navigate(Pages.CATEGORYLISTPAGE)}
                  options={formCredit}
                  onOptionsChange={(data) => {
                    setformCredit(data);
                    cleanFormError("formCredit");
                    requestCategory(data);
                  }}
                  placeholder={"Seleccione una categoria"}
                />
              </FormComponent>
            </div>
          ) : (
            <div className="col-xs-12 col-md-6 col-lg-6">
              <FormComponent header="Acreditacion">
                <FormDropDown
                  onAction={() => navigate(Pages.SOURCELISTPAGE)}
                  options={formCredit}
                  onOptionsChange={(data) => {
                    setformCredit(data);
                    cleanFormError("formCredit");
                  }}
                  placeholder={"Seleccione una caja"}
                />
              </FormComponent>
            </div>
          )}
          {TypeSelected &&
          TypeSelected.id === BalanceTransactionType.INCOME ? null : (
            <div className="col-xs-12 col-md-6 col-lg-6">
              <FormComponent header="Debito">
                <FormDropDown
                  onAction={() => navigate(Pages.SOURCELISTPAGE)}
                  options={formDebit}
                  onOptionsChange={(data) => {
                    setformDebit(data);
                    cleanFormError("formDebit");
                  }}
                  placeholder={"Seleccione una caja"}
                />
              </FormComponent>
            </div>
          )}
        </div>
      ) : (
        <div className="row" style={{ marginTop: 15 }}>
          <div className="col-xs-12 col-md-12 col-lg-12">
            <span style={textstyles.textBodyLegend}>
              Seleccione el tipo y vera las opciones
            </span>
          </div>
        </div>
      )}
      {TypeSelected && TypeSelected.id ? (
        <div className="row">
          <div className="col-xs-12 col-md-6 col-lg-6">
            <FormComponent
              header={`Monto ${
                formAmountLimits?.max ? `(Max $${formAmountLimits?.max})` : ""
              }`}>
              <FormInput
                name="formAmount"
                value={formAmount}
                isNumeric
                placeholder={"Ingrese el monto"}
                onValueChange={(value: string) => {
                  setFormAmount(toCurrency(value));
                  cleanFormError("formAmount");
                }}
              />
            </FormComponent>
          </div>
          {TypeSelected &&
          TypeSelected.id === BalanceTransactionType.TRANSFER ? null : (
            <div className="col-xs-12 col-md-6 col-lg-6">
              <FormComponent header="Detalle">
                <FormInput
                  name="formDetail"
                  value={formDetail}
                  placeholder={"Ingrese una descripcion"}
                  onValueChange={(value: string) => {
                    setFormDetail(restrictText(value));
                    cleanFormError("formDetail");
                  }}
                />
              </FormComponent>
            </div>
          )}
        </div>
      ) : null}
      {TypeSelected && TypeSelected.id ? (
        <div
          className="row"
          style={{ marginTop: 32, marginLeft: 0, marginRight: 0 }}>
          <div className="row" style={{ flexDirection: "row-reverse" }}>
            <div className="col-xs-12 col-md-4 col-lg-4">
              <ButtonReactive
                content={
                  isEditable ? genericMessages.save : genericMessages.continue
                }
                onPress={() => handleSubmit()}
              />
            </div>
            {isEditable ? (
              <div className="col-xs-12 col-md-4 col-lg-4">
                <ButtonReactive
                  content={genericMessages.delete}
                  color={platform.colors.expenseAmount}
                  onPress={() => handleSubmit(true)}
                />
              </div>
            ) : null}
          </div>
        </div>
      ) : null}
    </FormContainer>
  );
};

export default MovementDetailPage;
