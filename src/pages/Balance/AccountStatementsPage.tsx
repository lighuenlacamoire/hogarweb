import { containerStyles, platform, textstyles } from "styles";
import AccountStatements from "./components/AccountStatements";
import { calendarSettings, Pages } from "configuration/constants";
import { RootState } from "redux/store";
import React, { useCallback, useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { GDriveBalanceFile } from "interfaces/googleDrive";
import { setLoading } from "redux/actions/status";
import { CalendarMonthSlide } from "components/Calendar";
import {
  addTime,
  toCurrency,
  toDatesRange,
  toDecimal,
  toNumber,
} from "utils/formatter";
import {
  BalanceMovement,
  BalanceSource,
  BalanceState,
  BalanceTransactionType,
} from "interfaces/state";
import { calculateSum, setListFiltered } from "utils/functions";
import { AppError } from "interfaces/services";
import { clearMessage, setMessage } from "redux/actions/notification";
import { ModalMessage } from "interfaces/buttons";
import { useNavigate } from "react-router-dom";
import { genericMessages } from "configuration/messages";
import { getFileById } from "services/googleApi/googleFiles";
import { FormContainer } from "components/Form";
import SourceStatement from "./components/SourceStatement";

/**
 * Pantalla de estado de cuenta
 */
const AccountStatementsPage = () => {
  /** Referencia del archivo en redux */
  const { balanceFile } = useSelector((state: RootState) => state.storage);
  /** Dispatch de Redux */
  const dispatch = useDispatch();
  /** Navegacion */
  const navigate = useNavigate();

  /** Datos del archivo que buscaremos en la Api */
  const [dataBalance, setDataBalance] = useState<BalanceState>();
  const [listMovements, setListMovements] = useState<BalanceMovement[]>([]);
  const [focusedMonth, setFocusedMonth] = useState<Date | undefined>();

  const dates = focusedMonth ? toDatesRange(focusedMonth) : undefined;
  const changeMonth = (value: number) => {
    if (value) {
      if (focusedMonth) {
        const newMonth = addTime(
          focusedMonth,
          value,
          calendarSettings.units.months,
          null,
        ) as Date;
        setFocusedMonth(newMonth);
      } else {
        setFocusedMonth(new Date());
      }
    } else {
      setFocusedMonth(undefined);
    }
  };

  /**
   * Muestra el error en el modal
   * @param {AppError} error Error del servicio
   */
  const handleError = (error: AppError) => {
    const newMessage: ModalMessage = {
      title: error.title || "Error",
      content: error.message,
      selfClose: false,
      isVisible: true,
      actions: {
        primary: {
          content: genericMessages.ok,
          onPress: () => {
            dispatch(clearMessage());
          },
        },
      },
    };
    dispatch(setMessage(newMessage));
  };

  /**
   * Obtiene el archivo y guarda su contenido en hooks
   */
  const requestFileById = async () => {
    dispatch(setLoading(Pages.ACCOUNTSTATEMENTSPAGE, true));
    getFileById<GDriveBalanceFile>(balanceFile?.id || "")
      .then((response) => {
        const { balance } = response;
        setDataBalance(balance);
        setListMovements(
          balance.movements.sort((a, b) => toNumber(b.date) - toNumber(a.date)),
        );
      })
      .catch((error: AppError) => {
        setDataBalance(undefined);
        setListMovements([]);
        handleError(error);
      })
      .finally(() => {
        dispatch(setLoading(Pages.ACCOUNTSTATEMENTSPAGE, false));
      });
  };

  const filterList = () => {
    const originalList: BalanceMovement[] = dataBalance?.movements || [];
    if (focusedMonth) {
      const dates = toDatesRange(focusedMonth);
      setListFiltered(originalList, setListMovements, { date: dates });
    } else {
      setListMovements(originalList);
    }
  };

  /**
   * Renderiza cada Item del array
   * @param {BalanceSource} item item del array
   * @param {number} index indice del array
   */
  //const renderItem = (item: BalanceSource, index: number) => ();

  useEffect(() => {
    filterList();
  }, [focusedMonth, dataBalance]);

  useEffect(() => {
    requestFileById();
  }, [balanceFile]);

  return (
    <div
      style={{
        backgroundColor: platform.colors.listBackground,
        paddingTop: "20px",
        paddingBottom: "20px",
        flex: 1,
      }}>
      <div className="cards-list" style={{ alignItems: "flex-start" }}>
        {dataBalance?.sources.map((item, index) => (
          <SourceStatement
            key={`${index}${item.name}`}
            item={item}
            listMovements={listMovements}
          />
        ))}
      </div>
    </div>
  );
  /*
  return (
    <FormContainer
      header={`Estado de cuenta`}
      icon="building-bank"
      className="col-xs-12 col-md-9 col-lg-6">
      <div>
        <CalendarMonthSlide
          defaultHeader="Posicion Consolidada"
          focusedMonth={focusedMonth}
          onPress={(value: number) => changeMonth(value)}
        />
        <AccountStatements
          movements={list}
          sources={dataBalance?.sources}
          focusedMonth={focusedMonth}
        />
      </div>
    </FormContainer>
  );
  */
};

export default AccountStatementsPage;
