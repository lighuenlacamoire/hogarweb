import { ButtonReactive } from "components/Button";
import { Pages } from "configuration/constants";
import { RootState } from "redux/store";
import { capitalize, restrictText } from "utils/formatter";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setLoading } from "redux/actions/status";
import { GDriveBalanceFile } from "interfaces/googleDrive";
import { genericMessages } from "configuration/messages";
import { ModalMessage } from "interfaces/buttons";
import { clearMessage, setMessage } from "redux/actions/notification";
import { BalanceSource, BalanceState } from "interfaces/state";
import { FormComponent, FormContainer, FormInput } from "components/Form";
import { AppError } from "interfaces/services";
import { useLocation, useNavigate } from "react-router-dom";
import { getFileById, putFileById } from "services/googleApi/googleFiles";

const SourceDetailPage = () => {
  /** Referencia del archivo en redux */
  const { balanceFile } = useSelector((state: RootState) => state.storage);

  /** Datos del archivo que buscaremos en la Api */
  const [dataBalance, setDataBalance] = useState<BalanceState>();

  /** Dispatch de Redux */
  const dispatch = useDispatch();
  /** Navegacion */
  const navigate = useNavigate();

  const location = useLocation();
  /** Parametros de navegacion */
  const params = location?.state;

  const [formName, setFormName] = useState(params?.name || "");

  /** Indica si es 'Creacion' o 'Modificacion' */
  const isEditable = params?.id && params?.name;

  /**
   * Submit: Crea Modifica o Elimina el item
   * @param {boolean} isDelete Indica si debe borrar el item
   */
  const handleSubmit = async (isDelete?: boolean) => {
    dispatch(setLoading(Pages.SOURCEDETAILPAGE, true));
    try {
      const { balance } = await getFileById<GDriveBalanceFile>(
        balanceFile?.id || "",
      );
      let newList: BalanceSource[] = [];

      // Borrado
      if (isDelete) {
        newList = [...balance.sources];
        const idx = newList.findIndex((item) => item.id === params?.id);
        if (idx !== -1) {
          newList.splice(idx, 1);
        }
      } else {
        // Creacion o Modificacion
        const newName = capitalize(formName);
        const duplicated = dataBalance?.sources.find((x) => x.name === newName);

        if (duplicated?.id && duplicated?.id !== params?.id) {
          const newMessage: ModalMessage = {
            title: "Validacion",
            content: genericMessages.list.duplicated("nombre", duplicated.name),
            selfClose: false,
            isVisible: true,
            actions: {
              primary: {
                content: genericMessages.ok,
                onPress: () => {
                  dispatch(clearMessage());
                },
              },
            },
          };
          dispatch(setMessage(newMessage));
          return;
        }
        const newItem: BalanceSource = {
          id: params?.id || `${new Date().getTime()}`,
          name: newName,
        };
        if (isEditable) {
          // Modificacion
          newList = balance.sources.map((item) => {
            if (item.id === params.id) {
              return newItem;
            }
            return item;
          });
        } else {
          // Creacion
          newList = [...balance.sources, newItem];
        }
      }

      const newFile: GDriveBalanceFile = {
        balance: {
          ...balance,
          sources: newList.sort((a, b) => (a.name < b.name ? -1 : 1)),
        },
      };
      await putFileById(
        balanceFile?.id || "",
        balanceFile?.name || "",
        newFile,
      );

      setDataBalance(newFile.balance);

      navigate(-1);
      //navigation.goBack();
    } catch (error) {
      console.log(error);
    } finally {
      dispatch(setLoading(Pages.SOURCEDETAILPAGE, false));
    }
  };

  /**
   * Obtiene el archivo y guarda su contenido en hooks
   */
  const requestFileById = async () => {
    dispatch(setLoading(Pages.SOURCEDETAILPAGE, true));
    getFileById<GDriveBalanceFile>(balanceFile?.id || "")
      .then((response) => {
        setDataBalance(response.balance);
      })
      .catch((error: AppError) => {
        setDataBalance(undefined);
        handleError(error);
      })
      .finally(() => {
        dispatch(setLoading(Pages.SOURCEDETAILPAGE, false));
      });
  };

  /**
   * Muestra el error en el modal
   * @param {AppError} error Error del servicio
   */
  const handleError = (error: AppError) => {
    const newMessage: ModalMessage = {
      title: error.title || "Error",
      content: error.message,
      selfClose: false,
      isVisible: true,
      actions: {
        primary: {
          content: genericMessages.ok,
          onPress: () => {
            dispatch(clearMessage());
          },
        },
      },
    };
    dispatch(setMessage(newMessage));
  };

  useEffect(() => {
    requestFileById();
  }, [balanceFile]);

  return (
    <FormContainer
      header={`Caja: ${params?.id ? "Edicion" : "Creacion"}`}
      icon="coins">
      <div className="row">
        <div className="col-xs-12 col-md-6 col-lg-6">
          <FormComponent header="Nombre">
            <FormInput
              name="formName"
              value={formName}
              placeholder={"Ingrese el nombre"}
              onValueChange={(value: string) => {
                setFormName(restrictText(value));
                // cleanError('formFullName');
              }}
            />
          </FormComponent>
        </div>
      </div>
      <div
        className="row"
        style={{ marginTop: 32, marginLeft: 0, marginRight: 0 }}>
        <div className="row" style={{ flexDirection: "row-reverse" }}>
          <div className="col-xs-12 col-md-4 col-lg-4">
            <ButtonReactive
              content={
                isEditable ? genericMessages.save : genericMessages.continue
              }
              onPress={() => handleSubmit()}
            />
          </div>
          {isEditable ? (
            <div className="col-xs-12 col-md-4 col-lg-4">
              <ButtonReactive
                content={genericMessages.delete}
                color={"#F44336"}
                onPress={() => handleSubmit(true)}
              />
            </div>
          ) : null}
        </div>
      </div>
    </FormContainer>
  );
};

export default SourceDetailPage;
