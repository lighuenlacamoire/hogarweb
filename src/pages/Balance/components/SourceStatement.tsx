import {
  BalanceMovement,
  BalanceSource,
  BalanceTransactionType,
} from "interfaces/state";
import React, { useState } from "react";
import { containerStyles, platform, textstyles } from "styles";
import { toCurrency, toDecimal, toNumber } from "utils/formatter";
import { calculateSum } from "utils/functions";

type Props = {
  item: BalanceSource;
  listMovements: BalanceMovement[];
};

const SourceStatement = ({ item, listMovements }: Props) => {
  const [expanded, setExpanded] = useState(false);

  const list: {
    amount: string;
    date: number;
    name: string;
    plus: boolean;
  }[] = [];
  let amountTransfer = 0;
  let total = 0;
  let amountD = "Sin movimientos";
  if (listMovements && listMovements.length > 0) {
    const transfers = listMovements.filter(
      (m) => m.transactionType === BalanceTransactionType.TRANSFER,
    );
    transfers.map((m) => {
      if (m.creditId === item.id) {
        // ingresa dinero (a + toDecimal(b[prop].replace('.', '').replace(',', '.'))) as number
        amountTransfer =
          amountTransfer +
          toDecimal(m.amount.replace(".", "").replace(",", "."));
        list.push({
          amount: m.amount,
          date: toNumber(m.date),
          name: "Transferencia (entrante)",
          plus: true,
        });
      }
      if (m.debitId === item.id) {
        // sale dinero
        amountTransfer =
          amountTransfer -
          toDecimal(m.amount.replace(".", "").replace(",", "."));
        list.push({
          amount: m.amount,
          date: toNumber(m.date),
          name: "Transferencia (saliente)",
          plus: false,
        });
      }
    });
    const incomes = listMovements.filter((m) => {
      if (
        m.creditId === item.id &&
        m.transactionType === BalanceTransactionType.INCOME
      ) {
        list.push({
          amount: m.amount,
          date: toNumber(m.date),
          name: "Ingreso",
          plus: true,
        });
        return m;
      }
    });
    const expenses = listMovements.filter((m) => {
      if (
        m.debitId === item.id &&
        m.transactionType === BalanceTransactionType.EXPENSE
      ) {
        list.push({
          amount: m.amount,
          date: toNumber(m.date),
          name: "Gasto",
          plus: false,
        });
        return m;
      }
    });
    const ingresos = calculateSum(incomes, "amount");
    const gastos = calculateSum(expenses, "amount");
    total = ingresos + amountTransfer - gastos;
    amountD = `$ ${toCurrency(total.toFixed(2))}`;
  }

  const toggleListItem = () => {
    setExpanded(!expanded);
  };

  return (
    <div
      style={{
        boxShadow: "2px 10px 20px rgba(0, 0, 0, 0.1)",
        maxWidth: "350px",
        minWidth: "300px",
        position: "relative",
        textAlign: "center",
        //padding: "24px 20px",
        borderRadius: 7,
        cursor: "pointer",
        //borderWidth: platform.generic.borderWidth,
        //borderColor: platform.colors.borderContainer,
        backgroundColor: platform.colors.white,
      }}
      onClick={toggleListItem}>
      <div
        style={{
          position: "absolute",
          height: 10,
          top: 0,
          left: 0,
          width: "100%",
          borderTopLeftRadius: 7,
          borderTopRightRadius: 7,
          background: "linear-gradient(69.83deg, #0084f4 0%, #00c48c 100%)",
        }}></div>
      <div
        style={{
          padding: "24px 20px",
        }}>
        <span
          style={{
            fontSize: "1.18em",
            color: "#6c6c6c",
            fontWeight: "500",
            display: "block",
          }}>
          {item.name}
        </span>
        <span
          style={{
            fontSize: "2.5em",
            fontWeight: "600",
            display: "block",
            color: total < 0 ? platform.colors.expenseAmount : "#323c43",
          }}>
          {amountD}
        </span>
      </div>

      <div
        style={{
          backgroundColor: platform.colors.listBackgroundLigher,
          borderTopColor: platform.colors.border,
          borderTopWidth: 1,
          width: "100%",
          borderBottomLeftRadius: 7,
          borderBottomRightRadius: 7,
          ...(expanded
            ? {
                maxHeight: "500px",
                overflow: "auto",
                transition: "max-height 1s ease-in-out",
                MozTransition: "max-height 1s ease-in-out",
                WebkitTransition: "max-height 1s ease-in-out",
                borderTopStyle: "solid",
              }
            : {
                maxHeight: "0px",
                overflow: "hidden",
                transition: "max-height 0.8s cubic-bezier(0, 1, 0, 1)",
                MozTransition: "max-height 0.8s cubic-bezier(0, 1, 0, 1)",
                WebkitTransition: "max-height 0.8s cubic-bezier(0, 1, 0, 1)",
                borderTopStyle: "none",
              }),
        }}>
        {list.map((i, index) => (
          <div
            style={{
              ...containerStyles.itemSpacingElements,
              flex: 1,
              backgroundColor: "transparent",
              paddingInline: 12,
              paddingTop: 4,
              paddingBottom: 4,
            }}
            key={`${index}${i.date}${i.amount}${i.name}`}>
            <span style={textstyles.textInputForm}>{i.name}</span>
            <span style={textstyles.textInputForm}>{`${i.plus ? "" : "-"}${
              i.amount
            }`}</span>
          </div>
        ))}
      </div>
    </div>
  );
};

export default SourceStatement;
