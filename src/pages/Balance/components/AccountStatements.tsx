import React from "react";
import {
  BalanceMovement,
  BalanceSource,
  BalanceTransactionType,
} from "interfaces/state";
import { containerStyles, platform, textstyles } from "styles";
import { ListItemAccordion } from "components/ListItem";
import { calculateSum } from "utils/functions";
import { toCurrency, toDatesRange, toDecimal, toNumber } from "utils/formatter";
import { Pages } from "configuration/constants";
import { useNavigate } from "react-router-dom";

type Props = {
  movements: BalanceMovement[];
  sources?: BalanceSource[];
  focusedMonth?: Date;
};

const AccountStatements = (props: Props) => {
  /** Navegacion */
  const navigate = useNavigate();
  const { movements, sources = [], focusedMonth } = props;
  const dates = focusedMonth ? toDatesRange(focusedMonth) : undefined;
  return (
    <div style={{ backgroundColor: platform.colors.white }}>
      {sources.map((item) => {
        const list: {
          amount: string;
          date: number;
          name: string;
          plus: boolean;
        }[] = [];
        let amountTransfer = 0;
        let total = 0;
        let amountD = "Sin movimientos";
        if (movements && movements.length > 0) {
          const transfers = movements.filter(
            (m) => m.transactionType === BalanceTransactionType.TRANSFER,
          );
          transfers.map((m) => {
            if (m.creditId === item.id) {
              // ingresa dinero (a + toDecimal(b[prop].replace('.', '').replace(',', '.'))) as number
              amountTransfer =
                amountTransfer +
                toDecimal(m.amount.replace(".", "").replace(",", "."));
              list.push({
                amount: m.amount,
                date: toNumber(m.date),
                name: "Transferencia (entrante)",
                plus: true,
              });
            }
            if (m.debitId === item.id) {
              // sale dinero
              amountTransfer =
                amountTransfer -
                toDecimal(m.amount.replace(".", "").replace(",", "."));
              list.push({
                amount: m.amount,
                date: toNumber(m.date),
                name: "Transferencia (saliente)",
                plus: false,
              });
            }
          });
          const incomes = movements.filter((m) => {
            if (
              m.creditId === item.id &&
              m.transactionType === BalanceTransactionType.INCOME
            ) {
              list.push({
                amount: m.amount,
                date: toNumber(m.date),
                name: "Ingreso",
                plus: true,
              });
              return m;
            }
          });
          const expenses = movements.filter((m) => {
            if (
              m.debitId === item.id &&
              m.transactionType === BalanceTransactionType.EXPENSE
            ) {
              list.push({
                amount: m.amount,
                date: toNumber(m.date),
                name: "Gasto",
                plus: false,
              });
              return m;
            }
          });
          const ingresos = calculateSum(incomes, "amount");
          const gastos = calculateSum(expenses, "amount");
          total = ingresos + amountTransfer - gastos;
          amountD = `$ ${toCurrency(total.toFixed(2))}`;
        }
        return (
          <ListItemAccordion
            key={`${item.id}`}
            content={
              <div
                onClick={() =>
                  navigate(Pages.MOVEMENTLISTPAGE, {
                    state: {
                      ...item,
                      amount: amountD,
                      date: dates,
                    },
                  })
                }
                style={{
                  ...containerStyles.itemSpacingElements,
                  cursor: "pointer",
                  flex: 1,
                  marginRight: 8,
                }}>
                <span style={textstyles.textBodyLegend}>{item.name}</span>
                <span
                  style={{
                    ...textstyles.textButtonLegend,
                    fontSize: platform.fontSizes.HEADER,
                    color:
                      total < 0
                        ? platform.colors.expenseAmount
                        : platform.colors.transferAmount,
                  }}>
                  {amountD}
                </span>
              </div>
            }>
            <>
              {list.map((i, index) => (
                <div
                  style={{
                    ...containerStyles.itemSpacingElements,
                    flex: 1,
                    backgroundColor: platform.colors.white,
                    paddingInline: 12,
                    paddingTop: 4,
                    paddingBottom: 4,
                  }}
                  key={`${index}${i.date}${i.amount}${i.name}`}>
                  <span style={textstyles.textInputForm}>{i.name}</span>
                  <span style={textstyles.textInputForm}>{`${
                    i.plus ? "" : "-"
                  }${i.amount}`}</span>
                </div>
              ))}
            </>
          </ListItemAccordion>
        );
      })}
    </div>
  );
};

export default AccountStatements;
