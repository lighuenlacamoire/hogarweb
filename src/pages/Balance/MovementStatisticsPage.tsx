import { Pie } from "components/Chart";
import { calendarSettings, Pages } from "configuration/constants";
import { PieSlice } from "interfaces/graphics";
import {
  BalanceMovement,
  BalanceState,
  BalanceTransactionType,
} from "interfaces/state";
import { RootState } from "redux/store";
import { addTime, toCurrency, toDatesRange, toDecimal } from "utils/formatter";
import { calculateSum, setListFiltered } from "utils/functions";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { CalendarMonthSlide } from "components/Calendar";
import { setLoading } from "redux/actions/status";
import { getFileById } from "services/googleApi/googleFiles";
import { GDriveBalanceFile } from "interfaces/googleDrive";
import { AppError } from "interfaces/services";
import { clearMessage, setMessage } from "redux/actions/notification";
import { genericMessages } from "configuration/messages";
import { ModalMessage } from "interfaces/buttons";

const MovementStatisticsPage = () => {
  /** Referencia del archivo en redux */
  const { balanceFile } = useSelector((state: RootState) => state.storage);

  /** Dispatch de Redux */
  const dispatch = useDispatch();
  const [focusedMonth, setFocusedMonth] = useState<Date | undefined>(
    new Date(),
  );
  /** Datos del archivo que buscaremos en la Api */
  const [dataBalance, setDataBalance] = useState<BalanceState>();
  const [list, setList] = useState<PieSlice[]>([]);
  const [centerValue, setCenterValue] = useState({
    name: "",
    value: "Sin movimientos",
  });

  const changeMonth = (value: number) => {
    if (value) {
      if (focusedMonth) {
        const newMonth = addTime(
          focusedMonth,
          value,
          calendarSettings.units.months,
          null,
        ) as Date;
        setFocusedMonth(newMonth);
      } else {
        setFocusedMonth(new Date());
      }
    } else {
      setFocusedMonth(undefined);
    }
  };

  /**
   * Obtiene el archivo y guarda su contenido en hooks
   */
  const requestFileById = async () => {
    dispatch(setLoading(Pages.MOVEMENTSTATISTICSPAGE, true));
    getFileById<GDriveBalanceFile>(balanceFile?.id || "")
      .then((response) => {
        const { balance } = response;
        setDataBalance(balance);
      })
      .catch((error: AppError) => {
        setDataBalance(undefined);
        handleError(error);
      })
      .finally(() => {
        dispatch(setLoading(Pages.MOVEMENTSTATISTICSPAGE, false));
      });
  };
  /**
   * Muestra el error en el modal
   * @param {AppError} error Error del servicio
   */
  const handleError = (error: AppError) => {
    const newMessage: ModalMessage = {
      title: error.title || "Error",
      content: error.message,
      selfClose: false,
      isVisible: true,
      actions: {
        primary: {
          content: genericMessages.ok,
          onPress: () => {
            dispatch(clearMessage());
          },
        },
      },
    };
    dispatch(setMessage(newMessage));
  };

  /**
   * Filtra la lista
   */
  const filterList = () => {
    const originalList: BalanceMovement[] = dataBalance?.movements || [];
    const newlist: PieSlice[] = [];
    let movementsFiltered: BalanceMovement[] = [];

    if (focusedMonth) {
      const dates = toDatesRange(focusedMonth);
      setListFiltered(originalList, (list) => (movementsFiltered = list), {
        date: dates,
      });
    } else {
      movementsFiltered = originalList;
    }
    if (movementsFiltered && movementsFiltered.length > 0) {
      const expenseMovements = movementsFiltered.filter(
        (x) => x.transactionType === BalanceTransactionType.EXPENSE,
      );
      const total = calculateSum(expenseMovements, "amount");
      dataBalance?.categories.map((x) => {
        const news = expenseMovements.filter((m) => m.creditId === x.id);
        const sume = calculateSum(news, "amount");
        if (news && news.length > 0) {
          newlist.push({
            value: toDecimal(((sume * 100) / total).toFixed(2)),
            color: x.color || "",
            name: `${x.name}`,
            data: `$ ${toCurrency(sume.toFixed(2))}`,
            id: x.id,
          });
        }
      });
      setCenterValue({
        name: "Total",
        value: `$ ${toCurrency(total.toFixed(2))}`,
      });
      setList(newlist);
    } else {
      setCenterValue({ name: "", value: "Sin movimientos" });
      setList([]);
    }
  };

  useEffect(() => {
    filterList();
  }, [focusedMonth, dataBalance]);

  useEffect(() => {
    requestFileById();
  }, [balanceFile]);

  return (
    <div style={{ display: "flex", flexDirection: "column" }}>
      <CalendarMonthSlide
        defaultHeader="Posicion Consolidada"
        focusedMonth={focusedMonth}
        onPress={(value: number) => changeMonth(value)}
      />
      <Pie
        size={400}
        onItemSelected={(item, index) => console.log("pie", item, index)}
        data={list}
        centerTitle={centerValue.name}
        centerValue={centerValue.value}
      />
    </div>
  );
};

export default MovementStatisticsPage;
