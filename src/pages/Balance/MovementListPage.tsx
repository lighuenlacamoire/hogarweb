import { ButtonReactive } from "components/Button";
import { CardMovement } from "components/Card";
import {
  FormCalendarModal,
  FormComponent,
  FormContainer,
  FormDropDown,
} from "components/Form";
import Icon from "components/Icon";
import {
  calendarSettings,
  Pages,
  transactionTypes,
} from "configuration/constants";
import { genericMessages } from "configuration/messages";
import { ModalMessage } from "interfaces/buttons";
import { GDriveBalanceFile } from "interfaces/googleDrive";
import { MovementFilter, MovementListFilter } from "interfaces/navigations";
import { AppError } from "interfaces/services";
import { BalanceMovement, BalanceState } from "interfaces/state";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams, useSearchParams } from "react-router-dom";
import { clearMessage, setMessage } from "redux/actions/notification";
import { setLoading } from "redux/actions/status";
import { RootState } from "redux/store";
import { getFileById } from "services/googleApi/googleFiles";
import { platform, textstyles } from "styles";
import { factoringDate, toDate, toNumber } from "utils/formatter";
import { setListFiltered } from "utils/functions";

const MovementListPage = () => {
  /** Referencia del archivo en redux */
  const { balanceFile } = useSelector((state: RootState) => state.storage);
  /** Dispatch de Redux */
  const dispatch = useDispatch();
  /** Datos del archivo que buscaremos en la Api */
  const [dataBalance, setDataBalance] = useState<BalanceState>();
  const [list, setList] = useState<BalanceMovement[]>([]);
  const [searchParams, setSearchParams] = useSearchParams();
  const [searchFilter, setSearchFilter] = useState<
    MovementListFilter | undefined
  >();

  /**
   * Muestra el error en el modal
   * @param {AppError} error Error del servicio
   */
  const handleError = (error: AppError) => {
    const newMessage: ModalMessage = {
      title: error.title || "Error",
      content: error.message,
      selfClose: false,
      isVisible: true,
      actions: {
        primary: {
          content: genericMessages.ok,
          onPress: () => {
            dispatch(clearMessage());
          },
        },
      },
    };
    dispatch(setMessage(newMessage));
  };

  /**
   * Obtiene el archivo y guarda su contenido en hooks
   */
  const requestFileById = async () => {
    dispatch(setLoading(Pages.MOVEMENTLISTPAGE, true));
    getFileById<GDriveBalanceFile>(balanceFile?.id || "")
      .then((response) => {
        const { balance } = response;
        setDataBalance(balance);
        setList(
          balance.movements.sort((a, b) => toNumber(b.date) - toNumber(a.date)),
        );
      })
      .catch((error: AppError) => {
        setDataBalance(undefined);
        setList([]);
        handleError(error);
      })
      .finally(() => {
        dispatch(setLoading(Pages.MOVEMENTLISTPAGE, false));
      });
  };

  const onFilter = (data?: MovementListFilter) => {
    setSearchFilter(data);
  };

  /**
   * Filtra la lista
   */
  const filterList = () => {
    const originalList: BalanceMovement[] = dataBalance?.movements || [];
    const array: BalanceMovement[] = originalList;
    /*
    if (params?.id) {
      array = originalList.filter(
        (x) => x.creditId === params.id || x.debitId === params.id,
      );
    }
    */
    setListFiltered(array, setList, searchFilter);
  };

  const updateFilters = () => {
    const dateStart = searchParams.get("start");
    const dateEnd = searchParams.get("end");
    const transactionType = searchParams.get("type");

    setSearchFilter({
      transactionType: transactionTypes.map((item) => ({
        ...item,
        active: item.id === transactionType,
      })),
      date: {
        start: toDate(dateStart),
        end: toDate(dateEnd),
      },
    });
  };

  useEffect(() => {
    filterList();
  }, [searchFilter, dataBalance]);

  useEffect(() => {
    updateFilters();
  }, [searchParams]);

  useEffect(() => {
    requestFileById();
  }, [balanceFile]);

  return (
    <div
      style={{
        backgroundColor: platform.colors.listBackground,
        paddingBottom: "20px",
        flex: 1,
      }}>
      <div
        className={"col-xs-12 col-md-10 col-lg-10"}
        style={{
          margin: "26px auto",
          paddingInline: platform.generic.paddingSpaces,
        }}>
        <div
          style={{
            padding: platform.generic.paddingSpaces,
            alignItems: "center",
            backgroundColor: platform.colors.primary,
            display: "flex",
            boxShadow: "0 1px 5px rgba(0,0,0,0.65)",
            borderRadius: "4px 4px 0 0",
            borderTop: "1px solid #23e0ba",
            borderBottom: "5px solid #16a085",
          }}>
          <Icon name={"filter"} size={24} color={platform.colors.white} />
          <label
            style={{
              ...textstyles.headerTitleStyle,
              marginLeft: platform.generic.paddingSpaces,
            }}>
            Filtros
          </label>
        </div>
        <div
          style={{
            padding: "24px",
            height: "fit-content",
            background: "var(--color-white)",
            border: "1px solid #e1e2f0",
            boxShadow:
              "0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24)",
            transition: "all 0.3s ease",
          }}>
          <div className="row">
            <div className="col-xs-12 col-md-6 col-lg-6">
              <FormComponent header="Fecha desde y hasta">
                <FormCalendarModal
                  name="formDate"
                  valueStart={searchFilter?.date?.start}
                  valueEnd={searchFilter?.date?.end}
                  mode={calendarSettings.mode.range}
                  placeholder={"Ingrese la fecha desde y hasta"}
                  onValueChange={(startDate?: Date, endDate?: Date) => {
                    const prev = Object.fromEntries([...searchParams]);
                    if (startDate && endDate) {
                      setSearchParams({
                        ...searchParams,
                        start: factoringDate(startDate),
                        end: factoringDate(endDate),
                        ...prev,
                      });
                    } else {
                      setSearchParams({
                        ...searchParams,
                        start: undefined,
                        end: undefined,
                        ...prev,
                      });
                    }
                    //setFormDate(value || new Date());
                    // cleanError('formFullName');
                  }}
                  //error={formErrors?.formFullName}
                />
              </FormComponent>
            </div>

            <div className="col-xs-12 col-md-6 col-lg-6">
              <FormComponent header="Tipo">
                <FormDropDown
                  options={searchFilter?.transactionType}
                  onOptionsChange={(data) => {
                    const selected = data?.find((x) => x.active);
                    const prev = Object.fromEntries([...searchParams]);
                    setSearchParams({
                      ...searchParams,
                      ...prev,
                      type: selected?.id,
                    });
                    /**                    
                    const start = searchParams.get("start") || undefined;
                    const end = searchParams.get("end") || undefined;
                    setSearchParams({
                      ...searchParams,
                      ...(start ? { start } : {}),
                      ...(end ? { end } : {}),
                      type: selected?.id,
                    });
                     */
                  }}
                  placeholder={"Seleccione un tipo de transaccion"}
                />
              </FormComponent>
            </div>
          </div>
          <div
            className="row"
            style={{ marginTop: 32, marginLeft: 0, marginRight: 0 }}>
            <div className="row" style={{ flexDirection: "row-reverse" }}>
              <div className="col-xs-12 col-md-4 col-lg-4">
                <ButtonReactive
                  content={genericMessages.apply}
                  onPress={() => console.log()}
                />
              </div>
              <div className="col-xs-12 col-md-4 col-lg-4">
                <ButtonReactive
                  content={genericMessages.clean}
                  inverse
                  onPress={() => console.log()}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="cards-list">
        {list.map((item, index) => (
          <CardMovement
            key={`${index}${item.id}`}
            movement={item}
            sources={dataBalance?.sources}
            categories={dataBalance?.categories}
          />
        ))}
      </div>
      {/**
       * 
      <div className="cards-list">
        <div className="card-item">ONE</div>
        <div className="card-item">TWO</div>
        <div className="card-item">THREE</div>
        <div className="card-item">FOUR</div>
        <div className="card-item">FIVE</div>
        <div className="card-item">SIX</div>
        <div className="card-item">SEVEN</div>
        <div className="card-item">EIGHT</div>
        <div className="card-item">NINE</div>
        <div className="card-item">TEN</div>
        <div className="card-item">ELEVEN</div>
        <div className="card-item">TWELVE</div>
      </div>
       */}
    </div>
  );
};

export default MovementListPage;
