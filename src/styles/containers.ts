import platform from "./platform";

type StyleProps = {
  shadowDefault: React.CSSProperties;
  listItemContainer: React.CSSProperties;
  footerDouble: React.CSSProperties;
  formControl: React.CSSProperties;
  itemStretch: React.CSSProperties;
  itemSpacingElements: React.CSSProperties;
};

const containerStyles: StyleProps = {
  itemStretch: {
    display: "flex",
    flexGrow: 1,
    flexBasis: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  shadowDefault: {
    background: platform.colors.white,
    border: "1px solid #e1e2f0",
    boxShadow: "0 0 10px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.25)",
    transition: "all 0.3s ease",
  },
  listItemContainer: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  itemSpacingElements: {
    flexDirection: "row",
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
  },
  footerDouble: {
    paddingTop: "var(--generic-space-container)",
    height:
      "calc( calc( var(--button-height) + var(--generic-space-container) - 4px ) * 2)",
    display: "grid",
    WebkitJustifyContent: "normal",
    justifyContent: "normal",
    boxSizing: "content-box",
    MozBoxSizing: "content-box",
    WebkitBoxSizing: "content-box",
  },
  formControl: {
    position: "relative",
    alignItems: "center",
    height: platform.generic.height,
    marginTop: "4px",
    display: "grid",
    border: "var(--form-control-border)",
    borderRadius: "4px",
  },
};
export default containerStyles;
