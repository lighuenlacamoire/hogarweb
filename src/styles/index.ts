import buttonStyles from "./buttons";
import textstyles from "./texts";
import reactnativeStyles from "./reactnatives";
import containerStyles from "./containers";
import platform from "./platform";

export {
  buttonStyles,
  textstyles,
  reactnativeStyles,
  containerStyles,
  platform,
};
