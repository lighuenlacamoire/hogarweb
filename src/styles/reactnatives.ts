type StyleProps = {
  viewDefault: React.CSSProperties;
  viewContainer: React.CSSProperties;
};

/**
 * Estilos del Boton
 */
const reactnativeStyles: StyleProps = {
  viewDefault: {
    flex: 1,
    display: "flex",
  },
  viewContainer: {
    flex: 1,
    display: "flex",
    flexDirection: "row",
  },
};

export default reactnativeStyles;
