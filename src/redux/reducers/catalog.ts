import { CatalogItem, CatalogState, Action } from "interfaces/state";

export const CATALOG_CHARGE = "hogarweb/catalog/CATALOG_CHARGE";
export const CATALOG_ITEM_UPDATE = "hogarweb/catalog/CATALOG_ITEM_UPDATE";

export const initialState: CatalogState = {
  catalog: undefined,
  categories: [],
};

export default (state = initialState, action: Action): CatalogState => {
  switch (action.type) {
    case CATALOG_CHARGE: {
      const { data } = action.payload;
      return {
        ...state,
        catalog: data,
      };
    }
    case CATALOG_ITEM_UPDATE: {
      const { catalog } = state;
      const { item } = action.payload;
      const newList: CatalogItem[] = [...(catalog?.items || [])];
      const idx = newList.findIndex((x) => x.id === item.id);
      if (idx !== -1) {
        newList[idx] = item;
      }
      if (catalog) {
        return {
          ...state,
          catalog: {
            ...catalog,
            items: newList.sort((a, b) => (a.name < b.name ? -1 : 1)),
          },
        };
      } else {
        return { ...state };
      }
    }
    default:
      return state;
  }
};
