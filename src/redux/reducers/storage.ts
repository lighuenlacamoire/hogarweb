import { StorageState, Action } from "interfaces/state";

export const STORAGE_SELECT = "hogarweb/storage/STORAGE_SELECT";

export const initialState: StorageState = {
  mainFolder: undefined,
  balanceFile: undefined,
  catalogFile: undefined,
};

export default (state = initialState, action: Action): StorageState => {
  switch (action.type) {
    case STORAGE_SELECT: {
      const { key, data } = action.payload;
      return {
        ...state,
        [key]: data,
      };
    }
    default:
      return state;
  }
};
