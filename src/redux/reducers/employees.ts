import { Action, EmployeeState } from "interfaces/state";

/** EMPLOYEE_LIST */
export const EMPLOYEE_LIST = "hogarweb/employees/EMPLOYEE_LIST";
/**
 * Estado inicial
 */
export const initialState: EmployeeState = {
  list: [],
};

export default (state = initialState, action: Action): EmployeeState => {
  switch (action.type) {
    case EMPLOYEE_LIST: {
      const { data } = action.payload;
      return {
        ...state,
        list: data,
      };
    }

    default:
      return state;
  }
};
