import { Action, AuthorizationState } from "interfaces/state";

export const LOGIN_SUCCESS = "hogarweb/authorization/LOGIN_SUCCESS";
export const LOGIN_FAILURE = "hogarweb/authorization/LOGIN_FAILURE";
export const LOGOUT = "hogarweb/authorization/LOGOUT";

export const initialState: AuthorizationState = {
  user: undefined,
  error: undefined,
  lastLogin: undefined,
  lastRefresh: undefined,
};

export default (state = initialState, action: Action): AuthorizationState => {
  switch (action.type) {
    case LOGIN_SUCCESS: {
      const { userInfo, accessToken } = action.payload;
      console.log("ue", userInfo);
      return {
        ...state,
        accessToken,
        user: userInfo,
        lastLogin: Date.now(),
      };
    }
    case LOGIN_FAILURE: {
      return {
        ...state,
        accessToken: undefined,
        user: undefined,
        error: action.payload,
      };
    }

    case LOGOUT: {
      return {
        ...state,
        accessToken: undefined,
        user: undefined,
      };
    }
    default:
      return state;
  }
};
