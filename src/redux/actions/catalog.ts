import { Action, CatalogData, CatalogItem } from "interfaces/state";
import { CATALOG_CHARGE, CATALOG_ITEM_UPDATE } from "redux/reducers/catalog";

/**
 * Realiza la carga en redux del json de Catalogo/Listas
 * @param {CatalogData} data contenido del archivo
 */
export const catalogCharge = (data?: CatalogData): Action => ({
  type: CATALOG_CHARGE,
  payload: { data },
});

/**
 * Agrega un item a la lista
 * @param {CatalogItem} item item para la lista
 */
export const catalogItemUpdate = (item: CatalogItem): Action => ({
  type: CATALOG_ITEM_UPDATE,
  payload: { item },
});
