import { EMPLOYEE_LIST } from "redux/reducers/employees";
import { CharacterDTO } from "interfaces/starwarsService";
import { Action } from "interfaces/state";

/**
 * Actualiza el listado de empleados para la capacitacion
 * @param {Array} data lista de empleados
 */
export const employeesSetList = (data: CharacterDTO[]): Action => ({
  type: EMPLOYEE_LIST,
  payload: { data },
});
