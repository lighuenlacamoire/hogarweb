import { Action } from "interfaces/state";
import { STORAGE_SELECT } from "redux/reducers/storage";
import { GDriveFile } from "interfaces/googleDrive";

/**
 * Guarda el archivo/carpeta que se ha seleccionado
 * @param {string} key clave
 * @param {GDriveFile} data datos del archivo de drive
 */
export const StorageSelect = (key: string, data?: GDriveFile): Action => ({
  type: STORAGE_SELECT,
  payload: { key, data },
});
