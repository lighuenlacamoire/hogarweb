import thunk from "redux-thunk";
import { persistReducer } from "reduxjs-toolkit-persist";
import { configureStore, combineReducers } from "@reduxjs/toolkit";
import Syncstorage from "reduxjs-toolkit-persist/lib/storage";
// Reducers
import status from "./reducers/status";
import authorization from "./reducers/authorization";
import notification from "./reducers/notification";
import catalog from "./reducers/catalog";
import storage from "./reducers/storage";

/**
 * Configuracion para la persistencia de redux
 */

const persistConfig = {
  key: "root",
  storage: Syncstorage,
  whitelist: ["authorization", "storage", "catalog"], // Solamente estos reducers seran persistidos
};

/**
 * Combina todos los reducers
 */
const rootReducer = combineReducers({
  status,
  authorization,
  notification,
  catalog,
  storage,
});

/**
 * Aplica la configuracion de persistencia a los reducers de Redux
 */
const persistedReducer = persistReducer(persistConfig, rootReducer);

/** Store de Redux */
export const store = configureStore({
  reducer: persistedReducer,
  middleware: [thunk],
  //middleware: getDefaultMiddleware => getDefaultMiddleware().concat(thunk),
});
//export const store = createStore(persistedReducer, applyMiddleware(thunk));

/**
 * Type del Store
 */
export type RootState = ReturnType<typeof rootReducer>;
